<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_announcement
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$menu = $app->getMenu();
$menulink = $menu->getItem($menuid)->link;

?>
<div class="mod_announcement">
	<?php if($list) { ?>
		<div class="listline">
			<?php foreach($list as $row) { ?>
				<div class="datablock">
					<div class="rdate">
						<?php 
							$date = explode(" ", $row->publish_up);
							echo JHtml::_('date', $date[0], JText::_('DATE_FORMAT_LC4'));
						?>
					</div>
					
					<span class="title_icon">•</span>
					
					<div class="rtitle">
						<a href="<?php echo JRoute::_("index.php?option=com_content&view=article&id={$row->id}&catid={$row->catid}&Itemid={$menuid}", false); ?>" title="<?php echo $row->title; ?>">
							<?php echo $row->title; ?>
						</a>
					</div>
				</div>
			<?php } ?>
		</div>
		<?php if(count($list) == $limit) { ?>
			<div class="rmore more_announcement">
				<a href="<?php echo $menulink . "&Itemid=" . $menuid; ?>" title="<?php echo JText::_('MOD_ANNOUNCEMENT_MORE'); ?>">
					<?php echo JText::_('MOD_ANNOUNCEMENT_MORE'); ?>
				</a>
			</div>
		<?php } ?>
	<?php }else{
			echo JText::_('MOD_ANNOUNCEMENT_NODATA');
	 	  } ?>	
</div>
