<?php
/**
 * @version		$Id: default.php 20196 2011-01-09 02:40:25Z ian $
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
?>
<script>
;(function(window, $){
	var $mainmenu;
	var $mainmainmenu;
	var $sfmenu;
	var $menuLinks;

	$(function(){
		$sfmenu = $('ul.sf-menu');
		$menuLinks = $('.menu_link_1');
		$submenu_warpper = $('.submenu_warpper');
		
        $sfmenu.superfish({
            autoArrows:  false, 
            dropShadows: false
        }); 
		
		$mainmenu = $('#open-mainmenu');
		$mainmainmenu = $('.mainmainmenu');
		
		$mainmenu.bind('click', function(){
			$('#topmenu').hide();			
			$mainmainmenu.toggle({speed: 500});
		});
		
		initMenuState();
		
		$(document).bind('responsive', initMenuState);
		
	});
	
	var initMenuState = function(){

			$mainmainmenu.show();
			$sfmenu.find('ul').css('position', 'absolute');
			$menuLinks.unbind('click');
			//$sfmenu.addClass('sf-menu');
	}
	
})(window, jQuery);