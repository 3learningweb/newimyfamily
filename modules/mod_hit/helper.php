<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_hit
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_hit
 *
 * @package     Joomla.Site
 * @subpackage  mod_hit
 * @since       1.5
 */
class ModHitHelper
{
	/**
	 * Retrieve list of banners
	 *
	 * @param   JRegistry  &$params  module parameters
	 *
	 * @return  mixed
	 */
	public static function &getList(&$params)
	{
		return;
	}
}
