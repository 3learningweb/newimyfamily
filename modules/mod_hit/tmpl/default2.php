<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_hit
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$text_limit = 40;
$text = strip_tags($h_content);

$menu = $app->getMenu();
$menulink = $menu->getItem($menuid)->link;

?>
<div class="mod_tmpl_hit">
	<div class="hit_name"><?php echo JText::_("MOD_HIT"); ?></div>
	<div class="hit_block">
		<div class="datablock">
			<div class="h_title"><?php echo $h_title; ?></div>
			<img src="http://img.youtube.com/vi/<?php echo $h_youtube; ?>/0.jpg" alt="<?php echo $h_title; ?>" width="70" height="48" />
			<div class="h_content">
				<?php 
					echo utf8_strlen($text) > $text_limit ? mb_substr($text, 0, $text_limit,'UTF-8').'...' : $text;
				?>
			</div>
			<div class="h_more">
				<a href="<?php echo $menulink . "&Itemid=" . $menuid; ?>" title="<?php echo JText::_('MOD_HIT_MORE'); ?>">
					<?php echo JText::_('MOD_HIT_MORE'); ?>
				</a>
			</div>
		</div>
	</div>
</div>
