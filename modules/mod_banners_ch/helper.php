<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_banners
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_banners
 *
 * @package     Joomla.Site
 * @subpackage  mod_banners
 * @since       1.5
 */
class ModBannersChHelper
{
	/**
	 * Retrieve list of banners
	 *
	 * @param   JRegistry  &$params  module parameters
	 *
	 * @return  mixed
	 */
	public static function &getBanner(&$params)
	{
		$app = JFactory::getApplication();
		$itemid = $app->input->getInt("Itemid");
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from($db->quoteName('#__menu'));
		$query->where("id = '{$itemid}'");
		$db->setQuery($query);
		$menu = $db->loadObject();

		if($menu->parent_id == 1) {
			$menu_id = $menu->id; 
		}else{
			$menu_id = $menu->parent_id;
		}
		
		$relation	 = $params->get('relation_id');
		$activity	 = $params->get('activity_id');
		$record = $params->get('record_id');

		switch($menu_id) {
			case $relation :
				$banner = $params->get('relation_img');
				break;
			case $activity :
				$banner = $params->get('activity_img');
				break;
			case $record :
				$banner = $params->get('record_img');
				break;
			default:
				$banner = $params->get('imy_img');
		}

		return $banner ;
	}
}
