<?php
/**
 * @version		:  2011-07-07 05:07:21$
 * @author		 
 * @package		Tabs
 * @copyright	Copyright (C) 2011- . All rights reserved.
 * @license		
 */

// no direct access
defined('_JEXEC') or die;

$cssFile 	= 'modules/mod_tabs/assets/css/tabs.css';
$scriptFile = 'modules/mod_tabs/assets/script/script.js';
$tabWidth 	= 120.5;
$separatorTop = 8;
$margin 	= 0;
$start		= 0;
$titleLeft 	= $tabWidth + $margin;

$separatorLeft = $tabWidth + ($margin / 2);
