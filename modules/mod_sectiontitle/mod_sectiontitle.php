<?php
/**
 * @version		:  2011-11-21 02:11:55$
 * @author		 
 * @package		Sectiontitle
 * @copyright	Copyright (C) 2011- . All rights reserved.
 * @license		
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
//require_once dirname(__FILE__).DS.'helper.php';

$section 	= null;
$app		= JFactory::getApplication();
$menu		= $app->getMenu();
		
// get menu path
$active = ($menu->getActive()) ? $menu->getActive() : $menu->getDefault();
$path		= $active->tree;

// if( isset($path[1]) ) {
	// $section = $menu->getItem($path[1]);
// } elseif( isset($path[0]) ){
	$section = $menu->getItem($path[0]);
// }

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

if($section){
	require JModuleHelper::getLayoutPath('mod_sectiontitle', $params->get('layout', 'default'));
}