<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_hit
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$app = JFactory::getApplication();

$desc = $params->get('desc');

modSearchcontentHelper::getKeywordCount($params);	// 統計熱門關鍵字
$kwlist = modSearchcontentHelper::getKeywordList($params);	// 取得熱門關鍵字
$itemid  = $params->get('menuid');
$catid  = $params->get('catid');


$jinput = $app->input;
$view	= $jinput->getCmd('view');

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_searchcontent', $params->get('layout', 'default'));
