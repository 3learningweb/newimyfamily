<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_hit
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$jinput = $app->input;

$keyword = $jinput->getString('content_keyword', '');
$keyword = htmlentities($keyword,ENT_QUOTES);

?>
<style>
	.desc {
		margin: 10px 0;
	}

	.keywordbar {
		margin-bottom: 20px;
	}

	.keywordbar > ul > li {
		float: left;
		color: #a90101;
		font-weight: bold;
		margin-left: 10px;
	}
	.keywordbar > ul > li a{
		cursor: pointer;
	}
</style>
<div class="mod_searchcontent">
	<div class="desc">
		<?php echo ($view == "article") ? "" : $desc; ?>
	</div>
	<div class="search">
		<form id="searchContentForm" action="<?php echo JRoute::_('index.php?option=com_content&Itemid=200',false);?>" method="get" class="form-inline">
			<div class="searchbar">
				<input name="content_keyword" id="content_keyword" maxlength="10" class="inputbox search-query" type="text" size="30" value="<?php echo $keyword; ?>" placeholder="請輸入問題關鍵字..." >
				<input type="submit" value="搜尋" class="searchBtn">&nbsp;&nbsp;
				<input type="button" value="所有問題" id="searchAllBtn">
			</div>
			<?php if ($kwlist) { ?>
			<div class="keywordbar">
				<ul>
					<li>熱門：</li>
					<?php foreach ($kwlist as $kw) { ?>
						<li><a><?php echo $kw->keyword; ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
			<input type="hidden" name="option" value="com_content" />
			<input type="hidden" name="view" value="category" />
			<input type="hidden" name="id" value="<?php echo $catid; ?>" />
			<input type="hidden" name="Itemid" value="<?php echo $itemid; ?>" />
		</form>
	</div>
</div>
<script type="text/javascript" language="javascript">
	jQuery(document).ready(function(){
		jQuery('.keywordbar > ul > li a').click(function() {
			jQuery('#content_keyword').val(jQuery(this).text());
			
			jQuery('#searchContentForm').submit();
		});

		jQuery('#searchAllBtn').click(function() {
			jQuery('#content_keyword').val('');

			jQuery('#searchContentForm').submit();
		});
	});
</script>