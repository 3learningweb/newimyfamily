<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tree extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    private $home = "https://imyfamily.moe.edu.tw/";
    public function index($page = 'tree1')
    {
        if (!file_exists(APPPATH . 'views/tree/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();

        $request = $this->input->post();
        if(isset($request["cat_id"]))
        {
            $session_write= array();
            $session_write["background"] =  $request["cat_id"];
            $session_write["background2"] = str_replace(".jpg","2.png",$request["cat_id"]);
            $this->session->set_userdata($session_write);
            $this->clean_session();
            redirect(base_url().'index.php/tree2', 'refresh');
        }


        $data['base_url'] = base_url();
        $data['home'] = $this->home;
        $this->load->view('tree/' . $page, $data);
    }

    public function tree2($page = 'tree2')
    {
        if (!file_exists(APPPATH . 'views/tree/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();

        $request = $this->input->post();
        if(isset($request["next"]))
            {
                $session_write= array();
                for($i=0; $i<12; $i++)
                {
                    if(isset($request["name".$i]))
                    {
                        $session_write["name".$i] =  $request["name".$i];
                    }
                }
                $this->session->set_userdata($session_write);
                $session = $this->session->all_userdata();
                $no_image = 1;
                $not_pair = 0;
                for($i=0; $i<12; $i++)
                {
                    if(!empty($session["img".$i]))
                    {
                        $no_image = 0;
                        break;
                    }
                }
                for($i=0; $i<12; $i++)
                {
                    if(!empty($session["img".$i]))
                    {
                        if(empty($session["name".$i]))
                        {
                            $not_pair = 1;

                            break;
                        }
                    }
                    if(!empty($session["name".$i]))
                    {
                        if(empty($session["img".$i]))
                        {
                            $not_pair = 1;

                            break;
                        }
                    }
                }
                if( $no_image == 1)
                {
                    echo '<script language="JavaScript"> alert("請至少上傳一張家人照片。");history.go(-1); //回上一頁</script>';
                    return;
                }
                if( $not_pair == 1)
                {
                    echo '<script language="JavaScript"> alert("請確認所有照片和稱謂均確實輸入。");history.go(-1); //回上一頁</script>';
                    return;
                }
                redirect(base_url().'index.php/tree3', 'refresh');
            }
        elseif(isset($request["click_img"]))
        {
            $session_write= array();
            for($i=0; $i<12; $i++)
            {
                if(isset($request["name".$i]))
                {
                    $session_write["name".$i] =  $request["name".$i];
                }
            }
            $this->session->set_userdata($session_write);
            redirect(base_url().'index.php/tree_img?id='.$request["click_img"], 'refresh');
        }
        $img = array();
        $name = array();
        for($i=0; $i<12; $i++)
        {
            if(isset($session["img".$i]))
            {
                $img[$i] = $session["img".$i];
            }
            if(isset($session["name".$i]))
            {
                $name[$i] = $session["name".$i];
            }
        }
        $data["img"] = $img;
        $data["name"] = $name;

        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('tree/' . $page, $data);
    }

    public function tree_img($page = 'tree_img')
    {
        if (!file_exists(APPPATH . 'views/tree/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();

        $request = $this->input->post();
        $get = $this->input->get();
        if(isset($request["upload"]))
        {
            if(!empty($_FILES["img"])) {
                if ($_FILES["img"]["error"] > 0) {
                    //echo "Error: " . $_FILES["img"]["error"];
                } else {
                    //echo "檔案名稱: " . $_FILES["file"]["name"]."<br/>";
                    //echo "檔案類型: " . $_FILES["file"]["type"]."<br/>";
                    //echo "檔案大小: " . ($_FILES["file"]["size"] / 1024)." Kb<br />";
                    //echo "暫存名稱: " . $_FILES["file"]["tmp_name"];

                    if(mb_strlen($_FILES["img"]["name"], "UTF-8") != strlen($_FILES["img"]["name"]))
                    {
                        echo '<script language="JavaScript"> alert("請上傳英文名稱的檔案"); //回上一頁</script>';
                        redirect(base_url().'index.php/tree_img?id='.$request["upload"], 'refresh');
                    }
                    $ext = substr(strrchr($_FILES["img"]["name"], '.'), 1);

                    if($ext != "jpg" && $ext != "JPEG")
                    {
                        echo '<script language="JavaScript"> alert("請勿上傳非jpg類型之檔案"); //回上一頁</script>';
                        redirect(base_url().'index.php/tree_img?id='.$request["upload"], 'refresh');
                    }
                    $time = (int)microtime(true);
                    move_uploaded_file($_FILES["img"]["tmp_name"],"upload/".$time.$_FILES["img"]["name"]);
                    $data["img"] =  $time.$_FILES["img"]["name"];
                }
                $session_write= array();
                $session_write["img_org".$request["upload"]] =  base_url().'upload/'.$time.$_FILES["img"]["name"];
                $this->session->set_userdata($session_write);
                $data["id"] = $request["upload"];
                $session["img_org".$data["id"]] =  $session_write["img_org".$request["upload"]];
            }else
            {
                echo '<script language="JavaScript"> alert("請選擇圖片後再點選上傳");history.go(-1); //回上一頁</script>';
                return;
            }
        }elseif(isset($request["crop"]))
        {
            $targ_w = $targ_h = 150;
            $jpeg_quality = 90;
            $time = (int)microtime(true);
            $src =  $session["img_org".$request["crop"]];
            $img_r = imagecreatefromjpeg($src);
            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

            imagecopyresampled($dst_r,$img_r,0,0,$request['x'],$request['y'],
                $targ_w,$targ_h,$request['w'],$request['h']);

            //header('Content-type: image/jpeg');

             $time = (int)microtime(true);
            $file = fopen(dirname(dirname(dirname(__FILE__))).'/upload/'.$time.'.jpg',"w");
            imagejpeg($dst_r, $file);
            imagedestroy($dst_r);
            $session_write= array();
            $session_write["img".$request["crop"]] =  base_url().'/upload/'.$time.'.jpg';
            $this->session->set_userdata($session_write);
            //var_dump($session_write["img".$request["crop"]]);
            //imagejpeg($dst_r,null,$jpeg_quality);
            redirect(base_url().'index.php/tree2', 'refresh');
        }
        if(isset($get["id"]))
        {
            $data["id"] = $get["id"];
        }
        if(isset($session["img_org".$data["id"]]))
        {
            $data["img"] = $session["img_org".$data["id"]];
        }



        $data['base_url'] = base_url();
        $this->load->view('tree/' . $page, $data);
    }

    public function tree3($page = 'tree3')
    {
        if (!file_exists(APPPATH . 'views/tree/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();

        $request = $this->input->post();
        if(!empty($request["download"]))
        {
            echo($request['img_val']);
            return;
            /*
            if(empty($request['img_val']))
            {
                echo '<script language="JavaScript"> alert("圖片轉存失敗");history.go(-1); //回上一頁</script>';
                return;
            }
            $time = (int)microtime(true);
            $filteredData=substr($request['img_val'], strpos($request['img_val'], ",")+1);
            $unencodedData=base64_decode($filteredData);
            file_put_contents(dirname(dirname(dirname(__FILE__))).'/tree_asset/images/'.$time.'img.png', $unencodedData);
            $jpeg_quality = 90;
            echo '<img src="'.base_url().'/tree_asset/images/'.$time.'img.png'.'">';
            die();
            */
            /*
            $html = $this->tree4_html();
            //redirect('http://FreeHTMLtoPDF.com/?convert='.$url.'&size=Ledger&orientation=portrait&framesize=2000&language=de', 'refresh');
            $url = 'http://freehtmltopdf.com';
            $data = array(  'convert' => '',
                'html' => $html,
                'baseurl' => 'http://www.myhost.com');

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            // set the pdf data as download content:
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="webpage.pdf"');
            echo($result);
            return;
            */
        }
        $data["bg"] = $session["background"];
        $data["bg2"] = $session["background2"];
        for($i=0; $i<12; $i++)
        {
            if(isset($session["img".$i]))
            {
                $img[$i] = $session["img".$i];
            }
            if(isset($session["name".$i]))
            {
                $name[$i] = $session["name".$i];
            }
        }
        $data["img"] = $img;
        $data["name"] = $name;
        $data['home'] = $this->home;
        $data['base_url'] = base_url();
        $this->load->view('tree/' . $page, $data);
    }

    public function tree4($page = 'tree4')
    {
        if (!file_exists(APPPATH . 'views/tree/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();

        $request = $this->input->post();
        if(!empty($request["download"]))
        {
            if(empty($request['img_val']))
            {
                echo '<script language="JavaScript"> alert("圖片轉存失敗");history.go(-1); //回上一頁</script>';
                return;
            }
            $time = (int)microtime(true);
            $filteredData=substr($request['img_val'], strpos($request['img_val'], ",")+1);
            $unencodedData=base64_decode($filteredData);
            file_put_contents(dirname(dirname(dirname(__FILE__))).'/tree_asset/images/'.$time.'img.png', $unencodedData);
            $jpeg_quality = 90;
            echo '<img src="'.base_url().'/tree_asset/images/'.$time.'img.png'.'">';
            die();
        }
        $data["bg"] = $session["background"];
        $data["bg2"] = $session["background2"];
        for($i=0; $i<12; $i++)
        {
            if(isset($session["img".$i]))
            {
                $img[$i] = $session["img".$i];
            }
            if(isset($session["name".$i]))
            {
                $name[$i] = $session["name".$i];
            }
        }
        $data["img"] = $img;
        $data["name"] = $name;

        $data['base_url'] = base_url();
        $this->load->view('tree/' . $page, $data);
    }

    public function tree4_html($page = 'tree4')
    {
        if (!file_exists(APPPATH . 'views/tree/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('url');
        $this->load->library('session');
        $session = $this->session->all_userdata();

        $request = $this->input->post();
        $data["bg"] = $session["background"];
        $data["bg2"] = $session["background2"];
        for($i=0; $i<12; $i++)
        {
            if(isset($session["img".$i]))
            {
                $img[$i] = $session["img".$i];
            }
            if(isset($session["name".$i]))
            {
                $name[$i] = $session["name".$i];
            }
        }
        $data["img"] = $img;
        $data["name"] = $name;
                $data['base_url'] = base_url();
        $this->load->view('tree/' . $page, $data);
        return $this->output->get_output();
    }

    private function clean_session()
    {
        $this->load->library('session');
        $this->session->unset_userdata('imember_uid');
        for($i=0;$i<12;$i++)
        {
            if(isset($session["img".$i]))
            {
                $this->session->unset_userdata("img".$i);
            }
            if(isset($session["name".$i]))
            {
                $this->session->unset_userdata("name".$i);
            }
            if(isset($session["img_org".$i]))
            {
                $this->session->unset_userdata("img_org".$i);
            }
        }

    }
}