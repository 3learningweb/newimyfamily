<div class="span9" id="content">




    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">電子報管理</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">


                    <form class="form-horizontal" action="?" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <legend>編輯電子報</legend>
                            <div class="control-group">
                                <label class="control-label" for="typeahead" >刊別 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="print" value="<?php if(!empty($edm_main[0]["print"])){echo $edm_main[0]["print"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">發行日期</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge datepicker" id="date01" name="publish" value="<?php if(!empty($edm_main[0]["publish"])){echo $edm_main[0]["publish"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">選擇版型 </label>
                                <div class="controls">
                                    <label>
                                        <input type="radio" id="optionsCheckbox2" name="template" value="1" <?php if(!empty($edm_main[0]["template"]) && $edm_main[0]["template"]==1){echo "checked";}?>>
                                        版型A
                                        <input type="radio" id="optionsCheckbox2" name="template" value="2" <?php if(!empty($edm_main[0]["template"]) && $edm_main[0]["template"]==2){echo "checked";}?>>
                                        版型B
                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">文章標題 </label>
                                <div class="controls">
                                    <input type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" name="title" value="<?php if(!empty($content[0]["title"])){echo $content[0]["title"];}?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label" for="typeahead">文章格式 </label>
                                <div class="controls">
                                    <label>
                                        <input type="radio" id="optionsCheckbox2" name="type" value="1" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==1){echo "checked";}?>>
                                        左圖繞文
                                        <input type="radio" id="optionsCheckbox2" name="type" value="2" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==2){echo "checked";}?>>
                                        右圖繞文
                                        <input type="radio" id="optionsCheckbox2" name="type" value="2" <?php if(!empty($content[0]["type"]) && $content[0]["type"]==3){echo "checked";}?>>
                                        上圖下文
                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="fileInput">文章照片</label>
                                <div class="controls">
                                    <?php 
                                    if(!empty($content[0]["img"]))
                                    {
                                        echo '<img src="'.$base_url.'upload/'.$content[0]["img"].'" style="max-width: 50%;">';
                                    }
                                    ?>
                                </div>
                                <div class="controls">
                                    <input class="input-file uniform_on" id="fileInput" type="file" name="img">
                                    <p class="help-block">檔案名稱必須為英文。</p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">內文</label>
                                <div class="controls">
                                    <textarea id="edm_content" name="edm_content" style="width: 600px; height: 200px"><?php if(!empty($content[0]["content"])){echo $content[0]["content"];}?></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">建議方法</label>
                                <div class="controls">
                                    <textarea  id="edm_suggestion" name="edm_suggestion" style="width: 600px; height: 200px"><?php if(!empty($content[0]["suggestion"])){echo $content[0]["suggestion"];}?></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="date01">新增日期</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge datepicker" id="date01" value="<?php if(!empty($edm_main[0]["create_date"])){echo $edm_main[0]["create_date"];}?>" disabled>
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">修改日期</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge datepicker" id="date01" value="<?php if(!empty($edm_main[0]["modify_date"])){echo $edm_main[0]["modify_date"];}?>" disabled>
                                    <p class="help-block"></p>
                                </div>
                            </div>


                            <div class="form-actions">
                                <button class="btn btn-primary" name="<?php if(!empty($edm_main[0])){echo "update";}else{echo "save";}?>" value="<?php if(!empty($edm_main[0])){echo $edm_main[0]["id"];}else{echo "1";}?>">儲存</button>
                                <button class="btn" name="back" value="1">不儲存 返回</button>
                            </div>
                        </fieldset>
                    </form>
                    </table>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
</div>
<hr>
<footer>
    <p></p>
</footer>
</div>
<!--/.fluid-container-->
<link href="<?php echo $base_url;?>vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="<?php echo $base_url;?>vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="<?php echo $base_url;?>vendors/chosen.min.css" rel="stylesheet" media="screen">

<link href="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

<script src="<?php echo $base_url;?>vendors/jquery-1.9.1.js"></script>
<script src="<?php echo $base_url;?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $base_url;?>vendors/jquery.uniform.min.js"></script>
<script src="<?php echo $base_url;?>vendors/chosen.jquery.min.js"></script>
<script src="<?php echo $base_url;?>vendors/bootstrap-datepicker.js"></script>

<script src="<?php echo $base_url;?>vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
<script src="<?php echo $base_url;?>vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

<script src="<?php echo $base_url;?>vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<?php echo $base_url;?>vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo $base_url;?>assets/form-validation.js"></script>

<script src="<?php echo $base_url;?>assets/scripts.js"></script>
<script>

    jQuery(document).ready(function() {
        FormValidation.init();
    });


    $(function() {
        $(".datepicker").datepicker();
        $(".uniform_on").uniform();
        $(".chzn-select").chosen();
        $('.textarea').wysihtml5();

        $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard').find('.bar').css({width:$percent+'%'});
            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }});
        $('#rootwizard .finish').click(function() {
            alert('Finished!, Starting over!');
            $('#rootwizard').find("a[href*='tab1']").trigger('click');
        });
    });
</script>
<script src="<?php echo $base_url;?>ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="<?php echo $base_url;?>ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">
<script src="<?php echo $base_url;?>ckeditor/samples/js/sample.js"></script>

<script>
    initSample();

    function fill_textarea(){
        var content = document.getElementById("edm_content");
        var suggestion = document.getElementById("edm_suggestion");
        var data = CKEDITOR.instances.edm_content.getData();
        content.innerHTML = data;
        alert(content.value);
        var data = CKEDITOR.instances.edm_suggestion.getData();
        suggestion.value = data;
    }
</script>

<script>
    CKEDITOR.replace( 'edm_content', {});
    CKEDITOR.replace( 'edm_suggestion', {});
</script>
</body>

</html>

