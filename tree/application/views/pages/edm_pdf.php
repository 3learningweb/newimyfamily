<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr">

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <base  />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="iMyfamily" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="/templates/system/js/html5shiv.js"></script>
    <script src="/media/jui/js/html5.js"></script>
    <![endif]-->


</head>
<body style='width:980px;background:#fff; font-family: "微軟正黑體", Microsoft JhengHei, "新細明體", PMingLiU, "細明體", MingLiU, "Helvetica Neue", Helvetica, Arial, sans-serif;'>
<table cellpadding="0" cellspacing="0" border="0" style="background:#fff;">
    <tr>
        <td colspan="3"><img src="<?php echo $base_url;?>assets_view/images/headPdf.jpg" border="0" style="display:block;"></td>
    </tr>
    <tr>
        <td colspan="3"><img src="<?php echo $base_url;?>upload/<?php echo $edm_main[0]["banner"];?>" border="0" style="display:block;"></td>
    </tr>
    <tr>
        <td width="30%" style=" text-align:left;">
            &nbsp;<br>
            &nbsp;<br>
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listAM.png" border="0" style="display:block;">
        </td>
        <td width="40%" style=" vertical-align:top;">
            <p style='color: #506816; line-height: 10px;font-family: "微軟正黑體", Microsoft JhengHei, "新細明體", PMingLiU, "細明體", MingLiU, "Helvetica Neue", Helvetica, Arial, sans-serif;'><?php echo $content[1]["list_title"];?></p>
            <p style='color: #1f1f1f;font-family: "微軟正黑體", Microsoft JhengHei, "新細明體", PMingLiU, "細明體", MingLiU, "Helvetica Neue", Helvetica, Arial, sans-serif;'><?php echo $content[1]["list_intro"];?></p>
        </td>
        <td width="30%" style="vertical-align:top;">
            &nbsp;<br>
            &nbsp;<br>
            <img width="120px" src="<?php echo $base_url;?>upload/<?php echo $content[1]["list_img"];?>" border="0" style="display:block;">
        </td>
    </tr>
    <tr>
        <td  width="100%"><img src="<?php echo $base_url;?>assets_view/images/dashed.png" border="0" style="display:block;"></td>
        <td width="0%" style="vertical-align:top;">

        </td>
        <td width="0%" style="vertical-align:top;">

        </td>
    </tr>
    <tr>
        <td width="30%" style=" text-align:left;">
            &nbsp;<br>
            &nbsp;<br>
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listBM.png" border="0" style="display:block;">
        </td>
        <td width="40%" style=" vertical-align:top;">
            <p style="color: #506816; line-height: 10px;"><?php echo $content[2]["list_title"];?></p>
            <p style="color: #1f1f1f;"><?php echo $content[2]["list_intro"];?></p>
        </td>
        <td width="30%" style="vertical-align:top;">
            &nbsp;<br>
            &nbsp;<br>
            <img width="120px" src="<?php echo $base_url;?>upload/<?php echo $content[2]["list_img"];?>" border="0" style="display:block;">
        </td>

    </tr>
    <tr>
        <td  width="100%"><img src="<?php echo $base_url;?>assets_view/images/dashed.png" border="0" style="display:block;"></td>
        <td width="0%" style="vertical-align:top;">

        </td>
        <td width="0%" style="vertical-align:top;">

        </td>
    </tr>
    <tr>
        <td width="30%" style="text-align:left;">
            &nbsp;<br>
            &nbsp;<br>
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listCM.png" border="0" style="display:block;">
        </td>
        <td width="40%" style="vertical-align:top;">
            <p style="color: #506816; line-height: 10px;"><?php echo $content[3]["list_title"];?></p>
            <p style="color: #1f1f1f;"><?php echo $content[3]["list_intro"];?></p>
        </td>
        <td width="30%" style="vertical-align:top;">
            &nbsp;<br>
            &nbsp;<br>
            <img width="120px" src="<?php echo $base_url;?>upload/<?php echo $content[3]["list_img"];?>" border="0" style="display:block;">
        </td>

    </tr>
    <tr>
        <td  width="100%"><img src="<?php echo $base_url;?>assets_view/images/dashed.png" border="0" style="display:block;"></td>
        <td width="0%" style="vertical-align:top;">

        </td>
        <td width="0%" style="vertical-align:top;">

        </td>
    </tr>
    <tr>
        <td width="30%" style="text-align:left;">
            &nbsp;<br>
            &nbsp;<br>
            <img width="100px" src="<?echo $base_url;?>assets_view/images/listDM.png" border="0" style="display:block;">
        </td>
        <td width="40%" style="vertical-align:top;">
            <p style="color: #506816; line-height: 10px;"><?php echo $content[4]["list_title"];?></p>
            <p style="color: #1f1f1f;"><?php echo $content[4]["list_intro"];?></p>
        </td>
        <td width="30%" style="vertical-align:top;">
            &nbsp;<br>
            &nbsp;<br>
            <img width="120px" src="<?php echo $base_url;?>upload/<?php echo $content[4]["list_img"];?>" border="0" style="display:block;">
        </td>

    </tr>
    <tr>
        <td  width="100%"><img src="<?php echo $base_url;?>assets_view/images/dashed.png" border="0" style="display:block;"></td>
        <td width="0%" style="vertical-align:top;">

        </td>
        <td width="0%" style="vertical-align:top;">

        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3" style="text-align:center; background-color: #f0f9e1;">
            <img width="50px" src="<?php echo $base_url;?>assets_view/images/logoM.png">
            和樂共親職
            <span>&#174;</span>
        </td>
    </tr>
    <tr>
        <td colspan="3"><img src="<?php echo $base_url;?>assets_view/images/newsMPdf.jpg" border="0" style="display:block;"></td>
    </tr>
    <tr>
        <td colspan="3">
            <table cellpadding="0" cellspacing="0" border="0" style="background:#fff; margin:0;padding:0;">
                <tr>
                    <td>
                        <p style="color: #fb7923;font-weight: bold;padding:0 15px;font-size: 18px; line-height: 14px;"><?php if(!empty($content)){echo $content[1]["title"];}?><br>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img width="250px" src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[1]["img"];}?>" border="0" style="display:block;">
                        <?php if(!empty($content)){echo $content[1]["content"];}?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #afd55c;"><p style="color: #666666;padding:0 15px;"><?php if(!empty($content)){echo $content[1]["suggestion"];}?></p></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3"><img src="<?php echo $base_url;?>assets_view/images/familyMPdf.png" border="0" style="display:block;"></td>
    </tr>
    <tr>
        <td colspan="3">
            <table cellpadding="0" cellspacing="0" border="0" style="background:#fff; margin:0;padding:0;">
                <tr>
                    <td>
                        <p style="color: #fb7923;font-weight: bold;padding:0 15px;font-size: 18px; line-height: 14px;"><?php if(!empty($content)){echo $content[2]["title"];}?><br>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img width="250px" src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[2]["img"];}?>" border="0" style="display:block;">
                        <?php if(!empty($content)){echo $content[2]["content"];}?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #afd55c;"><p style="color: #666666;padding:0 15px;"><?php if(!empty($content)){echo $content[2]["suggestion"];}?></p></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3"><img src="<?php echo $base_url;?>assets_view/images/teachMPdf.png" border="0" style="display:block;"></td>
    </tr>
    <tr>
        <td colspan="3">
            <table cellpadding="0" cellspacing="0" border="0" style="background:#fff; margin:0;padding:0;">
                <tr>
                    <td>
                        <p style="color: #fb7923;font-weight: bold;padding:0 15px;font-size: 18px; line-height: 14px;"><?php if(!empty($content)){echo $content[3]["title"];}?><br>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img width="250px" src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[3]["img"];}?>" border="0" style="display:block;">
                        <?php if(!empty($content)){echo $content[3]["content"];}?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #afd55c;"><p style="color: #666666;padding:0 15px;"><?php if(!empty($content)){echo $content[3]["suggestion"];}?></p></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td colspan="3"><img src="<?php echo $base_url;?>assets_view/images/wifeMPdf.png" border="0" style="display:block;"></td>
    </tr>
    <tr>
        <td colspan="3">
            <table cellpadding="0" cellspacing="0" border="0" style="background:#fff; margin:0;padding:0;">
                <tr>
                    <td>
                        <p style="color: #fb7923;font-weight: bold;padding:0 15px;font-size: 18px; line-height: 14px;"><?php if(!empty($content)){echo $content[4]["title"];}?><br>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img width="250px" src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[4]["img"];}?>" border="0" style="display:block;">
                        <?php if(!empty($content)){echo $content[4]["content"];}?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #afd55c;"><p style="color: #666666;padding:0 15px;"><?php if(!empty($content)){echo $content[4]["suggestion"];}?></p></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>

</table>
</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>

