<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw" lang="zh-tw" dir="ltr">

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <base  />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="iMyfamily" />
    <title>iMyfamily</title>
    <link href="<?php echo $base_url;?>assets_view/index.html" rel="canonical" />
    <link href="<?php echo $base_url;?>assets_view/indexc0d0.html?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
    <link href="<?php echo $base_url;?>assets_view/index7b17.html?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
    <link href="<?php echo $base_url;?>assets_view/templates/ch/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/reset.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/system/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/site.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/layout.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w1000.css" type="text/css" media="only screen and (min-width: 1000px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w6501000.css" type="text/css" media="only screen and (min-width: 650px) and (max-width: 999px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/templates/ch/css/w650.css" type="text/css" media="only screen and (max-width: 649px)" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/css/tabs.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/css/superfish.css" type="text/css" />
    <!-- Ming -->
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $base_url;?>assets_view/css/jquery-ui.css" />
    <!-- END Ming -->
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/system/js/caption.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/media/jui/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/ch/js/template.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/templates/system/js/respond.min.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_tabs/assets/script/script.js" type="text/javascript"></script>
    <script src="<?php echo $base_url;?>assets_view/modules/mod_sfmenu/assets/script/superfish.js" type="text/javascript"></script>

    <!-- Ming -->
    <script src="<?php echo $base_url;?>assets_view/js/jquery-1.8.2.min.js"></script>
    <script src="<?php echo $base_url;?>assets_view/js/jquery-ui.js"></script>
    <script src="<?php echo $base_url;?>assets_view/js/action.js"></script>
    <script>
        $(function() {
            $( "#education_datepicker" ).datepicker();
        });
    </script>
    <!-- END Ming -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--[if lt IE 9]>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="<?php echo $base_url;?>assets_view//templates/system/js/html5shiv.js"></script>
    <script src="<?php echo $base_url;?>assets_view//media/jui/js/html5.js"></script>
    <![endif]-->


</head>
<body class="epaperM">
<div class="epaperContentM">
    <a href="" id="goTop" style="text-align: center;"><img src="<?php echo $base_url;?>assets_view/images/top.png"><br>回<br>到<br>頂<br>端</a>
    <form action="?" method="post">
    <header>
        <div class="headerA">
            <img src="<?php echo $base_url;?>assets_view/images/header01.png">
            <a href="https://www.facebook.com/icoparenting" class="fbM">
                <img src="<?php echo $base_url;?>assets_view/images/facebook.png">
            </a>
            <div class="navMbM">
                <div class="navMbAM">
                    <img src="<?php echo $base_url;?>assets_view/images/navMBM.png">
                </div>
                <div class="navMbBM">
                    <a href="<?php echo $home."育兒預備/生育價值";?>">育兒預備</a>
                    <a href="<?php echo $home."教養孩子有方法/育兒困擾";?>">教養孩子有辦法</a>
                    <a href="<?php echo $home."寶寶的健康成長/寶寶的出生紀錄";?>">寶寶的健康成長</a>
                    <a href="<?php echo $home."爸媽加油站/育兒的甜蜜與負擔";?>">爸媽加油站</a>
                    <a href="<?php echo $base_url."index.php/edm_print";?>">各期電子報</a>
                </div>
            </div>
        </div>
        <div class="headerB">
            <div class="headerBL">
                <img src="<?php echo $base_url;?>assets_view/images/logoM.png">
                <h1>2016秋季刊</h1>
                <img src="<?php echo $base_url;?>assets_view/images/arrow_<?php echo $theme;?>.png">
            </div>

            <nav class="epaperNav">
                <a href="<?php echo $home."育兒預備/生育價值";?>">育兒預備</a>
                <a href="<?php echo $home."教養孩子有方法/育兒困擾";?>">教養孩子有辦法</a>
                <a href="<?php echo $home."寶寶的健康成長/寶寶的出生紀錄";?>">寶寶的健康成長</a>
                <a href="<?php echo $home."爸媽加油站/育兒的甜蜜與負擔";?>">爸媽加油站</a>
                <a href="<?php echo $base_url."index.php/edm_print";?>">各期電子報</a>
            </nav>
            <div class="headerBR">
                <p class="littleSun"><img src="<?php echo $base_url;?>assets_view/images/header02.png"><?php echo $edm_main[0]["title"];?></p>
                <p class="littleWording">FAMILY TIME</p>
            </div>
        </div>
    </header>
    <section class="bannerM">
        <img src="<?php echo $base_url;?>upload/<?php echo $edm_main[0]["banner"];?>">
    </section>
    <section class="epaperLFM">
        <section class="epaperListM">
            <ul>
                <li>
                    <a href="#detail1">
                    <div class="epaperListA">
                        <img src="<?php echo $base_url;?>assets_view/images/listAM.png">
                    </div>
                    <div class="epaperListB">
                        <p class="epaperListTitle"><?php echo $content[1]["list_title"];?></p>
                        <p><?php echo $content[1]["list_intro"];?></p>
                    </div>
                    <div class="epaperListC">
                        <img src="<?php echo $base_url;?>upload/<?php echo $content[1]["list_img"];?>">
                    </div>
                    </a>
                </li>
                <li>
                    <a href="#detail2">
                    <div class="epaperListA">
                        <img src="<?php echo $base_url;?>assets_view/images/listBM.png">
                    </div>
                    <div class="epaperListB">
                        <p class="epaperListTitle"><?php echo $content[2]["list_title"];?></p>
                        <p><?php echo $content[2]["list_intro"];?></p>
                    </div>
                    <div class="epaperListC">
                        <img src="<?php echo $base_url;?>upload/<?php echo $content[2]["list_img"];?>">
                    </div>
                    </a>
                </li>
                <li>
                    <a href="#detail3">
                    <div class="epaperListA">
                        <img src="<?php echo $base_url;?>assets_view/images/listCM.png">
                    </div>
                    <div class="epaperListB">
                        <p class="epaperListTitle"><?php echo $content[3]["list_title"];?></p>
                        <p><?php echo $content[3]["list_intro"];?></p>
                    </div>
                    <div class="epaperListC">
                        <img src="<?php echo $base_url;?>upload/<?php echo $content[3]["list_img"];?>">
                    </div>
                    </a>
                </li>
                <li>
                    <a href="#detail4">
                    <div class="epaperListA">
                        <img src="<?php echo $base_url;?>assets_view/images/listDM.png">
                    </div>
                    <div class="epaperListB">
                        <p class="epaperListTitle"><?php echo $content[4]["list_title"];?></p>
                        <p><?php echo $content[4]["list_intro"];?></p>
                    </div>
                    <div class="epaperListC">
                        <img src="<?php echo $base_url;?>upload/<?php echo $content[4]["list_img"];?>">
                    </div>
                        </a>
                </li>
            </ul>
        </section>
        <footer>
            <img src="<?php echo $base_url;?>assets_view/images/logoM.png">
            和樂共親職
            <span>&#174;</span>
        </footer>

        <?php 
            for($i=1; $i<=4; $i++)
            {
        ?>
                <section class="epaperDetailTitleM" style="margin-top: -20px;">
                    <a name="detail<?php echo $i;?>"><img class="epaperDetailTitleAM" src="<?php echo $base_url;?>assets_view/images/content_title_<?php echo $i;?>.png"></a>
                    <img src="<?php echo $base_url;?>assets_view/images/edetaillast.png">
                </section>
                <?php if(!empty($content) && $content[$i]["type"] != 4) {?>
                <section class="epaperListM">
                    <div class="detailAll">
                        <h2 class="detailTitle"><?php if(!empty($content)){echo $content[$i]["title"];}?></h2>
                        <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[$i]["img"];}?>" style="float:right;"><?php if(!empty($content)){echo $content[$i]["content"];}?></p>
                        <div class="detailG">
                            <p><?php if(!empty($content)){echo $content[$i]["suggestion"];}?></p>
                        </div>
                    </div>
                </section>
                <?php }else { ?>
                <section class="epaperListM">
                    <div class="detailAll">
                        <h2 class="detailTitle"><?php if(!empty($content)){echo $content[$i]["title"];}?></h2>
                        <div class="detailL">
                            <h3 class="detailTitle2"></h3>
                            <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[$i]["img"];}?>" width="300px" style="float:left;margin-right:20px;"><?php if(!empty($content)){echo $content[$i]["content"];}?></p>
                            <h3 class="detailTitle2" style="margin-top:20px;"></h3>
                            <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[$i]["img2"];}?>" width="300px" style="float:right;margin-left:20px;"><?php if(!empty($content)){echo $content[$i]["content2"];}?></p>
                        </div>
                        <div class="detailR">
                            <div class="detailG">
                                <h3 class="detailTitle3"></h3>
                                <p><img src="<?php if(!empty($content)){echo $base_url.'upload/'.$content[$i]["img3"];}?>" style="float:left;"><?php if(!empty($content)){echo $content[$i]["suggestion"];}?></p>
                            </div>
                        </div>
                    </div>
                </section>
        <?php
                    }
            }
        ?>

        <section class="epaperListM">
            <div style="text-align: left;"><button style="width: 80px; font-size: 14px; border: 0px; background-color: transparent;" name="back" value="1"><img src="<?php echo $base_url;?>assets_view/images/back.png"></button></div>
        </section>

    </section>
        </form>
</div>
</body>

<!-- Mirrored from imyfamily.moe.edu.tw/ by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 05 Sep 2016 16:33:54 GMT -->
</html>