$(document).on("click", ".navMobile", function(){
   $('body').find('.navMobileOpen').slideToggle();
});
$(function(){
	$("#goTop").click(function(){
		$("html,body").animate({scrollTop:0},900);
    		return false;
	});
});
$(window).scroll(function() {
    var y = $(this).scrollTop();
    if ( y > 50) {
        $("#goTop").fadeIn();
    } else {
        $("#goTop").fadeOut();
    }
});
