<?php
define( '_JEXEC', 1 );

define('JPATH_BASE', '../../');

define( 'DS', DIRECTORY_SEPARATOR );

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

JDEBUG ? $_PROFILER->mark( 'afterLoad' ) : null;
$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

header ("Content-Type:text/html;charset=utf-8");
header ("Cache-Control: no-cache, must-revalidate");

jimport( 'joomla.factory');

require_once ( "html2doc.php" );

$app = JFactory::getApplication();

$post = $app->input->getArray($_POST);
$todo_data = $post['todo_data'];

// print_r($post); exit();
ob_start();

?>
<style>
	.admintable th {
		background-color: #E5B7B7;
	}
</style>
<p align="center">
	<font size="4"><b>我的家庭急救包</b></font>
</p>
<p align="right">
	<font size="3"><b>清點日期_______________</b></font>
</p>

<?php if($post['check_list'] == 1) { ?>
	<table border="1" class="admintable" align="center">
		<tbody>
		<!-- 器材 -->
		<?php if($post['equipment_data']){ ?>
			<?php $items = explode(",", $post['equipment_data']) ?>
			<tr>
				<th width="350">器材</th>
				<th width="80">清點打v</th>		
			</tr>
			<?php foreach($items as $key => $item){ ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 內服藥 -->
		<?php if($post['medicine_data']){ ?>
			<?php $items = explode(",", $post['medicine_data']) ?>
			<tr>
				<th width="350">內服藥</th>
				<th width="80">清點打v</th>		
			</tr>
			<?php foreach($items as $key => $item) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 外傷 -->
		<?php if($post['traumatic_data']){ ?>
			<?php $items = explode(",", $post['traumatic_data']) ?>
			<tr>
				<th width="350">外傷</th>
				<th width="80">清點打v</th>		
			</tr>
			<?php foreach($items as $key => $item) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 醫療用品 -->	
		<?php if($post['medical_data']){ ?>
			<?php $items = explode(",", $post['medical_data']) ?>
			<tr>
				<th width="350">醫療用品</th>
				<th width="80">清點打v</th>		
			</tr>
			<?php foreach($items as $key => $item) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 其他 -->
		<?php if($post['other_data']){ ?>
			<?php $items = explode(",", $post['other_data']) ?>
			<tr>
				<th width="350">其他</th>
				<th width="80">清點打v</th>		
			</tr>
			<?php foreach($items as $key => $item) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
		
		</tbody>
	</table>
<?php } ?>

<?php
	$content = ob_get_contents();

	ob_end_clean();

	$filename = "家庭急救包";

	$htmltodoc= new HTML_TO_DOC();
	$htmltodoc->createDoc($content, $filename, true);
?>
