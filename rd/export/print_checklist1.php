<?php
define( '_JEXEC', 1 );

define('JPATH_BASE', '../../');

define( 'DS', DIRECTORY_SEPARATOR );

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

JDEBUG ? $_PROFILER->mark( 'afterLoad' ) : null;
$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

header ("Content-Type:text/html;charset=utf-8");
header ("Cache-Control: no-cache, must-revalidate");

jimport( 'joomla.factory');

require_once ( "html2doc.php" );

$app = JFactory::getApplication();

$post = $app->input->getArray($_POST);
$todo_data = $post['todo_data'];

// print_r($post); exit();
ob_start();

?>
<style>
	.admintable th {
		background-color: #C6D9F1;
	}
</style>
<p align="center">
	<font size="4"><b><?php echo $post['mother_name'] ?>的待產包</b></font>
</p>
<p align="center">
	<font size="4"><b>預產期：<?php echo $post['date'] ?></b></font>
</p>

<?php if($post['check_list'] == 1) { ?>
	<table border="1" class="admintable" align="center">
		<tbody>
		<!-- 媽媽用品 -->
		<?php if($post['mother_data']){ ?>
			<?php $items = explode(",", $post['mother_data']) ?>
			<tr>
				<th width="350">媽媽用品</th>
				<th width="80">確認打v</th>		
			</tr>
			<?php foreach($items as $key => $item){ ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 寶寶用品 -->
		<?php if($post['baby_data']){ ?>
			<?php $items = explode(",", $post['baby_data']) ?>
			<tr>
				<th width="350">寶寶用品</th>
				<th width="80">確認打v</th>		
			</tr>
			<?php foreach($items as $key => $item) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 哺乳用品 -->
		<?php if($post['breastfeed_data']){ ?>
			<?php $items = explode(",", $post['breastfeed_data']) ?>
			<tr>
				<th width="350">哺乳用品</th>
				<th width="80">確認打v</th>		
			</tr>
			<?php foreach($items as $key => $item) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 證件 -->	
		<?php if($post['card_data']){ ?>
			<?php $items = explode(",", $post['card_data']) ?>
			<tr>
				<th width="350">證件</th>
				<th width="80">確認打v</th>		
			</tr>
			<?php foreach($items as $key => $item) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 其他 -->
		<?php if($post['other_data']){ ?>
			<?php $items = explode(",", $post['other_data']) ?>
			<tr>
				<th width="350">其他</th>
				<th width="80">確認打v</th>		
			</tr>
			<?php foreach($items as $key => $item) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $item; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>
		
		<!-- 待辦事項 -->
		<?php if(count($todo_data) != 0) { ?>
			<tr>
				<th width="350">待辦事項</th>
				<th width="80">確認打v</th>
			</tr>
			<?php foreach($todo_data as $key => $todo) { ?>
			<tr>
				<td><?php echo $key+1 . "." . $todo; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		<?php } ?>	
		</tbody>
	</table>
<?php } ?>

<?php if($post['check_contact'] == 1){ ?>
	<table border="1" class="admintable" align="center">
		<thead>
			<tr><th colspan="5">重要聯絡資訊</th></tr>
			<tr>
				<th width="140">類別</th>
				<th width="130">姓名</th>
				<th width="130">電話</th>
				<th width="200">E-mail</th>
				<th width="300">其他資訊</th>
			</tr>
		</thead>
		<tbody>
		
		<!-- 待產醫院 -->
		<?php if($post['hospital_num'] != 0) { ?>
			<?php for($i=1 ; $i<=$post['hospital_num'] ; $i++) { ?>
			<tr>
				<?php for($j=0 ; $j<=3 ; $j++) { ?>
					<?php if($i==1 && $j==0) { ?>
						<td rowspan="<?php echo $post['hospital_num']; ?>" align="center">待產醫院</td>
					<?php } ?>
					<td><?php echo $post["hospital_{$i}"][$j]; ?></td>
				<?php } ?>
			</tr>
			<?php } ?>
		<?php } ?>	
	
		<!-- 月子中心-->
		<?php if($post['postpartum_num'] != 0) { ?>
			<?php for($i=1 ; $i<=$post['postpartum_num'] ; $i++) { ?>
			<tr>
				<?php for($j=0 ; $j<=3 ; $j++) { ?>
					<?php if($i==1 && $j==0) { ?>
						<td rowspan="<?php echo $post['postpartum_num']; ?>" align="center">月子中心</td>
					<?php } ?>
					<td><?php echo $post["postpartum_{$i}"][$j]; ?></td>
				<?php } ?>
			</tr>
			<?php } ?>
		<?php } ?>
		
		<!-- 月子餐 -->
		<?php if($post['postpartum_diet_num'] != 0) { ?>
			<?php for($i=1 ; $i<=$post['postpartum_diet_num'] ; $i++) { ?>
			<tr>
				<?php for($j=0 ; $j<=3 ; $j++) { ?>
					<?php if($i==1 && $j==0) { ?>
						<td rowspan="<?php echo $post['postpartum_diet_num']; ?>" align="center">月子餐</td>
					<?php } ?>
					<td><?php echo $post["postpartum_diet_{$i}"][$j]; ?></td>
				<?php } ?>
			</tr>
			<?php } ?>
		<?php } ?>
		
		<!-- 臍帶血 -->
		<?php if($post['cord_blood_num'] != 0) { ?>
			<?php for($i=1 ; $i<=$post['cord_blood_num'] ; $i++) { ?>
			<tr>
				<?php for($j=0 ; $j<=3 ; $j++) { ?>
					<?php if($i==1 && $j==0) { ?>
						<td rowspan="<?php echo $post['cord_blood_num']; ?>" align="center">臍帶血</td>
					<?php } ?>
					<td><?php echo $post["cord_blood_{$i}"][$j]; ?></td>
				<?php } ?>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 重要聯絡人 -->
		<?php if($post['important_num'] != 0) { ?>
			<?php for($i=1 ; $i<=$post['important_num'] ; $i++) { ?>
			<tr>
				<?php for($j=0 ; $j<=3 ; $j++) { ?>
					<?php if($i==1 && $j==0) { ?>
						<td rowspan="<?php echo $post['important_num']; ?>" align="center">重要聯絡人</td>
					<?php } ?>
					<td><?php echo $post["important_{$i}"][$j]; ?></td>
				<?php } ?>
			</tr>
			<?php } ?>
		<?php } ?>
	
		<!-- 其他 -->
		<?php if($post['other_contact_num'] != 0) { ?>
			<?php for($i=1 ; $i<=$post['other_contact_num'] ; $i++) { ?>
			<tr>
				<?php for($j=0 ; $j<=3 ; $j++) { ?>
					<?php if($i==1 && $j==0) { ?>
						<td rowspan="<?php echo $post['other_contact_num']; ?>" align="center">其他</td>
					<?php } ?>
					<td><?php echo $post["other_contact_{$i}"][$j]; ?></td>
				<?php } ?>
			</tr>
			<?php } ?>
		<?php } ?>		
		</tbody>
	</table>
<?php } ?>

<?php
	$content = ob_get_contents();

	ob_end_clean();

	$filename = "幸福待產包";

	$htmltodoc= new HTML_TO_DOC();
	$htmltodoc->createDoc($content, $filename, true);
?>
