	(function($) {
		$(document).ready(function() {
			var limit = [];
			limit['choose1'] = 10;
			
			// drag 			
			$(".choose_block").each(function() {
				$(this).draggable({
					activeClass: 'droppable-active',
					containment: '#choose_form',
					stack: '.choose_block',
					cursor: 'move',
					scroll: true,
					revert: true
				});
			});
			
			// accept
			$(".block").each(function() {  // 起始地
				var block = $(this).children().prop("id").split("_");
				$(this).droppable( {
			      	accept: '#block_'+block[1],
			      	activeClass: 'choose_active',
			      	hoverClass: 'hovered',
			      	drop: handleCardDrop
    			});
			});
			
			$(".mychoose").each(function() {  // 目的地
				$(this).droppable( {
			      	accept: '.choose_block',
			      	hoverClass: 'hovered',
			      	drop: handleCardDrop
    			});
			});

			function handleCardDrop( event, ui ) {
				var choose = $(this);
				if(!checkLimit(choose)){
					return false;
				}
				
				if(choose.prop("id")) {
					$(ui.draggable).css("margin", "2px");
					$(ui.draggable).css("float", "left");
					$(this).append($(ui.draggable));
				}else{
					$(ui.draggable).css("margin", "0px");
					$(ui.draggable).css("float", "none");
					$(this).append($(ui.draggable));
				}
				
			}
			
			// 限制個數
			function checkLimit(choose) {
				var key = choose.prop("id");
				var count = choose.children("div").length;
				if(limit[key] == count){
					alert("預備事項最多只能選取"+limit[key]+"項");
					return false;
				}
				return true;
			}

			
			// submit
			$(document).on("click", "#submit_btn", function() {
				var num = $(".mychoose").children("div").length;
				
				if(num > 10 ) {
					alert("預備事項最多只能選取 10 項");
					return false;
				}else if(num < 10) {
					alert("預備事項請選取 10 項");
					return false;
				}else if(num == 10) {
					var value = "";
					var item;
					$(".mychoose").children("div").each(function() {
						item = $(this).prop("id").split("_");
						value += item[1] + ",";				
					});
					
					$(".code_list").val(value);

					$("#choose_form").submit();
				}
			});

			// reset
			$(document).on("click", "#reset_btn", function() {
				location.reload();
			});
		});
	})(jQuery);