<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

// 特質分配
$label_code[1] = "書本：會讀書，適合當學者、專家。";
$label_code[2] = "彩色筆：有藝術天份，適合當畫家、藝術家。";
$label_code[3] = "印章：有權勢，會當大官。";
$label_code[4] = "尺：適合當設計師、建築師。";
$label_code[5] = "蔥：代表非常聰明。";
$label_code[6] = "芹菜：代表生性勤勞。";
$label_code[7] = "相機：適合當攝影師、影像相關的工作。";
$label_code[8] = "金元寶：代表很富有，財源滾滾來。";
$label_code[9] = "聽診器：適合當醫生、醫護人員。";
$label_code[10] = "針線包：適合當服裝、造型設計師。";
$label_code[11] = "樂器(笛子)：適合音樂天份，當演奏家。";
$label_code[12] = "計算機：適合從商、會計師。";
$label_code[13] = "麥克風：適合當歌星.演藝工作。";
$label_code[14] = "桌球拍(運動用品)：適合當運動員。";
$label_code[15] = "玩具槍：能當軍官、警察工作。";
$label_code[16] = "手機：3c專家，適合科技.通訊業。";
$label_code[17] = "湯匙：適合當廚師.飯店業者。";
$label_code[18] = "積木：適合建築師.設計師。";

// 初始化
$cats[1] = 0; $cats[2] = 0; $cats[3] = 0; $cats[4] = 0;


	$code_list = $app->input->getString('code_list');
	$codes = explode(",", $code_list);
	foreach ($codes as $code) {
		$result_str.=$label_code[$code]."<br>";
	}


?>

<style>
    .box {
		width:60%;
		margin: 0 auto;
	}
    .zone {
		width:300px; height:300px;
	}

	.cattb {
		width: 100%;
		margin-top: 20px;
		text-align: center;
		border: 1px solid #f89406;
	}

	.cattb th {
		background-color: #f89406;
		font-weight: bold;
		padding: 5px;
	}

	.cattb td {
		vertical-align: top;
	}

	.readmore {
		margin-top: 20px;
	}
</style>

<div class="com_choose">
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	<div style="font-size: 16px; line-height: 30px;">
		您期望寶寶會抓的東西是：<br>
		<?php echo $result_str;?>
		<br />
		<div class="choose_description">
		『抓周』可以說是寶寶人生第1回的生涯試探，全家人給予寶寶祝福，慶祝平安長大到1歲，並開始展開對寶寶未來發展的殷殷期待。<br>

		想一想，為何期望寶寶抓到以上3個物品？<br>

問一問，自己滿周歲時，家中長輩有進行抓週活動嗎？當時，親友期望自己成為什麼樣的人？</div>
	</div>
<?php /*	<div class="component_intro">
		
		<div class="box">
			<canvas id="chart-area" class="zone"></canvas>
		</div>
		
		<div class="tb">
			<table border="1" class="listtable">
				<tbody>
					<tr>
						<th>精力資源</th>
						<td>育兒所需的付出較多的照顧心力，也會增加許多的家務、生理及情緒負擔，不但要共同討論規劃與分工，並須正視喘息與抒壓的需要，適當的提供紓解與支持。</td>
					</tr>
					<tr>
						<th>時間資源</th>
						<td>當時間不變，育兒的責任與壓力增加，將排擠到工作－家庭、夫妻相處與親密關係的時間，夫妻更需不斷營造親密互動的時間，找到工作與家庭的平衡點。</td>
					</tr>
					<tr>
						<th>溝通/關係資源</th>
						<td>當父母的角色出現，夫妻之間的關係須有改變調整，如能事先建立一致的教養策略、良好的衝突處理、溝通與親友關係，對於育兒準備十分有助益。</td>
					</tr>
					<tr>
						<th>金錢/財務資源</th>
						<td>孩子的食、衣、住、行、育、樂，甚至是未來發展，都會增添家庭的經濟負擔，因此重新規劃收支並事先規劃是相當重要的。</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div style="font-size: 16px;">關於育兒準備，夫妻之間想的不一定，邀請另一半來做做測驗，討論一下彼此的想法與需要，攜手面對育兒的需要與挑戰，為寶寶的到來多一分準備，多一分喜悅。</div>
		
		<div class="readmore">
			<div class="noteTableExplain" style="font-size: large; color: #333333; padding-top: 10px; padding-bottom: 10px; font-family: 微軟正黑體;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td><img style="width: 42px; height: 48px;" src="images/Speaker_03_02.png" alt="" />
							</td>
							<td style="background: url('images/Speaker_03_bg.png') repeat-x;"><a href="index.php?option=com_content&amp;view=article&amp;id=38&amp;Itemid=149" style="color: #333333; text-decoration: none;">當父母?攜手先學做好夫妻!!</a>
							</td>
							<td><img src="images/Speaker_03.png" alt="" width="21" height="48" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div style="text-align: right;">
			<img alt="" src="images/Owl.png" style="text-align: right; width: 188px; height: 164px;">
		</div>
	</div> */ ?>
</div>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {
			
		});
	})(jQuery);
</script>