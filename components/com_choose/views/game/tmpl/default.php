<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$age = $app->getUserState("form.choose.age");

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>
<script type="text/javascript" src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo (((!empty($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) == "on") || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://')?>www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="components/com_choose/assets/js/choose.js"></script>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<?php /*<!-- .arrow_box {
		position: relative;
		background: #f7ffb3;
		border: 8px solid #f5c36c;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #f7ffb3;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #f5c36c;
		border-width: 23px;
		margin-top: -23px;
	}
	--> */?>
	#temperature {
		display: none;
		position: relative;
		top: 60px; 
		right: 125px;
	}
</style>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {	
			$('input[name=submit0]').click(function() {
				$(".game_block").show();
				$(".pretext").hide();
			});
		});
	})(jQuery);
	
</script>

<div class="com_choose">	
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<div class="hint_msg" style="display: none;">此頁面請切換為電腦版，即可正常使用功能</div><br/>
	
	<div class="pretext">
		<div class="game_page-header">
			<div class="title">線上抓周
			</div>
		</div>
		<div class="dialog_box">&nbsp;</div>
		<div class="arrow_box">
			<p style="font-size: 17px; line-height: 35px; margin: 10px;">
				寶寶出生滿週歲，古稱「度晬」，依習俗會進行「抓週」儀式，象
徵親友對寶寶未來的祝福。請問您，會期望寶寶抓到什麼物品呢？
					<br><input type="button" id="submit0" name="submit0" value="開始" />
			</p>
		</div>
	</div>
	
	<form id="choose_form" name="choose_form" method="post" action="<?php echo JRoute::_("index.php?option=com_choose&view=result&Itemid={$itemid}", false); ?>">
		<div class="game_block" style="display: none;">
			<div class="game_page-header">
				<div class="title">
					線上抓周
				</div>
			</div>
				
			<div id="mychoose_block">
				<div class="mychoose" id="choose1">
					<input type="hidden" class="code_list" name="code_list" id="code_list" value="">
				</div>
			</div>
	「每一種物品都代表對寶寶的祝福，最期望寶寶得到的祝福是什麼呢？請由下方抓出三樣物品放入篩子。」
			<!-- 特質一覽表 -->
			<div class="title_icon">
				<span class="title_text">擺設物件</span>
			</div>
	
			<table>
				<tbody>
				<!-- 1~6 -->
				<tr>
					<td>
						<div class="choose_block" id="block_1">
							<div class="item_text">1.書本</div>
							<div class="choose_item" id="choose_item1" tag="1" >
								<img src="components/com_choose/assets/images/01.png" alt="書本" />
								<input type="hidden" class="item_code" value="1">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_2">
							<div class="item_text">2.彩色筆</div>
							<div class="choose_item" id="choose_item2" tag="2">
								<img src="components/com_choose/assets/images/02.png" alt="彩色筆" />
								<input type="hidden" class="item_code" value="2">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_3">
							<div class="item_text">3.印章</div>
							<div class="choose_item" id="choose_item3" tag="3">
								<img src="components/com_choose/assets/images/03.png" alt="印章" />
								<input type="hidden" class="item_code" value="3">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_4">
							<div class="item_text">4.尺</div>
							<div class="choose_item" id="choose_item4" tag="4">
								<img src="components/com_choose/assets/images/04.png" alt="尺" />
								<input type="hidden" class="item_code" value="4">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_5">
							<div class="item_text">5.蔥</div>
							<div class="choose_item" id="choose_item5" tag="5">
								<img src="components/com_choose/assets/images/05.png" alt="蔥" />
								<input type="hidden" class="item_code" value="5">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_6">
							<div class="item_text">6.芹菜</div>
							<div class="choose_item" id="choose_item6" tag="6">
								<img src="components/com_choose/assets/images/06.png" alt="芹菜" />
								<input type="hidden" class="item_code" value="6">
							</div>
						</div>
					</td>
				</tr>
				
				<!-- 7~12 -->
				<tr>
					<td>
						<div class="choose_block" id="block_7">
							<div class="item_text">7.相機</div>
							<div class="choose_item" id="choose_item7" tag="7">
								<img src="components/com_choose/assets/images/07.png" alt="相機" />
								<input type="hidden" class="item_code" value="7">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_8">
							<div class="item_text">8.金元寶</div>
							<div class="choose_item" id="choose_item8" tag="8">
								<img src="components/com_choose/assets/images/08.png" alt="金元寶" />
								<input type="hidden" class="item_code" value="8">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_9">
							<div class="item_text">9.聽診器</div>
							<div class="choose_item" id="choose_item9" tag="9">
								<img src="components/com_choose/assets/images/09.png" alt="聽診器" />
								<input type="hidden" class="item_code" value="9">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_10">
							<div class="item_text">10.針線包</div>
							<div class="choose_item" id="choose_item10" tag="10">
								<img src="components/com_choose/assets/images/10.png" alt="針線包" />
								<input type="hidden" class="item_code" value="10">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_11">
							<div class="item_text">11.笛子(樂器)</div>
							<div class="choose_item" id="choose_item10" tag="11">
								<img src="components/com_choose/assets/images/11.png" alt="笛子(樂器)" />
								<input type="hidden" class="item_code" value="11">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_12">
							<div class="item_text">12.計算機</div>
							<div class="choose_item" id="choose_item10" tag="12">
								<img src="components/com_choose/assets/images/12.png" alt="計算機" />
								<input type="hidden" class="item_code" value="12">
							</div>
						</div>
					</td>
				</tr>
				
				<!-- 13~18 -->
					<td>
						<div class="choose_block" id="block_13">
							<div class="item_text">13.麥克風</div>
							<div class="choose_item" id="choose_item9" tag="13">
								<img src="components/com_choose/assets/images/13.png" alt="麥克風" />
								<input type="hidden" class="item_code" value="13">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_14">
							<div class="item_text">14.桌球拍(運動用品)</div>
							<div class="choose_item" id="choose_item10" tag="14">
								<img src="components/com_choose/assets/images/14.png" alt="桌球拍(運動用品)" />
								<input type="hidden" class="item_code" value="14">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_15">
							<div class="item_text">15.玩具槍</div>
							<div class="choose_item" id="choose_item10" tag="15">
								<img src="components/com_choose/assets/images/15.png" alt="玩具槍" />
								<input type="hidden" class="item_code" value="15">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_16">
							<div class="item_text">16.手機</div>
							<div class="choose_item" id="choose_item10" tag="16">
								<img src="components/com_choose/assets/images/16.png" alt="手機" />
								<input type="hidden" class="item_code" value="16">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_17">
							<div class="item_text">17.湯匙</div>
							<div class="choose_item" id="choose_item9" tag="17">
								<img src="components/com_choose/assets/images/17.png" alt="湯匙" />
								<input type="hidden" class="item_code" value="17">
							</div>
						</div>
					</td>
					<td>
						<div class="choose_block" id="block_18">
							<div class="item_text">18.積木</div>
							<div class="choose_item" id="choose_item10" tag="18">
								<img src="components/com_choose/assets/images/18.png" alt="積木" />
								<input type="hidden" class="item_code" value="18">
							</div>
						</div>
					</td>
				</tr>
				</tbody>
			</table>
			<div align="center">
			<div class="submit_block"><input type="button" id="submit_btn" value="選好了" /></div>
			<div class="reset_block"><input type="button" id="reset_btn" value="重新選擇" /></div>
			</div>
		</div>

		<div class="choose_footer">
		</div>
	</form>
</div>

