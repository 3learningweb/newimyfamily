<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	(function($) {
		$(document).ready(function(){
			$("#date").datepicker({
				dayNames:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
				dayNamesMin:["日","一","二","三","四","五","六"],
				monthNames:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				monthNamesShort:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				prevText:"上月",
				nextText:"次月",
				weekHeader:"週",
				changeYear: true,
				showMonthAfterYear:true,
				dateFormat:"yy-mm-dd",
				
				onSelect: function(dateText, inst) {
					var date = $(this).val();
			        var url = "<?php echo JRoute::_(JURI::base()."components/com_checklist/assets/js/getDate.php ", false); ?>" ;
    			    jQuery.post(url, { date: date }, function(data) {
            			$(".weeks").html("(懷孕週數：" + data + "週)");
        			});
				}
			});
		});
		
		// submit
		$(document).on("click", "#submit_btn", function() {
			var name = $("#name").val();
			var baby = $("#date").val();
			
			$("#profile_form").submit();
		});	
	})(jQuery);
</script>

<div class="com_checklist">
	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_ONE"); ?>
		</div>
	</div>
	
		<div class="pretext">
		<div class="arrow_box">
			<p style="font-size: 17px; line-height: 35px; margin: 10px;">
				每個人常常會有幾本不同的存款簿，在生活中重覆地累積和提款。不論金額怎麼加減，我們總希望存款簿的數字越大越好。<br>
				在我們的家庭裡，也有一本無形的『愛的存款簿』，當我們為家人付出愛、關心、肯定、服務等正向能量，就會存入一筆屬於我們家的『愛的存款』，相反的，如果用錯方式表達家人間的情感，或是彼此感受失望、痛苦，帳戶裡的存款就會變少<br>
				我們都希望存款豐厚，因此我們可以透過了解家人的愛的語言開始，將愛付諸行動！
			</p>
		</div>
	</div>
	
	<div class="checklist_block">
		<form id="profile_form" name="profile_form" method="post" action="<?php echo JRoute::_("index.php?option=com_checklist&view=item&layout=checklist&Itemid={$itemid}", false); ?>">
			<table align="center" class="datatable">
				<tbody>
				<tr>
					<td><a href="https://ilove.moe.edu.tw/index.php?option=com_vquiz&view=quizmanager&id=3" target="_blank"><div class="submit_block"><input type="button" id="submit0" name="submit0" value="愛的語言測驗" /></div></a></td>
					<td>
						<div class="submit_block"><input type="button" id="submit_btn" value="為家人存款" style="width: 155px;" /></div>
					</td>
				</tr>
				</tbody>
			</table>
		</form>
	</div>
</div>

<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_checklist/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<!-- .arrow_box {
		position: relative;
		background: #f7ffb3;
		border: 8px solid #f5c36c;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #f7ffb3;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #f5c36c;
		border-width: 23px;
		margin-top: -23px;
	}
	-->
</style>
