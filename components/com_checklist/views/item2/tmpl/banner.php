<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>

<div class="com_checklist">
	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_TWO"); ?>
		</div>
	</div>

	<div class="banner" align="center">
		<a href="<?php echo JRoute::_("index.php?option=com_checklist&view=item2&layout=langtype&Itemid={$itemid}", false); ?>">
		<img src="<?php echo JURI::root();?>components/com_checklist/assets/images/banner_save.png" alt="我知道家人愛的語言，存款去吧！" /> <br />
		</a>
		<br />
		<a href="https://ilove.moe.edu.tw/index.php?option=com_vquiz&view=quizmanager&id=16&Itemid=266" target="_blank">
		<img src="<?php echo JURI::root();?>components/com_checklist/assets/images/banner_test.png" alt="我還不知道家人愛的語言，做測驗去！" />
		</a>
		<br />
	</div>
</div>