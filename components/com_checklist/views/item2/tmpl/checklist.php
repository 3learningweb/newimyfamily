<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

//家人語言類型
$lang_type = $app->input->get('lang_type');
$app->setUserState('form.checklist.lang_type', $lang_type);

$menu = $app->getMenu();
$menu_title = $menu->getActive()->title;

$error_flag = 0;

$document = JFactory::getDocument();
$document->addCustomTag( '<meta property="og:url" content="'.JURI::root().'家庭活動/家庭存款簿" />' );
$document->addCustomTag( '<meta property="og:type" content="website" />' );
$document->addCustomTag( '<meta property="og:title" content="'.$document->getTitle().'" />' );
$document->addCustomTag( '<meta property="og:description" content="有人認為，普及的網路使用會破壞親子間的關係，但也有 人認爲，透過網路可以獲得很多教養方法與夥伴，即時又快速。您覺得呢？網路有助於育兒嗎？看看大家怎麼說。" />' );
$document->addCustomTag( '<meta property="og:image" content="'.JURI::root().'images/fb/card.jpg" />' );
$document->addCustomTag( '<meta property="og:image:width" content="600" />' );
$document->addCustomTag( '<meta property="og:image:height" content="315" />' );
?>

<!-- include jquery ui -->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<div class="com_checklist">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_TWO"); ?>
		</div>
	</div>
	<form id="card_output" name="output" method="post" action="<?php echo JRoute::_('index.php?option=com_checklist&view=item2&layout=result&Itemid=' . (int) $itemid); ?>">
	<div class="info">
		請想想，這個禮拜為了家人做了哪些付出<br />
		請勾選屬於你和家人的愛的存款清單<br />
		親愛的<input type="text" id="family_name" name="family_name" placeholder="    --請填寫家人暱稱--" maxlength="5">，這個禮拜我為你做了：
	</div>

	<div class="checklist_block">
			<?php
			switch ($lang_type) {

				case "recognition":
					?>

					<!-- 肯定言詞 -->
					<div id='recognition_list'>						
						<div class="list_block">
							<input type="checkbox" id="recognition_1" name="recognition[]" value="我有傳問候的簡訊給你" /> 我有傳問候的簡訊給你
						</div>
						<div class="list_block">
							<input type="checkbox" id="recognition_2" name="recognition[]" value="我有對你說『我愛你』" /> 我有對你說『我愛你』
						</div>
						<div class="list_block">
							<input type="checkbox" id="recognition_3" name="recognition[]" value="我有對你說讚美的話" /> 我有對你說讚美的話
						</div>
						<div class="list_block">
							<input type="checkbox" id="recognition_4" name="recognition[]" value="我有支持你的決定" /> 我有支持你的決定
						</div>
						<div class="list_block">
							<input type="checkbox" id="recognition_5" name="recognition[]" value="我有肯定你的付出" /> 我有肯定你的付出
						</div>
						<div class="list_block">
							<input type="checkbox" id="recognition_6" name="recognition[]" value="請填寫" /> <input type="text" id="recognition_6_value" placeholder="--請填寫--" maxlength="10">
						</div>
						<div class="list_block">
							<input type="checkbox" id="recognition_7" name="recognition[]" value="請填寫" /> <input type="text" id="recognition_7_value" placeholder="--請填寫--" maxlength="10">
						</div>
					</div>

					<?php
					break;
				case "time":
					?>

					<!-- 精心時刻 -->
					<div id="time_list">
						<div class="list_block">
							<input type="checkbox" id="time_1" name="time[]" value="我有和你一起做喜歡的事" /> 我有和你一起做喜歡的事
						</div>
						<div class="list_block">
							<input type="checkbox" id="time_2" name="time[]" value="我有和你一起聊聊天" /> 我有和你一起聊聊天
						</div>
						<div class="list_block">
							<input type="checkbox" id="time_3" name="time[]" value="我有和你出去旅行或是到處走走" /> 我有和你出去旅行或是到處走走
						</div>
						<div class="list_block">
							<input type="checkbox" id="time_4" name="time[]" value="我有為你慶祝重要的日子" /> 我有為你慶祝重要的日子
						</div>
						<div class="list_block">
							<input type="checkbox" id="time_5" name="time[]" value="我有記住你說的話並給予回應" /> 我有記住你說的話並給予回應
						</div>
						<div class="list_block">
							<input type="checkbox" id="time_6" name="time[]" value="請填寫" /> <input type="text" id="time_6_value" placeholder="--請填寫--" maxlength="10">
						</div>
						<div class="list_block">
							<input type="checkbox" id="time_7" name="time[]" value="請填寫" /> <input type="text" id="time_7_value" placeholder="--請填寫--" maxlength="10">
						</div>
					</div>

					<?php
					break;
				case "gift":
					?>

					<!-- 精美禮物 -->
					<div id="gift_list">
						<div class="list_block">
							<input type="checkbox" id="gift_1" name="gift[]" value="我有給你喜歡的東西" /> 我有給你喜歡的東西
						</div>
						<div class="list_block">
							<input type="checkbox" id="gift_2" name="gift[]" value="我有買你愛吃的東西" /> 我有買你愛吃的東西
						</div>
						<div class="list_block">
							<input type="checkbox" id="gift_3" name="gift[]" value="我有送禮物給你" /> 我有送禮物給你
						</div>
						<div class="list_block">
							<input type="checkbox" id="gift_4" name="gift[]" value="我有送卡片給你" /> 我有送卡片給你
						</div>
						<div class="list_block">
							<input type="checkbox" id="gift_5" name="gift[]" value="我有特別注意你喜歡什麼" /> 我有特別注意你喜歡什麼
						</div>
						<div class="list_block">
							<input type="checkbox" id="gift_6" name="gift[]" value="請填寫" /> <input type="text" id="gift_6_value" placeholder="--請填寫--" maxlength="10">
						</div>
						<div class="list_block">
							<input type="checkbox" id="gift_7" name="gift[]" value="請填寫" /> <input type="text" id="gift_7_value" placeholder="--請填寫--" maxlength="10">
						</div>
					</div>

					<?php
					break;
				case "service":
					?>

					<!-- 服務行動 -->
					<div id="service_list">
						<div class="list_block">
							<input type="checkbox" id="service_1" name="service[]" value="我有和你一起做家事" /> 我有和你一起做家事
						</div>
						<div class="list_block">
							<input type="checkbox" id="service_2" name="service[]" value="很累時我有為你捶背" /> 很累時我有為你捶背
						</div>
						<div class="list_block">
							<input type="checkbox" id="service_3" name="service[]" value="我有去接送你" /> 我有去接送你
						</div>
						<div class="list_block">
							<input type="checkbox" id="service_4" name="service[]" value="在你需要時我有幫助你" /> 在你需要時我有幫助你
						</div>
						<div class="list_block">
							<input type="checkbox" id="service_5" name="service[]" value="需要時我有為你服務" /> 需要時我有為你服務
						</div>
						<div class="list_block">
							<input type="checkbox" id="service_6" name="service[]" value="請填寫" /> <input type="text" id="service_6_value" placeholder="--請填寫--" maxlength="10">
						</div>
						<div class="list_block">
							<input type="checkbox" id="service_7" name="service[]" value="請填寫" /> <input type="text" id="service_7_value" placeholder="--請填寫--" maxlength="10">
						</div>
					</div>

					<?php
					break;
				case "touch":
					?>

					<!-- 身體接觸 -->
					<div id="touch_list">
						<div class="list_block">
							<input type="checkbox" id="touch_1" name="touch[]" value="我有親吻你" /> 我有親吻你
						</div>
						<div class="list_block">
							<input type="checkbox" id="touch_2" name="touch[]" value="出門我有牽著你的手" /> 出門我有牽著你的手
						</div>
						<div class="list_block">
							<input type="checkbox" id="touch_3" name="touch[]" value="我有和你做親密接觸" /> 我有和你做親密接觸
						</div>
						<div class="list_block">
							<input type="checkbox" id="touch_4" name="touch[]" value="我有擁抱你" /> 我有擁抱你
						</div>
						<div class="list_block">
							<input type="checkbox" id="touch_5" name="touch[]" value="難過時我有默默拍拍你" /> 難過時我有默默拍拍你
						</div>
						<div class="list_block">
							<input type="checkbox" id="touch_6" name="touch[]" value="請填寫" /> <input type="text" id="touch_6_value" placeholder="--請填寫--" maxlength="10">
						</div>
						<div class="list_block">
							<input type="checkbox" id="touch_7" name="touch[]" value="請填寫" /> <input type="text" id="touch_7_value" placeholder="--請填寫--" maxlength="10">
						</div>
					</div>

					<?php
					break;
				default:
					echo "<strong>請返回上頁重新選擇家人類型。</strong>";
					$error_flag = 1;
			}
			?>

<?php if ($error_flag == 0) { ?>
				<br />
				<div align="center">
					<div class="submit_block"><input type="button" id="submit_btn" value="下載清單" /></div>
				</div>
				<input type="hidden" name="task" value="item2.check" />
				<input type="hidden" name="lang_type" value="<?php echo $lang_type; ?>" />
<?php } ?>
	</div>
	</form>
</div>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {
		
			// submit
			$(document).on("click", "#submit_btn", function() {

				if($("#family_name").val() == ""){
					alert("請填寫家人暱稱");
					return false;
				}
					
				//判斷checklist是否未被選取
				if ( $("input[name='<?php echo $lang_type; ?>[]']:checked").length == 0 ) {
					alert("請至少勾選一項目");
					return false;
				}
				else if ( $("input[name='<?php echo $lang_type; ?>[]']:checked").length > 5 ) {
					alert("最多選擇五項");
					return false;
				}
				else{
					$("#<?php echo $lang_type;?>_6").val($("#<?php echo $lang_type;?>_6_value").val());	
					$("#<?php echo $lang_type;?>_7").val($("#<?php echo $lang_type;?>_7_value").val());	
					$("#card_output").submit();
				}
			
			});
		});
	})(jQuery);
</script>