<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu = $app->getMenu();
$menu_title = $menu->getActive()->title;

$this->bg_img = $app->getUserState('form.checklist.bg_img', '');
?>

<div class="com_checklist">
	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_TWO"); ?>
		</div>
	</div>
	<?php
	if ($this->bg_img) {
		?>
		<div class="image">
			<img width="100%" src="<?php echo JURI::root() . $this->bg_img; ?>" alt="<?php echo JText::_('COM_CHECKLIST_IMAGE_TWO'); ?>">
		</div>
		<br />
		<div align="center">
			<div class="download_card submit_block">
				<a href="<?php echo JURI::root() . $this->bg_img; ?>"><?php echo JText::_('COM_CHECKLIST_DOWNLOAD_TWO'); ?></a>
			</div>
		</div>
	<?php } else { ?>
		<div class="nodata"><?php echo JText::_('COM_CHECKLIST_NODATA'); ?></div>
	<?php } ?>
</div>
