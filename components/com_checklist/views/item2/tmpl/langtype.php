<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

?>

<div class="com_checklist">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<div class="game_page-header">
		<div class="title">
			你家人的愛的語言是哪種類型呢？
		</div>
	</div>
	
	<form id="lang_form" name="lang_form" method="post" action="<?php echo JRoute::_("index.php?option=com_checklist&view=item2&layout=checklist&Itemid={$itemid}", false); ?>">
	<div class="langtype_blocks">
		<div class="langtype_block">
			<div class="title">
				<input type="radio" name="lang_type" value="recognition" checked /> 肯定言詞型
			</div>
			<div class="intro">
				表示家人對於肯定與正向的言詞，特別能夠感受到愛，所以如果你或你的家人屬於這個類型，多用讚美、鼓勵、感謝和肯定的話來表達愛意，常說「我愛你」，就會最有意想不到的效果喔!
			</div>
		</div>
		<div class="langtype_block">
			<div class="title">
				<input type="radio" name="lang_type" value="time" /> 精心時刻型
			</div>
			<div class="intro">
				表示家人特別在意心靈上親密的感受。非常喜歡“一起行動”和”精心安排的相處”，如果你或你的家人屬於這個類型時，多花一些心思和時間陪伴、用心聆聽彼此的心情、一起參與活動，將每個時刻化為最溫馨的時刻。
			</div>
		</div>
		<div class="langtype_block">
			<div class="title">
				<input type="radio" name="lang_type" value="gift" /> 精美禮物型
			</div>
			<div class="intro">
				家人喜歡收到花心思挑選的禮物，如果你或你的家人屬於這個類型時，仔細的觀察需求，挑選適合的禮物或是DIY一些溫馨的小東西，表達心意!
			</div>
		</div>
		<div class="langtype_block">
			<div class="title">
				<input type="radio" name="lang_type" value="service" /> 服務行動型
			</div>
			<div class="intro">
				家人在接受協助與幫忙時，特別能夠感受到愛，你或你的家人屬於這個類型時，可以用實際具體的行動來表達愛意，為對方服務、做事。
			</div>
		</div>
		<div class="langtype_block">
			<div class="title">
				<input type="radio" name="lang_type" value="touch" /> 身體接觸型
			</div>
			<div class="intro">
				透過身體的接觸可以建立親密關係和傳達愛意，如果你或你的家人屬於這個類型時，牽手、擁抱、親吻和肢體碰觸，都能讓家人感到溫馨。
			</div>
		</div>			
		</div>
		<div align="center">
		<div class="submit_block"><input type="button" id="submit_btn" value="我的家人是這個類型，我要製作存款清單卡" /></div>
		</div>
	</form>
	<div class="info">
		其實，每個人對於以上五種類型的表達方式，多少都能感受到愛，只是每個人對於五種方式的比例和感受程度不同罷了。學習可以讓我們更相愛，因此，當我們很在乎家人時，我們可以透過學習，去發現家人間的「愛的語言」，並且將這幾些表達愛的方式成為一種習慣，天天力行。
	</div>
</div>

<script language="JavaScript">
(function($) {
	$(document).ready(function() {

		// submit
		$(document).on("click", "#submit_btn", function() {
				$("#lang_form").submit();
		});
	});
})(jQuery);
</script>