<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		checklist
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu = $app->getMenu();
$menu_title = $menu->getActive()->title;

// 清空session
$app->setUserState('form.checklist.lang_type', null);
$app->setUserState('form.checklist.bg_img', null);
?>
<script>
	(function($) {
		// submit
		$(document).on("click", "#submit_btn", function() {
			$("#profile_form").submit();
		});
	})(jQuery);
</script>

<div class="com_checklist">
	<div class="game_page-header">
		<div class="title">
			<?php echo JText::_("COM_CHECKLIST_TITLE_TWO"); ?>
		</div>
	</div>

	<div class="pretext">
		<div class="arrow_box">
			<p style="font-size: 17px; line-height: 35px; margin: 10px;">
				每個人常常會有幾本不同的存款簿，在生活中重覆地累積和提款。不論金額怎麼加減，我們總希望存款簿的數字越大越好。<br />
				<br />
				在我們的家庭裡，也有一本無形的『愛的存款簿』，當我們為家人付出愛、關心、肯定、服務等正向能量，就會存入一筆屬於我們家的『愛的存款』，相反的，如果用錯方式表達家人間的情感，或是彼此感受失望、痛苦，帳戶裡的存款就會變少<br />
				<br />
				我們都希望存款豐厚，因此我們可以透過了解家人的愛的語言開始，將愛付諸行動！
			</p>
		</div>
	</div>
	<div>
		<img src="<?php echo JURI::root();?>components/com_checklist/assets/images/intro.png" alt="intro" style="max-width:100px">
	</div>
	<div class="checklist_block" style="text-align: right;">
		<form id="profile_form" name="profile_form" method="post" action="<?php echo JRoute::_("index.php?option=com_checklist&view=item2&layout=banner&Itemid={$itemid}", false); ?>">
			<div class="submit_block"><input type="button" id="submit_btn" value="開始準備" style="width: 155px;" /></div>
		</form>
	</div>
</div>

<style>
	.pretext {
		margin-bottom: 20px;
	}
</style>
