<?php

/**
 * @version		: controller.php 2015-09-22 21:06:39$
 * @author		EFATEK 
 * @package		Checklist
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * CHECKLIST Component Controller
 */
class ChecklistController extends JControllerLegacy {

	
}