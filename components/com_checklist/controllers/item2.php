<?php

/**
 * @version     1.0.0
 * @package     com_checklist
 * @copyright   Efatek Inc. Copyright (C) 2015. All rights reserved.
 * @license     http://www.efatek.com
 * @author      Efatek <rene_chen@efatek.com> - http://www.efatek.com
 */
// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * Items list controller class.
 */
class ChecklistControllerItem2 extends ChecklistController {

	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'List', $prefix = 'Checklist') {
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}

	public function check() {
		$app = JFactory::getApplication();
		$jinput = $app->input;
		$Itemid = $jinput->getInt('Itemid');
		$post = $jinput->getArray($_POST);
		$lang_type = $app->getUserState('form.checklist.lang_type', $post["lang_type"]);
		
		//列出存款清單完成內容
		$post["content"] = "";		
		foreach($post[$lang_type] as $key => $content):
			$post["content"] .= ($key+1).". ".$content."\n";
		endforeach;

		//找出語言類型對應背景圖
		if(isset($post["family_name"]))
			$background_image = JPATH_SITE."/components/com_checklist/assets/images/".$lang_type."2.png";
		else 
			$background_image = JPATH_SITE."/components/com_checklist/assets/images/".$lang_type.".png";
			
		//設定合成後相片名、儲存位置
		$new_img = time() . ".jpg";
		$bg_img = "tmp/checklist/" . $new_img;

		// 合成存款清單卡，文字座標固定(295, 125)
		if($lang_type == 'recognition' || $lang_type == 'service')
			$this->mergeImg($background_image, $post["content"], JPATH_SITE . "/tmp/checklist", $new_img, 295, 140, $post["family_name"]);
		else
			$this->mergeImg($background_image, $post["content"], JPATH_SITE . "/tmp/checklist", $new_img, 255, 140, $post["family_name"]);

		$app->setUserState('form.checklist.bg_img', $bg_img);

		$link = "index.php?option=com_checklist&view=item2&layout=result&Itemid={$Itemid}";

		$this->setRedirect($link);
	}

	// 合併 (修改自edu001m com_postcard controller gviewcard.php)
	function mergeImg($background_img, $content, $save_path, $new_img, $msg_pos_x, $msg_pos_y, $family_name = null) {

		$app = JFactory::getApplication();
		$lang_type = $app->getUserState('form.checklist.lang_type', $post["lang_type"]);
		$font = JPATH_SITE . "/components/com_checklist/assets/wqy-zenhei.ttc"; // 字型

		//得到底圖info，目前底圖固定是png檔
//		$dst_im = imagecreatefromjpeg($background_img);
		$dst_im = imagecreatefrompng($background_img);

		if($family_name != null)
		{
			$text_color = imagecolorallocate($dst_im, 255, 255, 0);  //設定文字顏色為 #272727
			
			if($lang_type == 'recognition' || $lang_type == 'service')
			{
				imagettftext($dst_im, 18, 0, 295, 100, $text_color, $font, "親愛的".$family_name);
				imagettftext($dst_im, 18, 0, 296, 100, $text_color, $font, "親愛的".$family_name);
				imagettftext($dst_im, 18, 0, 297, 100, $text_color, $font, "親愛的".$family_name);
			}
			else
			{
				imagettftext($dst_im, 18, 0, 255, 100, $text_color, $font, "親愛的".$family_name);
				imagettftext($dst_im, 18, 0, 256, 100, $text_color, $font, "親愛的".$family_name);
				imagettftext($dst_im, 18, 0, 257, 100, $text_color, $font, "親愛的".$family_name);
			}
		}
		
		//合併文字
		$text_color = imagecolorallocate($dst_im, 39, 39, 39);  //設定文字顏色為 #272727
		imagettftext($dst_im, 16, 0, $msg_pos_x, $msg_pos_y, $text_color, $font, $content);

		$filename = $save_path . "/" . $new_img; // 合併後的圖片檔名

		//輸出合併後圖片
		if (imagejpeg($dst_im, $filename)) {
			imagedestroy($dst_im);
			return true;
		} else {
			return false;
		}
	}

}