<?php
/**
 * @version		: default.php 2015-09-21 10:06:39$
 * @author		EFATEK 
 * @package		growthinspection
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');


?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$ = jQuery;
	$(document).ready(function(){
		// date
		var opt={dayNames:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
				 dayNamesMin:["日","一","二","三","四","五","六"],
				 monthNames:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				 monthNamesShort:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				 prevText:"上月",
				 nextText:"次月",
				 weekHeader:"週",
				 changeYear: true,
				 showMonthAfterYear:true,
				 dateFormat:"yy-mm-dd",
				 minDate: new Date(1930, 1, 1),
				 maxDate: new Date()
				 };

		$("#datepicker1").datepicker(opt);
		$("#datepicker2").datepicker(opt);
	});

	function send_date(_type) {
		$("#type").val(_type);

		$("#mainForm").submit();
	}
</script>

<div class="com_lovetime">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<form id="mainForm" method="post" action="<?php echo JRoute::_('index.php?option=com_growthinspection&view=game&layout=quiz&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">寶寶的發展檢核</div>
        <div class="contentTableMain">
			<div class="loveBox">
				<div class="loveCt">
							
					<div class="component_intro">
						<div class="intro_text">
							什麼是寶寶的發展關鍵呢？ 寶寶現在的發展正常嗎？<br/>
							大多數的健康寶寶，在成長的過程，<br/>
							在動作、語言、人際互動中，都會正常發展的，<br/>
							我們可以透過這個檢核，了解寶寶的發展。<br/>
							別過度焦慮或對寶寶做出不合理的要求，各位親愛的爸媽，<br/>
							我們一起來看看寶寶的發展里程碑吧！
						</div>
								
						<div class="intro_image">
							<img id="choose_intro_image" src="components/com_lovetime/assets/images/intro_image.png" alt="<?php echo $menu_title; ?>" />
						</div>
					</div>

					<div class="loveDate">
						<form id="profile_form" name="profile_form" method="post" action="<?php echo JRoute::_("index.php?option=com_growthinspection&view=game&Itemid={$itemid}", false); ?>">
							<span style="font-size: 16px;"><b>請輸入寶寶的生日：</b></span><br/>
							<input type="text" id="datepicker1" name="love_date" value="<?php echo date("Y-m-d"); ?>" size="15" maxlength="10">
							<div class="submit_block"><input type="button" value="開始計算" onClick="send_date('love')"></div>
						</form>
					</div>
					
				</div>
			</div>
        </div>
        <div class="contentTableDown"></div>
    </div>
		<input type="hidden" name="type" id="type" value="">
	</form>
</div>