<?php
/**
 * @version		: default.php 2015-09-21 10:06:39$
 * @author		EFATEK 
 * @package		growthinspection
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$model = &$this->getModel();
$quiz=$model->getQuiz();
if($quiz != 0) {
	$quiz1=$quiz[0];
	$quiz2=$quiz[1];
	$quiz3=$quiz[2];
	$baby_icon = $quiz[3];
	$age = $quiz[4];
}
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	function send_date() {
//		$("#type").val(_type);

		$("#mainForm2").submit();
	}
</script>

<style>
	.baby_icon {
		margin: 20px;
	}
	
	.baby_icon img {
		margin-right: 10px;
	}
	
	.baby_icon table {
		color: #0F7F29;
		font-size: 18px;
		font-weight: bold;
	}
	
	.baby_icon table span {
		font-size: 30px;	
	}
	
	.submit_block {
		margin: 10px 0; 
		float: right;
	}
	
	.check_block {
		text-align: left;
		background-color: #f2fdd9;
		border:1px solid #c3de85;
	}
	
	.check_icon {
    	width: 190px;
    	margin: 10px 0;
    	text-align: center;
		display: inline-block;
	}
	
	.check_table {
		margin: 10px 0;
		font-size: 16px;
		font-weight: bold;
		vertical-align: top;
		display: inline-block;
	}
	
	.check_table table {
		width: 465px;
	}

	.check_table input[type='radio'] {
		width: 21px;
		height: 21px;
		position: relative;
    	top: 5px;
	}

	.check_table tr {
		line-height: 25px;
	}
</style>

<div class="com_lovetime">
	<form id="mainForm2" method="post" action="<?php echo JRoute::_('index.php?option=com_growthinspection&view=game&layout=result&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">寶寶的發展檢核</div>
        <?php if($quiz != 0) { ?>
	        <div class="baby_icon">
	        	<table>
	        		<tr>
	        			<td><img src="components/com_growthinspection/assets/images/<?php echo $baby_icon; ?>.png" alt="baby_icon" /></td>
	        			<td><?php echo $age; ?></td>
	        		</tr>
	        	</table>
	        </div>
	        
	        <div class="contentTableMain">
				<div class="loveBox">
					<div class="loveCt">
						<div style="text-align: center">
							<div class="check_block">
								<div class="check_icon">
									<img src="components/com_growthinspection/assets/images/icon_01.png">
								</div>
								<div class="check_table">
									<table>
									<?php
										$i=0;
										foreach($quiz1 as $key=>$value){
											echo "<tr><td width='80%'>．{$value}</td><td><input type='radio' name='quiz1[{$i}]' value='Y'><span>是</span></td><td><input type='radio' name='quiz1[{$i}]' value='N'><span>否</span></td></tr>";
											$i++;
										}
									?>
									</table>
								</div>
							</div><br>
							<div class="check_block">
								<div class="check_icon"><img src="components/com_growthinspection/assets/images/icon_02.png"></div>
								<div class="check_table">
									<table>
									<?php
										$j=0;
										foreach($quiz2 as $key=>$value){
											echo "<tr><td width='80%'>．{$value}</td><td><input type='radio' name='quiz2[{$j}]' value='Y'><span>是</span></td><td><input type='radio' name='quiz2[{$j}]' value='N'><span>否</span></td></tr>";
											$j++;
										}
									?>
									</table>
								</div>
							</div><br>
							<div class="check_block">
								<div class="check_icon"><img src="components/com_growthinspection/assets/images/icon_03.png"></div>
								<div class="check_table">
									<table>
									<?php
										$k=0;
										foreach($quiz3 as $key=>$value){
											echo "<tr><td width='80%'>．{$value}</td><td><input type='radio' name='quiz3[{$k}]' value='Y'><span>是</span></td><td><input type='radio' name='quiz3[{$k}]' value='N'><span>否</span></td></tr>";
											$k++;
										}
									?>
									</table>
								</div>
							</div>
						</div>
	
						<div class="submit_block"><input type="submit" value="開始檢核"></div>
	
					</div>
				</div>
	        </div>
	    <?php }else{ ?>
	    	<div>您的寶寶已經超過六歲摟！</div>
	    	<div class="submit_block"><input type="button" onclick="javascript:history.go(-1)" value="重新輸入生日"></div>
	    <?php } ?>
        <div class="contentTableDown"></div>
    </div>
		<input type="hidden" name="q1cnt" id="q1cnt" value="<?php echo $i;?>">
		<input type="hidden" name="q2cnt" id="q2cnt" value="<?php echo $j;?>">
		<input type="hidden" name="q3cnt" id="q3cnt" value="<?php echo $k;?>">
	</form>
</div>