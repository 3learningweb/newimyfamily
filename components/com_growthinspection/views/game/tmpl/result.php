<?php
/**
 * @version		: result.php 2015-09-21 10:06:39$
 * @author		EFATEK 
 * @package		growthinspection
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$q1cnt = $app->input->getInt('q1cnt');
$q2cnt = $app->input->getInt('q2cnt');
$q3cnt = $app->input->getInt('q3cnt');
$type = $app->input->getString('type');
$quiz1 = $app->input->get('quiz1', array(), 'ARRAY');
$quiz2 = $app->input->get('quiz2', array(), 'ARRAY');
$quiz3 = $app->input->get('quiz3', array(), 'ARRAY');
$qcnt=$q1cnt+$q2cnt+$q3cnt;
$qcnt1Y=0;
$qcnt1N=0;
$qcnt2Y=0;
$qcnt2N=0;
$qcnt3Y=0;
$qcnt3N=0;
foreach($quiz1 as $key=>$value){
	if($value=='Y') $qcnt1Y++;
	if($value=='N') $qcnt1N++;
}
foreach($quiz2 as $key=>$value){
	if($value=='Y') $qcnt2Y++;
	if($value=='N') $qcnt2N++;
}
foreach($quiz3 as $key=>$value){
	if($value=='Y') $qcnt3Y++;
	if($value=='N') $qcnt3N++;
}
$qcntY=$qcnt1Y+$qcnt2Y+$qcnt3Y;
$qcntN=$qcnt1N+$qcnt2N+$qcnt3N;

if($qcnt==$qcntY){
	$html_file =  "1.html";
}
elseif($qcnt==$qcntN){
	$html_file =  "2.html";
}
else{
	$html_file =  "3.html";
}

?>
<script>
	$ = jQuery;
	$(document).ready(function(){
		
	});
</script>

<div class="com_lovetime">
	<form id="mainForm" method="post" action="<?php echo JRoute::_('index.php?option=com_growthinspection&view=game&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">寶寶的發展檢核</div>
        <div class="contentTableMain">
			<div class="loveBox">
				<div class="loveCt">
					
					<div class="component_intro">
						<div class="intro_text">
							<?php //echo $subject; ?>
							<?php include('components/com_growthinspection/assets/html/'. $html_file); ?>
						</div>
					</div>
					
				</div>
				
				<div class="submit_block" style="margin-top: 20px;"><input type="submit" value="重新計算"></div>
			</div>
			
        </div>
        <div class="contentTableDown"></div>
    </div>
	</form>
</div>
