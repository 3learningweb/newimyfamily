<?php
/**
 * @version		: default.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		truelove
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>

<script language="JavaScript">	
(function($) {
	$(document).ready(function() {
		$("#submit_btn").on("click", function() {
			var url = "<?php echo JRoute::_("index.php?option=com_truelove&view=game&layout=game&Itemid={$itemid}", false); ?>";
			document.location.href = url;
		});
	});	
})(jQuery);
</script>

<link href="components/com_truelove/assets/css/main.css" rel="stylesheet" type="text/css" />

<div class="com_truelove" id="com_truelove">
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	<div id="navContent">
		<div class="intro_text">
			<?php echo JText::_("COM_TRUELOVE_INTRO_TEXT"); ?>
		</div>
	</div>
	<div class="start">
		<div class="submit_block"><input type="button" id="submit_btn" value="開始遊戲" /></div>
	</div>
	<div id="truelove_bg"></div>
</div>