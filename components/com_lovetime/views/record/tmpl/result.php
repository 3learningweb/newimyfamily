<?php
/**
 * @version		: default.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		lovetime
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$type = $app->input->getString('type');
$start_date = $app->input->getString('love_date');

$start_date = ($start_date) ? $start_date : date("Y-m-d");
$diff_days = ceil((time() - strtotime($start_date)) / 86400);
$diff_month = ceil($diff_days/30);
$diff_year = floor($diff_days/365);
$title = "結婚紀念日";

	if ($diff_year == 0) {//new wed
		$subject = "<h6>•	恭喜你正處於新婚階段！</h6>";
		$html_file = $diff_year.".html";
	} else if ($diff_year > 0 && $diff_year <= 24) {//1-24
		$subject = "<h6>•	恭喜你結婚滿".$diff_year."年！</h6>";
		$html_file = $diff_year.".html";
	} else if ($diff_year > 24 && $diff_year <= 29) {//25-29
		$subject = "<h6>•	恭喜你結婚滿".$diff_year."年！</h6>";
		$html_file = "25.html";
	} else if ($diff_year > 29 && $diff_year <= 39) {//30-39
		$subject = "<h6>•	恭喜你結婚滿".$diff_year."年！</h6>";
		$html_file = "30.html";
	} else if ($diff_year > 39 ) {//40up
		$subject = "<h6>•	恭喜你結婚滿".$diff_year."年！</h6>";
		$html_file = "40.html";
	} else {
		$subject = "<h6>•	恭喜你即將結婚！</h6>";//possible
		$html_file = "0.html";
	}

?>
<script>
	$ = jQuery;
	$(document).ready(function(){
		
	});
</script>

<div class="com_lovetime">
	<form id="mainForm" method="post" action="<?php echo JRoute::_('index.php?option=com_lovetime&view=record&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">結婚紀念日</div>
        <div class="contentTableMain">
			<div class="loveBox">
				<div class="loveTop">
					<h6><?php echo $title; ?></h6>
				</div>
				<div class="loveCt">
					<?php echo $subject; ?>
					<br>
					<?php include('components/com_lovetime/assets/html/'. $html_file); ?>
				</div>
				<div class="submit_block"><input type="submit" value="重新計算"></div>
			</div>
			
        </div>
        <div class="contentTableDown"></div>
    </div>
	</form>
</div>
