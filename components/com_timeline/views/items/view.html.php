<?php
/**
 * @version		: view.html.php 2015-10-21 05:06:09$
 * @author		efatek 
 * @package		com_timeline
 * @copyright	Copyright (C) 2015- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class TimelineViewItems extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$config = JFactory::getConfig();
		$config->set('list_limit', 10);
		$layout = $this->getLayout();	

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');
		$this->items	= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		if($layout == "form") {
			$this->displayForm($tpl);
		}
		
		// Display the view
		parent::display($tpl);

	}
	
	function displayForm($tpl = null) {
		$app = JFactory::getApplication();
		$id = $app->input->getInt("id");
		if($id) {
			$this->item = $this->get('Item');
		}
	}
}
