<?php 
/**
 * @version		: default.php 2015-01-22 00:00:00$
 * @author		EFATEK 
 * @package		com_share
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file

defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid	= $app->input->getInt('Itemid');
$id	= $app->input->getInt('id');
$user = JFactory::getUser();
?>
<div class="com_timeline">
	<div class="page-header">
		<div class="title">
			<?php if(!$id){echo "新增";}else{echo "修改";} ?> 寶寶成長日記
		</div>
	</div>
	<div class="content">
	<form id="timeline_form" name="timeline_form" method="post" enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_timeline&view=items&layout=form&Itemid='. (int) $itemid, false); ?>">
		<!-- 成長日記表格 -->
		<table align="center" class="form_table">
			<!-- 日記標題 -->
			<tr>
				<th class="form_title">標題：</th>
				<td class="form_text">
					<input type="text" class="form_input" id="title" name="title" value="<?php echo $this->item->title; ?>" />
				</td>
			</tr>
			
			<!-- 日期 -->
			<tr>
				<th class="form_title">日期：</th>
				<td class="form_text">
					<?php 
						if($id) {
							$date = $this->item->date;
						}else{
							$date = date("Y-m-d");
						}
						
						echo JHTML::_('calendar', "{$date}", 'date', 'date', '%Y-%m-%d', array('class' => 'formItem', 'size' => '10', 'maxlength' => '10', 'style' => 'width:120px;'));
					?>
				</td>
			</tr>
			
			<!-- 描述 -->
			<tr>
				<th class="form_title">描述：</th>
				<td class="form_text">
					<textarea class="form_input" id="description" name="description" rows="6"><?php echo $this->item->description; ?></textarea>
				</td>
			</tr>
			
			<!-- 寶寶照片 -->
			<tr>
				<td></td>
				<td>
					<?php if($this->item->photo) { ?>
						<img id="baby_photo" src="<?php echo $this->item->photo; ?>" alt="<?php echo $this->item->title; ?>" />
					<?php } ?>
				</td>
			</tr>
			<tr>
				<th class="form_title">寶寶照片：</th>
				<td class="form_text">					
					<input type="file" id="photo" name="photo" /><br/>
				</td>
			</tr>
			
			<!-- submit -->
			<tr>
				<td colspan="2" align="center"><br/>
					<!-- submit -->
					<input id="submit_btn" type="button" onclick="sendForm()" onkeypress="sendForm()" value="送出" />
					<input type="reset" value="清除" />
					<input type="hidden" name="user_id" value="<?php echo $user->get('id'); ?>" />
					<input type="hidden" name="task" value="items.save" />
					<?php if($id) { ?>
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
					<?php } ?>
				</td>
			</tr>
		</table>
	</form>
	</div>
</div>

<script language="JavaScript">	
	function sendForm() {
		var id = "<?php echo $id; ?>";
		
		if(!jQuery("#title").val()) {
			alert("請輸入標題");
			jQuery("#title").focus();
			return false;
		}else if(!jQuery("#date").val()) {
			alert("請輸入日期");
			jQuery("#date").focus();
			return false;
		}else if(!jQuery("#description").val()) {
			alert("請輸入描述");
			jQuery("#description").focus();
			return false;
		}else if(!id && !jQuery("#photo").val()) {
			alert("請選擇照片上傳");
			jQuery("#photo").focus();
			return false;
		}
		
		if(confirm("是否確定送出？")) {
			jQuery("#timeline_form").submit();
		}
		
	}

</script>