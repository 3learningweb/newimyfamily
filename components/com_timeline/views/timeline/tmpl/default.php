<?php
/**
 * @version		: default.php 2015-10-22 21:06:39$
 * @author		EFATEK 
 * @package		com_timeline
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$id = $app->input->getInt('id');

// 短網址
$current_url = JURI::current() . "?view=timeline&id={$id}";
$url = file_get_contents( 'http://tinyurl.com/api-create.php?url='.urlencode($current_url));

?>

<link type="text/css" rel="stylesheet" href="components/com_timeline/assets/css/flat.css" />
<link type="text/css" rel="stylesheet" href="components/com_timeline/assets/css/style.css" />
<link type="text/css" rel="stylesheet" href="components/com_timeline/assets/css/lightbox.css" />
<link type="text/css" rel="stylesheet" href="components/com_timeline/assets/css/jquery.mCustomScrollbar.css" />

<script type="text/javascript" src="components/com_timeline/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="components/com_timeline/assets/js/image.js"></script>
<script type="text/javascript" src="components/com_timeline/assets/js/lightbox.js"></script>
<script type="text/javascript" src="components/com_timeline/assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="components/com_timeline/assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="components/com_timeline/assets/js/jquery.timeline.min.js"></script>
<script type="text/javascript" src="components/com_timeline/assets/js/jquery.mCustomScrollbar.js"></script>

<style>
	#left_menu {
		display: none;
	}
	#content, .return {
		margin-left: 0px !important;
		padding-left: 0px !important;
	}
</style>

<div class="com_timeline">
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			分享：<?php echo JHtml::_('toolsbar._timeline'); ?>
		</ul>
		<!-- <div>
			連結：
			<a href="<?php echo $url; ?>" title="寶寶成長日記" target="_break">
				<?php echo $url ?>
			</a>
		</div> -->
	</div>

	<div class="timelineLoader">
		<img src="components/com_timeline/assets/images/loadingAnimation.gif" />
	</div>
	<!-- BEGIN TIMELINE -->
	<div class="timelineFlat tl1">
		<?php foreach($this->items as $key => $item) { ?>
			<?php 
				$date_arr = explode("-", $item->date); 
				$date = implode("/", array_reverse($date_arr));
				if(count($this->items) == 1 && $key == 0) {
					$first_date = $date;
				}elseif(count($this->items) > 1 && $key == 1){
					$first_date = $date;
				}
			?>
			<div class="item" data-id="<?php echo $date; ?>" data-description="<?php echo $item->title; ?>">
				<a class="image_rollover_bottom con_borderImage" data-description="ZOOM IN" href="<?php echo $item->photo; ?>" rel="lightbox[timeline]">
					<img src="<?php echo $item->photo; ?>" alt="<?php echo $item->title; ?>" />
				</a>
				<h2><?php echo $date_arr["1"] . "月" . $date_arr["2"] . "日"; ?></h2>
				<span><?php  echo preg_replace("/\\n/", "<br />", $item->description); ?></span>
			</div>
			<div class="item_open" data-id="<?php echo $date; ?>" data-access="ajax-content-no-image.html">
				<div class="item_open_content">
					<img class="ajaxloader" src="components/com_timeline/assets/images/loadingAnimation.gif" alt="loading" />
				</div>
			</div>
		<?php } ?>
	</div>
	<!-- END TIMELINE -->
</div>

<script type="text/javascript">
	$(window).load(function() {
		// light
		$('.tl1').timeline({
			openTriggerClass : '.read_more',
			startItem : '<?php echo $first_date; ?>',
			closeText : 'x',
			ajaxFailMessage: 'This ajax fail is made on purpose. You can add your own message here, just remember to escape single quotes if you\'re using them.'
		});
		$('.tl1').on('ajaxLoaded.timeline', function(e){
			var height = e.element.height()-60-e.element.find('h2').height();
			e.element.find('.timeline_open_content span').css('max-height', height).mCustomScrollbar({
				autoHideScrollbar:true,
				theme:"light-thin"
			});	
		});
	});
	
		
</script>
