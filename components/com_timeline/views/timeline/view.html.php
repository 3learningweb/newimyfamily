<?php
/**
 * @version		: view.html.php 2015-10-22 05:06:09$
 * @author		efatek 
 * @package		com_timeline
 * @copyright	Copyright (C) 2015- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

/**
 * HTML View class for the gmap Component
 */
class TimelineViewTimeline extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');
		$this->items	= $this->get('Items');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the view
		parent::display($tpl);
	}
	

}
