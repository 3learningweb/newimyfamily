<?php

/**
 * @version     1.0.0
 * @package     com_cinema
 * @copyright   Efatek Inc. Copyright (C) 2012. All rights reserved.
 * @license     http://www.efatek.com
 * @author      Efatek <sam@efatek.com> - http://www.efatek.com
 */
// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.filesystem.file' );

require_once JPATH_COMPONENT . '/controller.php';

/**
 * Items list controller class.
 */
class PostcardsControllerJewelry extends PostcardsController {

	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function getModel($name = 'jewelry', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function check() {
		$nowtime=time();
		$app = JFactory::getApplication();
		$jinput = $app->input;
		$Itemid = $jinput->getInt('Itemid');
		$post = $jinput->getArray($_POST);
		$cat_id = JRequest::getInt('cat_id');
		$params=array("efu_parent"=>"images","efu_folder"=>"","efu_maxsize"=>"1000000","efu_custom"=>"1","efu_label"=>"\u8acb\u9078\u64c7\u6a94\u6848=>","efu_button"=>"\u4e0a\u50b3\u6a94\u6848","efu_question"=>"Replace existing files with uploaded files?","efu_yes"=>"Yes","efu_no"=>"No","efu_filetypes"=>"image\/gif;image\/jpeg;image\/pjpeg;image\/png","efu_replace"=>"0","efu_variable"=>"fileToUpload","efu_multiple"=>"5","moduleclass_sfx"=>"","layout"=>"_=>default","efu_user"=>"0","module_tag"=>"div","bootstrap_size"=>"0","header_tag"=>"h3","header_class"=>"","style"=>"0","nowtime"=>$nowtime,"cat_id"=>$cat_id);
		$this->getFileToUpload($params);

		//自訂祝福
		$post["content"] = JFilterOutput::cleanText($post["custom_text"]);
		$app->setUserState('form.postcards.custom_text', $post["custom_text"]);

		//賀卡ID
		if ($post["message_type"] == "reference") {
			//範本賀卡ID
			$cardid = $post["message_refer"];
		} else {
			//自訂文字賀卡ID
			$cardid = $post["customize_id"];
		}
		//將賀卡ID存入session
		$app->setUserState('form.postcards.card_id', $cardid);

		$msg = '';

/*		if ($post["message_type"] == "customize" && $post["content"] == "") {
			//自訂祝福卻未填寫
			$msg = JText::_('COM_POSTCARDS_FORM_REUIRED');
		} elseif ($post["message_type"] == "customize" && utf8_strlen($post["content"]) > 15) {
			//自訂祝福字數太長
			$msg .= JText::_('COM_POSTCARDS_FORM_CONTENT_LONG');
		}*/


		if ($msg != "") {
			//資料填寫有問題
			$this->setRedirect("index.php?option=com_postcards&view=gform&Itemid={$Itemid}", $msg);

			return;
		} else {
			$path='/components/com_postcards/upload/'.$params['nowtime'];
			$apath=JPATH_SITE.$path;
			$files = explode("\n", trim(`find -L $apath`)); // -L follows symlinks

			foreach($files as $s=>$file){
				if($s>0){
					$menu = $app->getMenu();
					$par = $menu->getParams($Itemid);

					//撈出賀卡圖
					$model = $this->getModel();
					$image = $model->getImage($cat_id);


					$pic = $jinput->files->get('user_image');
		//			$new_img = time() . ".jpg";
		//			$bg_img = "tmp/" . $new_img;
					$bg_img=$path.'/animegif.gif';
					// 判斷是否為jpg，若否則轉換
					$img_type = exif_imagetype($file);
					if ($img_type == 1) {
						$im = @imagecreatefromgif($file);
						imagejpeg($im, $file, $quality);
					}

					if ($img_type == 3) {
						$im = @imagecreatefrompng($file);
						imagejpeg($im, $file, $quality);
					}
					// 合成照片
		//			$this->mergeImg($image->image, $pic["tmp_name"], $post["content"], $image->pic_pos_x, $image->pic_pos_y, $image->pic_width, $image->pic_height, JPATH_SITE . "/tmp", $new_img, $image->msg_pos_x, $image->msg_pos_y);

					$this->mergeImg($image->image, $file, $post["content"], $image->pic_pos_x, $image->pic_pos_y, $image->pic_width, $image->pic_height, JPATH_SITE . $path, $file, $image->msg_pos_x, $image->msg_pos_y);
				}
			}
			$this->createGif(JPATH_SITE.$path);
			$app->setUserState('form.postcards.bg_img', $bg_img);
			$app->setUserState('form.postcards.nowtime', $nowtime);

			$link = "index.php?option=com_postcards&view=jewelry&layout=combind&Itemid={$Itemid}";

			$this->setRedirect($link);
		}
	}

	// 合併
	function mergeImg($background_img, $pic_img, $content, $position_x, $position_y, $pic_width, $pic_height, $save_path, $new_img, $msg_pos_x, $msg_pos_y) {

		$font = JPATH_SITE . "/components/com_postcards/assets/wqy-zenhei.ttc"; // 字型
		$alpha = 100;	//浮水印透明度
		$markerImg_width = $pic_width;  // 小圖-寬
		$markerImg_height = $pic_height; // 小圖-高
		//得到底圖info
		$dst_im = imagecreatefromjpeg($background_img);

		// 取得上傳的圖像
		$src_img_filename = pathinfo($pic_img, PATHINFO_BASENAME);

		// 進行縮圖
		$this->thumbnailImg($pic_img, $save_path, $src_img_filename, $markerImg_width, $markerImg_height);

		$thumbnail_src = $save_path . "/" . $src_img_filename;

		$src_im = imagecreatefromjpeg($thumbnail_src);

		$size = getimagesize($thumbnail_src); // 取得縮圖後的大小
		if ($size[0] > $markerImg_width) {
			$size_width = $markerImg_width;
		} else {
			$size_width = $size[0];
		}

		if ($size[1] > $markerImg_height) {
			$size_height = $markerImg_height;
		} else {
			$size_height = $size[1];
		}

		//合併景點圖片
		imagecopymerge($dst_im, $src_im, $position_x, $position_y, 0, 0, $size_width, $size_height, $alpha);

		//合併文字
		$text_color = imagecolorallocate($dst_im, 0, 0, 0);  //設定文字顏色為黑色
		imagettftext($dst_im, 20, 0, $msg_pos_x, $msg_pos_y, $text_color, $font, $content);

		$filename = $new_img; // 合併後的圖片檔名
		//輸出合並後水印圖片
		if (imagejpeg($dst_im, $filename)) {
			imagedestroy($dst_im);
			imagedestroy($src_im);

//			unlink($thumbnail_src);

			return true;
		} else {
			return false;
		}
	}

	/*	 * * 縮圖 ** */

	function thumbnailImg($filename, $dest_path, $dest_filename, $small_w = 300, $small_h = 300) {
		// 取得上傳圖片
		$src = imagecreatefromjpeg($filename);

		// 取得來源圖片長寬
		$src_w = imagesx($src);
		$src_h = imagesy($src);

		// 儲存縮圖到指定 thumb 目錄
		if (!is_dir($dest_path)) {
			@mkdir($dest_path, 0755);
		}

		// 查看原始圖檔是否達到指定大小，未達到，則以原檔為新圖
		if (($src_w < $small_w) && ($src_h < $small_h)) {
			copy($filename, $dest_path . "/" . $dest_filename);
			return;
		}

		// 假設要長寬不超過指定大小
		if ($src_w > $src_h) {
			$thumb_w = $small_w;
			$thumb_h = intval($src_h / $src_w * $small_w);
		} else {
			$thumb_h = $small_h;
			$thumb_w = intval($src_w / $src_h * $small_h);
		}
		// 建立縮圖
		$thumb = imagecreatetruecolor($thumb_w, $thumb_h);

		// 開始縮圖
		imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);

		imagejpeg($thumb, $dest_path . "/" . $dest_filename);
	}

	public function send() {
		$app = JFactory::getApplication();
		$jinput = $app->input;
		$post = $jinput->getArray($_POST);

		$Itemid = $jinput->getInt('Itemid');

		$post["subject"] = JFilterOutput::cleanText($post["subject"]);
		$app->setUserState('form.postcards.friend_email', $post["friend_email"]);
		$app->setUserState('form.postcards.name', $post["name"]);
		$app->setUserState('form.postcards.email', $post["email"]);
		$app->setUserState('form.postcards.subject', $post["subject"]);

		$msg = '';
		if ($post["friend_email"] == "" || $post["name"] == "" || $post["email"] == "" || $post["subject"] == "") {
			$msg = JText::_('COM_POSTCARDS_FORM_REUIRED');
		} else {
			$reg = "/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/";
			if (!preg_match($reg, $post["friend_email"]) || !preg_match($reg, $post["email"])) {
				$msg = JText::_('COM_POSTCARDS_FORM_EMAIL');
			}
		}

		if ($msg != "") {
			$this->setRedirect("index.php?option=com_postcards&view=gviewcard&Itemid={$Itemid}", $msg);

			return;
		} else {
			$menu = $app->getMenu();
			$par = $menu->getParams($Itemid);

			$sitedomain = substr_replace(JURI::root(), '', -1, 1);

			$html = array();
			$html [] = '<div style="width: 95%; margin: 0 auto; background: #d3ece4; border: 1px solid #78b5a2; padding: 10px;">';
			$html [] = "<p>" . sprintf(JText::_('COM_POSTCARDS_SEND_CONTENT'), $post["name"]) . "</p><br>";
			if ($par->get('card_type') == "sticker") {
				$attachment = JPATH_SITE . "/" . $app->getUserState('form.postcards.bg_img', '');
			} else {

				$html [] = '<div><img src="' . JURI::root() . $app->getUserState('form.postcards.bg_img', '') . '"></div>';
				$html [] = "<p>" . $app->getUserState('form.postcards.content', '') . "</p>";
				$attachment = "";
			}


			$html [] = '<div style="text-align: right;"><a style="color: #265c4b; text-decoration: none;" href="' . JURI::root() . '">' . JText::_('COM_POSTCARDS_SEND_FROM_SITE') . '</a></div>';
			$html [] = '</div>';
			$body = implode('<br/>', $html);


			$sendEmailStatus = JUtility::sendMail($post['email'], $post["name"], $post['friend_email'], $post["subject"], $body, 1, "", "", $attachment);

			if ($sendEmailStatus) {
				$msg = JText::_('COM_POSTCARDS_SEND_OK');
				$app->setUserState('form.postcatds', null);

				unlink($attachment);

				$this->setRedirect(JRoute::_("index.php?option=com_postcards&view=greeting&Itemid=" . $Itemid, false), $msg);

				return;
			} else {
				$msg = JText::_('COM_POSTCARDS_SEND_ERROR');

				$this->setRedirect(JRoute::_("index.php?option=com_postcards&view=gviewcard&Itemid=" . $Itemid, false), $msg);

				return;
			}
		}
	}
	function getFileToUpload(&$params)
	{
		$_POST['answer']=0;
		$result = array();
//		$path = JPATH_SITE.DIRECTORY_SEPARATOR.$params->get('efu_parent').DIRECTORY_SEPARATOR.$params->get('efu_folder');
		$path=JPATH_SITE.'/components/com_postcards/upload/'.$params['nowtime'];
		if(!is_dir($path))mkdir($path);
		if ($params['efu_user'] == true)
		{
			//get the user data
			$user =& JFactory::getUser();
			if ($user->guest == false)
			{
				$path.= DIRECTORY_SEPARATOR.$user->username;
			}
			else
			{
				//If it is a guest user, then store the file in the 'efu_folder'.
				//You can add something here, if you have a special folder that
				//you want to add the guest uploads to.
				//$path.= DIRECTORY_SEPARATOR.'your_guest_folder';
			}
		}
		
		//check to see if the upload process has started
		if (isset($_FILES[$params['efu_variable']]))
		{
			//now, we're going to check each of the uploaded files
			$total = intval($params['efu_multiple']);
			for ($i = 0; $i < $total; $i++)
			{
				//so, now, check for any other errors
				if ($_FILES[$params['efu_variable']]["error"][$i] > 0)
				{
					//error was found. Show the return code.
					$error_text = JText::_('MOD_EFU_RETURN_CODE').": ".$_FILES[$params['efu_variable']]["error"][$i]."<br />";
					$error_text.= $this->fileUploadErrorMessage($_FILES[$params['efu_variable']]["error"][$i]);
					
//					$result[$i]['type'] = 'error';
//					$result[$i]['text'] = $error_text;
				}
				else
				{
					//no errors found.
					//check to see if the file type is correct
					//but first, we have to store the file types in a variable. I was getting an issue with empty()
					if ($this->isValidFileType($params, $i))
					{
						//the file type is permitted
						//so, check for the right size
						if ($_FILES[$params['efu_variable']]["size"][$i] < $params['efu_maxsize'])
						{
							//file is an acceptable size
							//check to see if file already exists in the destination folder
							if (file_exists($path.DIRECTORY_SEPARATOR.$_FILES[$params['efu_variable']]["name"][$i]))
							{
								//file already exists
								//check whether the user wants to replace the file or not.
								if ($params['efu_replace'] == true && $_POST["answer"] == true)
								{
									//yep, the user wants to replace the file, so just delete the existing file
									JFile::delete($path.DIRECTORY_SEPARATOR.$_FILES[$params['efu_variable']]["name"][$i]);
									$this->storeUploadedFile($path, $params, $result, $i, true);
								}
								else
								{
//									$result[$i]['type'] = 'info';
//									$result[$i]['text'] = $_FILES[$params['efu_variable']]["name"][$i]." ".JText::_('MOD_EFU_ALREADY_EXISTS');
								}
							}
							else
							{
								$this->storeUploadedFile($path, $params, $result, $i);
							}
						}
						else
						{
							//file is too large
//							$result[$i]['type'] = 'warning';
//							$result[$i]['text'] = JText::_('MOD_EFU_TOO_LARGE_ERROR').sizeToText($params['efu_maxsize']).".";
						}
					}
					else
					{
						//the file type is not permitted
						$fakeMIME = $_FILES[$params['efu_variable']]["type"][$i];
						$trueMIME = $this->actualMIME($_FILES[$params['efu_variable']]["tmp_name"][$i]);
//						$result[$i]['type'] = 'error';
//						$result[$i]['text'] = JText::_('MOD_EFU_INVALID_ERROR')."<br />".JText::_('MOD_EFU_PHP_MIME_ERROR').($trueMIME!==false?"(".$trueMIME.")":"")."<br />".JText::_('MOD_EFU_BROWSER_MIME_ERROR').$fakeMIME;
					}
				}
			}
		}
		
//		return $result;
	}
	
	function isValidFileType(&$params, &$i)
	{
		$valid = false;
		
		$filetypes = $params['efu_filetypes'];
		$actualMIME = $this->actualMIME($_FILES[$params['efu_variable']]["tmp_name"][$i]);
		if ($filetypes == "*" || 
			(stripos(stripslashes($filetypes), $_FILES[$params['efu_variable']]["type"][$i]) !== false &&
			$actualMIME !== false &&
			stripos(stripslashes($filetypes), $actualMIME) !== false))
		{
			$valid = true;
		}
		
		return $valid;
	}
	
	function actualMIME($file)
	{
		if (!file_exists($file))
		{
			return false;
		}
		
		$mime = false;
		// try to use recommended functions
		if (defined('FILEINFO_MIME_TYPE') &&
			function_exists('finfo_open') && is_callable('finfo_open') && 
			function_exists('finfo_file') && is_callable('finfo_file') && 
			function_exists('finfo_close') && is_callable('finfo_close'))
		{
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mime = finfo_file($finfo, $file);
			if ($mime === '')
			{
				$mime = false;
			}
			finfo_close($finfo);
		}
		else if (strtoupper(substr(PHP_OS,0,3)) !== 'WIN')
		{
			$f = "'".$file."'";
			if (function_exists('escapeshellarg') && is_callable('escapeshellarg'))
			{
				//prefer to use escapeshellarg if it is available
				$f = escapeshellarg($file);
			}
			
			if (function_exists('exec') && is_callable('exec'))
			{
				//didn't like how 'system' flushes output to browser. replaced with 'exec'
				//note: You can change this to: shell_exec("file -b --mime-type $f"); if you get
				//      "regular file" as the mime type
				$mime = exec("file -bi $f");
				//this removes the charset value if it was returned with the mime type. mime is first.
				$mime = strtok($mime, '; ');
				$mime = trim($mime); //remove any remaining whitespace
			}
			else if (function_exists('shell_exec') && is_callable('shell_exec'))
			{
				//note: You can change this to: shell_exec("file -b --mime-type $f"); if you get
				//      "regular file" as the mime type
				$mime = shell_exec("file -bi $f");
				//this removes the charset value if it was returned with the mime type. mime is first.
				$mime = strtok($mime, '; ');
				$mime = trim($mime); //remove any remaining whitespace
			}
		}
		else if (function_exists('mime_content_type') && is_callable('mime_content_type'))
		{
			//test using mime_content_type last, since it sometimes detects the mime incorrectly
			$mime = mime_content_type($file);
		}
		
		return $mime;
	}
	
	function storeUploadedFile($filepath, &$params, &$result, &$i, $replaced = false)
	{
		$result_text = '';
		
		//move the file to the destination folder
		$success = JFile::upload($_FILES[$params['efu_variable']]["tmp_name"][$i], $filepath.DIRECTORY_SEPARATOR.$_FILES[$params['efu_variable']]["name"][$i]);
		
		if ($replaced)
		{
			$result_text.= JText::_('MOD_EFU_REPLACEMENT_APPROVED')." ";
		}
		
		if ($success)
		{
			//Upload was successful.
			$result_text.= JText::_('MOD_EFU_UPLOAD_SUCCESSFUL')."<br />";
			$result_text.= JText::_('MOD_EFU_NAME').": ".$_FILES[$params['efu_variable']]["name"][$i]."<br />";
			$result_text.= JText::_('MOD_EFU_TYPE').": ".$_FILES[$params['efu_variable']]["type"][$i]."<br />";
			$result_text.= JText::_('MOD_EFU_SIZE').": ".$this->sizeToText($_FILES[$params['efu_variable']]["size"][$i])."<br />";
			//$result_text.= "Temp file: ".$_FILES[$params['efu_variable']]["tmp_name"][$i]."<br />";
			//$result_text.= "Stored in: ".$filepath;
			
			$result[$i]['type'] = 'success';
			$result[$i]['text'] = $result_text;
		}
		else
		{
			$result_text.= JText::_('MOD_EFU_UPLOAD_UNSUCCESSFUL');
			
			$result[$i]['type'] = 'warning';
			$result[$i]['text'] = $result_text;
		}
	}
	
	function fileUploadErrorMessage($error_code)
	{
		switch ($error_code)
		{
			case UPLOAD_ERR_INI_SIZE:
				$message = JText::_('MOD_EFU_INI_SIZE_ERROR'); 
				break;
			case UPLOAD_ERR_FORM_SIZE: 
				$message = JText::_('MOD_EFU_FORM_SIZE_ERROR'); 
				break;
			case UPLOAD_ERR_PARTIAL: 
				$message = JText::_('MOD_EFU_PARTIAL_ERROR'); 
				break;
			case UPLOAD_ERR_NO_FILE: 
				$message = JText::_('MOD_EFU_NO_FILE_ERROR'); 
				break;
			case UPLOAD_ERR_NO_TMP_DIR: 
				$message = JText::_('MOD_EFU_NO_TMP_DIR_ERROR'); 
				break;
			case UPLOAD_ERR_CANT_WRITE: 
				$message = JText::_('MOD_EFU_CANT_WRITE_ERROR'); 
				break;
			case UPLOAD_ERR_EXTENSION: 
				$message = JText::_('MOD_EFU_EXTENSION_ERROR'); 
				break;
			default: 
				$message = JText::_('MOD_EFU_UNKNOWN_ERROR');
				break;
		}
		return $message;
	}
	
	function sizeToText($size)
	{
		$text = "";
		$kb = 1024;
		$mb = $kb * $kb;
		$gb = $mb * $kb;
		
		if ($size >= $gb)
		{
			$size = round($size / $gb, 2);
			$text = $size."GB";
		}
		elseif ($size >= $mb)
		{
			$size = round($size / $mb, 2);
			$text = $size."MB";
		}
		elseif ($size >= $kb)
		{
			$size = round($size / $kb, 2);
			$text = $size."KB";
		}
		else
		{
			$text = $size.JText::_('MOD_EFU_BYTES');
		}
		return $text;
	}

	function createGif($path){
					//產生GIF檔
//			header ('Content-type:image/gif');
			include(JPATH_SITE.'/components/com_postcards/assets/GIFEncoder.class.php');

			$text = "";
			// Open the first source image and add the text.
			$files = explode("\n", trim(`find -L $path`)); // -L follows symlinks
			foreach($files as $s=>$file){
				if($s>0){
					$image = imagecreatefromjpeg($file);
					$text_color = imagecolorallocate($image, 200, 200, 200);
					imagestring($image, 0, 0, 0,  $text, $text_color);

					// Generate GIF from the $image
					// We want to put the binary GIF data into an array to be used later,
					//  so we use the output buffer.
					ob_start();
					imagegif($image);
					$frames[]=ob_get_contents();
					$framed[]=300; // Delay in the animation.
					ob_end_clean();
				}
			}
			// Generate the animated gif and output to screen.
			$gif = new GIFEncoder($frames,$framed,0,2,0,0,0,'bin');
			//echo $gif->GetAnimation();
			$fp = fopen($path.'/animegif.gif', 'w');
			fwrite($fp, $gif->GetAnimation());
			fclose($fp);
			//GIF end

	}
}