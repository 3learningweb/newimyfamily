<?php
/**
 * @version     1.0.0
 * @package     com_cinema
 * @copyright   Efatek Inc. Copyright (C) 2012. All rights reserved.
 * @license     http://www.efatek.com
 * @author      Efatek <sam@efatek.com> - http://www.efatek.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Items list controller class.
 */
class PostcardsControllerImage extends PostcardsController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function getModel($name = 'image', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}
	

}