<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		postcard
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$model=&$this->getModel('jewelry');
//$params='{"efu_parent":"images","efu_folder":"","efu_maxsize":"1000000","efu_custom":"1","efu_label":"\u8acb\u9078\u64c7\u6a94\u6848:","efu_button":"\u4e0a\u50b3\u6a94\u6848","efu_question":"Replace existing files with uploaded files?","efu_yes":"Yes","efu_no":"No","efu_filetypes":"image\/gif;image\/jpeg;image\/pjpeg;image\/png","efu_replace":"0","efu_variable":"fileToUpload","efu_multiple":"5","moduleclass_sfx":"","layout":"_:default","efu_user":"0","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}';
$params=array("efu_parent"=>"images","efu_folder"=>"","efu_maxsize"=>"1000000","efu_custom"=>"1","efu_label"=>"\u8acb\u9078\u64c7\u6a94\u6848=>","efu_button"=>"\u4e0a\u50b3\u6a94\u6848","efu_question"=>"Replace existing files with uploaded files?","efu_yes"=>"Yes","efu_no"=>"No","efu_filetypes"=>"image\/gif;image\/jpeg;image\/pjpeg;image\/png","efu_replace"=>"0","efu_variable"=>"fileToUpload","efu_multiple"=>"5","moduleclass_sfx"=>"","layout"=>"_=>default","efu_user"=>"0","module_tag"=>"div","bootstrap_size"=>"0","header_tag"=>"h3","header_class"=>"","style"=>"0");
//$result=$model->getFileToUpload($params);

?>
<div class="com_postcards">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<div class="<?php echo $moduleclass_sfx;?>">
		<!-- Display the Results of the file upload if uploading was attempted -->
		<?php //if (isset($_FILES[$params->get('efu_variable')])) : ?>
			<?php for ($j = 0; $j < count($result); $j++) : ?>
				<div class="efum-alert efum-<?php echo $result[$j]['type'];?>">
				<span class="close-btn" onclick="this.parentNode.style.display = 'none';">&times;</span>
				<?php echo $result[$j]['text']; ?>
				</div>
			<?php endfor; ?>
		<?php //endif; ?>
		<!-- Input form for the File Upload -->
		<form enctype="multipart/form-data" action="index.php?option=com_postcards&view=jewelry&layout=combind&Itemid=<?php echo $itemid;?>" method="post">
			<?php if ($params['efu_multiple'] == "1"): ?>
			<label for=<?php echo '"'.$params['efu_variable'].'[]"'; ?>><?php echo $labelText; ?></label>
			<?php else: ?>
			上傳檔案<br />
			<?php endif; ?>
			<?php 
			$max = intval($params['efu_multiple']);
			for ($i = 0; $i < $max; $i++): ?>
			<input type="file" name=<?php echo '"'.$params['efu_variable'].'[]"'; ?> id=<?php echo '"'.$params['efu_variable'].'[]"'; ?> style="margin-top:1px; margin-bottom:1px;" /> 
			<br />
			<?php endfor; ?>
			<?php if ($params['efu_replace'] == true): /* 1 means 'Yes' or true. 0 means 'No' or false. */ ?>
			<div>上傳檔案</div>
			<input type="radio" name="answer" value="1" /><?php echo $yesText; ?><br />
			<input type="radio" name="answer" value="0" checked /><?php echo $noText; ?><br />
			<br />
			<?php endif; ?>
			<input class="btn" type="submit" name="submit" value="上傳檔案" />
		</form>
	</div>


</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		
		

	});
</script>
