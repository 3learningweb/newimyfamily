<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$component = JComponentHelper::getComponent('com_postcards');

$params = $app->getParams();
$catid = $params->get('catid');

$categories = JCategories::getInstance('Postcards');
$model=&$this->getModel('jewelry');

// get subcategories 1st level
$cat_lv1 = $categories->get($catid);
$children_lv1 = $cat_lv1->getChildren();
?>
<div class="com_postcards postcard_greeting">

	<?php
	if ($children_lv1) {
		?>
		<form name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=jewelry&layout=gform&Itemid=' . (int) $itemid); ?>">
			<div class="game_page-header">
				<div class="title">回憶珠寶盒
				</div>
			</div>
				<div class="cat_title">
					生活中，無論刻意或不經意拍下的家庭照片，都讓當下的時光，被永恆珍藏。<br>請上傳家中的照片，透過動態播放，串起生活中的重要時刻。<br><br>
				</div>
					<?php echo JText::_('COM_POSTCARDS_GREETING_THEME'); ?>

			<?php
			$check = $app->getUserState('form.postcards.cat_id', '');

			// get subcategories 1st level
			foreach ($children_lv1 as $key_lv1 => $child_lv1) {
				?>
				<div class="cat_title">
					<?php echo $child_lv1->title; ?>
				</div>
				<div class="gimg_blocks">
					<?php 
					$children_lv2=$model->getImages($child_lv1->id);
					foreach ($children_lv2 as $key_lv2 => $child_lv2) {
						//if ($key_lv1 == 0 && $key_lv2 == 0 && $check == 0) {
						//	$check = $child_lv2->id;
						//}

						// get category params image
						//$cat_params = json_decode($child_lv1->params);
						//$cat_image = $cat_params->image;
						?>
						<div class="gimg_block">
							<div class="image">
								<img alt="<?php echo $child_lv2->title; ?>" src="<?php echo JURI::root() . $child_lv2->image; ?>" />
							</div>
							<div class="img_title">
								<input type="radio" id="cat_id_<?php echo $child_lv2->id; ?>" name="cat_id" value="<?php echo $child_lv2->id; ?>" <?php echo ($check == $child_lv2->id) ? "checked" : ""; ?>>
								<label for="cat_id_<?php echo $child_lv2->id; ?>"><?php echo $child_lv2->title; ?></label>


							</div>

						</div>
					<?php } ?>
				</div>
			<?php } ?>
	</div>
	<div class="submit">
		<input type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>">
	</div>
	</form>
	<?php
} else {
	?>
	<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

	<?php
}
?>


