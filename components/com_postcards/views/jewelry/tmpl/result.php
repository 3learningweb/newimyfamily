<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		postcard
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
header ('Content-type:image/gif');
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$model=&$this->getModel('jewelry');
//$nowtime=time();
$nowtime=$app->getUserState('form.postcards.nowtime');
$bg_img=$app->getUserState('form.postcards.bg_img');


$file=JPATH_SITE.'/components/com_postcards/upload/'.$nowtime.'/animegif.gif';
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename($file));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($file));
ob_clean();
flush();
readfile($file);
exit;


?>
<div class="com_postcards postcard_greeting view_card">
<?php
//	if ($this->bg_img) {
?>
	<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=gviewcard&layout=result&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >

		<div class="preview">
			<table class="form_table" width="100%">

				<tr>
					<td colspan="2">
						<img width="100%" src="<?php echo JURI::root() . $bg_img; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_IMAGE'); ?>">
					</td>
				</tr>
			</table>
		</div>
		<br />
	</form>
	

</div>
