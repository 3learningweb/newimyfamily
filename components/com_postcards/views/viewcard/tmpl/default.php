<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid	= $app->input->getInt('Itemid');

?>
<div class="com_postcards">
<?php
	if ($this->bg_img) {
?>
	<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=email&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >

		<div class="preview">
			<table class="form_table" width="100%">

				<tr>
					<td colspan="2">
						<img src="<?php echo JURI::root() . $this->bg_img; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_IMAGE'); ?>">
					</td>
				</tr>
				<?php if ($this->card_type == "postcard") { ?>
				<tr>
					<td colspan="2">
						<?php echo $app->getUserState('form.postcards.content', ''); ?>
					</td>
				</tr>
				<?php } ?>

			
				<tr>
					<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_RECEIVER_EMAIL'); ?></th>
					<td class="form_text"><input type="text" id="friend_email" name="friend_email" class="required" value="<?php echo $app->getUserState('form.postcards.friend_email', ''); ?>"></td>
				</tr>
				<tr>
					<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_SENDER_NAME'); ?></th>
					<td class="form_text"><input type="text" name="name" class="required" value="<?php echo $app->getUserState('form.postcards.name', ''); ?>"></td>
				</tr>
				<tr>
					<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_SENDER_EMAIL'); ?></th>
					<td class="form_text"><input type="text" id="email" name="email" class="required" value="<?php echo $app->getUserState('form.postcards.email', ''); ?>"></td>
				</tr>
				<tr>
					<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_RECEIVER_SUBJECT'); ?></th>
					<td class="form_text"><input type="text" name="subject" class="required" value="<?php echo $app->getUserState('form.postcards.subject', ''); ?>"></td>
				</tr>

			</table>

			
		</div>
		<div class="submit">
			<input id="submit_btn" type="button" value="<?php echo JText::_('COM_POSTCARDS_SEND'); ?>" onClick="sendForm()" />
			<input type="hidden" name="task" value="viewcard.send" />
		</div>
	</form>
	
	<?php
		} else {
	?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

	<?php
		}
	?>


</div>
<script>


	function sendForm() {

		if (jQuery(".required").val() == "") {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_REUIRED'); ?>");

			return false;
		}

		reEcourse=/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
		var friend_email = jQuery("#friend_email").val();
		if (!reEcourse.test(jQuery.trim(friend_email))) {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_EMAIL'); ?>");
			return false;
		}

		var email = jQuery("#email").val();
		if (!reEcourse.test(jQuery.trim(email))) {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_EMAIL'); ?>");
			return false;
		}


		if(confirm("<?php echo JText::_('COM_POSTCARDS_SEND_CHECK'); ?>")) {
			document.getElementById("submitForm").submit();
		}
	}
</script>