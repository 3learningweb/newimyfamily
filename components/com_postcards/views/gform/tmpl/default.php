<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

//自填文字標題，後台參數
$params = $app->getParams();
$customize_name = $params->get('customize_name');
$model=&$this->getModel('gform');
$cat_id = JRequest::getInt('cat_id');
$image = $model->getImage($cat_id);
?>
<div class="com_postcards postcard_greeting">
	<?php
	if ($this->items) {
		?>
		<div class="message_form">
(建議上傳最佳尺寸為<?php echo $image->pic_width; ?>x<?php echo $image->pic_height; ?> ，<br>避免上傳圖片被裁切，影響圖片最佳顯示尺寸。)
			<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=gviewcard&Itemid=' . (int) $itemid); ?>"  enctype="multipart/form-data" >

				<?php //上傳相片 ?>
				<div class="user_img">
					<div class="game_page-header">
						<div class="title">
							<?php echo JText::_('COM_POSTCARDS_GREETING_IMAGE'); ?>
							<span class="note">(<?php echo JText::_('COM_POSTCARDS_GREETING_IMAGE_NOTE'); ?>)</span>
						</div>
					</div>
					<input type="file" id="user_image" name="user_image" accept="image/jpeg" />
				</div>

				<?php //寫下祝福 ?>
				<div class="message">
					<div class="game_page-header">
						<div class="title">
							<?php echo JText::_('COM_POSTCARDS_GREETING_MESSAGE'); ?>
						</div>
					</div>
					<table class="form_table" width="100%">
						<tr>
							<th class="control"><input type="radio" name="message_type" value="customize" checked /></th>
							<td colspan="2">
								<?php echo JText::_('COM_POSTCARDS_GREETING_MESSAGE_CONTENT'); ?>
							</td>
						</tr>
						<tr>
							<th class="control"></th>
							<td colspan="2">
								<input type="text" size="30" id="custom_text" name="custom_text" placeholder="<?php echo JText::_('COM_POSTCARDS_GREETING_MESSAGE_CONTENT_NOTE'); ?>" value="<?php echo $app->getUserState('form.postcards.content', ''); ?>" />
							</td>
						</tr>
						<tr>
							<th class="control"><input type="radio" name="message_type" value="reference" /></th>
							<td colspan="2">
								<?php echo JText::_('COM_POSTCARDS_GREETING_MESSAGE_REFER'); ?>
							</td>
						</tr>
						<?php
						$check = 0; //預設選取項目
						$customize_id = "";
						foreach ($this->items as $item) {
							//跳過自填項目，後台需有參數可過濾文字
							if ($item->title == $customize_name) {
								$customize_id = $item->id;
								continue;
							}
							?>
							<tr>
								<th>

								</th>
								<td class="control">
									<input type="radio" name="message_refer" value="<?php echo $item->id ?>"  <?php echo ($check == 0) ? "checked" : "" ?> />
								</td>
								<td>
									<?php echo $item->title ?>
								</td>
							</tr>
							<?php $check++;
						} ?>
					</table>
				</div>

				<div class="submit">
					<input id="submit_btn" type="button" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>" onClick="sendForm()" />
					<input type="reset" value="<?php echo JText::_('COM_POSTCARDS_RESET'); ?>">
					<input type="hidden" name="task" value="gviewcard.check" />
					<input type="hidden" name="customize_id" value="<?php echo $customize_id; ?>" />
				</div>
			</form>
		</div>
		<?php
	} else {
		?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NOIMAGE'); ?></div>

		<?php
	}
	?>


</div>
<script>
	jQuery(document).ready(function() {
		
	});

	function sendForm() {

		//檢查相片是否上傳
		if (jQuery("#user_image").val() == "") {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_PIC'); ?>");
			return false;
		}

		//檢查當使用者選擇「自填文字」時，有無確實填寫文字
		if (jQuery("input[name='message_type']:checked").val() == "customize" && jQuery("#custom_text").val() == "") {
			alert("<?php echo JText::_('COM_POSTCARDS_GREETING_MESSAGE_CONTENT_ALERT'); ?>");
			return false;
		}


		//
		if(confirm("<?php echo JText::_('COM_POSTCARDS_FORM_CHECK'); ?>")) {
			document.getElementById("submitForm").submit();
		}
	}
</script>