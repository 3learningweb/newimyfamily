<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid	= $app->input->getInt('Itemid');
$img_link = JURI::root() . $app->getUserState('form.postcards.bg_img', '');

$app->setUserState('form.postcards', null);

?>
<div class="com_postcards postcard_greeting result">
	<div class="result_title">
		<?php echo JText::_('COM_POSTCARDS_GREETING_EMAIL_SUCCESS'); ?>
	</div>
	<br />
<!--	<div align="center">
	<div class="download_card submit_block">
		<a href="<?php echo $img_link; ?>"><?php echo JText::_('COM_POSTCARDS_GREETING_CARD_DOWNLOAD'); ?></a>
	</div>
	</div>-->
</div>