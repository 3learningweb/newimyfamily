<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		postcard
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
header ('Content-type:image/gif');
defined('_JEXEC') or die('Restricted access');
define('JPATH_BASE', dirname(__FILE__) );

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$cat_id = $app->input->getInt('cat_id');
$nowtime = $app->input->getInt('nowtime');
$model=&$this->getModel('familytree');
//$nowtime=time();

//$cat_id=$app->getUserState('form.postcards.cat_id');
//$nowtime=$app->getUserState('form.postcards.nowtime');
$bg_img=$app->getUserState('form.postcards.bg_img');
$filearr=$app->getUserState('form.postcards.filearr');
//print_r($filearr);echo '$time='.$time;
//$params='{"efu_parent":"images","efu_folder":"","efu_maxsize":"1000000","efu_custom":"1","efu_label":"\u8acb\u9078\u64c7\u6a94\u6848:","efu_button":"\u4e0a\u50b3\u6a94\u6848","efu_question":"Replace existing files with uploaded files?","efu_yes":"Yes","efu_no":"No","efu_filetypes":"image\/gif;image\/jpeg;image\/pjpeg;image\/png","efu_replace":"0","efu_variable":"fileToUpload","efu_multiple":"5","moduleclass_sfx":"","layout":"_:default","efu_user":"0","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}';
//$params=array("efu_parent"=>"images","efu_folder"=>"","efu_maxsize"=>"1000000","efu_custom"=>"1","efu_label"=>"\u8acb\u9078\u64c7\u6a94\u6848=>","efu_button"=>"\u4e0a\u50b3\u6a94\u6848","efu_question"=>"Replace existing files with uploaded files?","efu_yes"=>"Yes","efu_no"=>"No","efu_filetypes"=>"image\/gif;image\/jpeg;image\/pjpeg;image\/png","efu_replace"=>"0","efu_variable"=>"fileToUpload","efu_multiple"=>"5","moduleclass_sfx"=>"","layout"=>"_=>default","efu_user"=>"0","module_tag"=>"div","bootstrap_size"=>"0","header_tag"=>"h3","header_class"=>"","style"=>"0","nowtime"=>$nowtime);
//$result=$model->getFileToUpload($params);
$image = $model->getImage($cat_id);




?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
<script src="<?php echo JURI::root();?>components/com_postcards/assets/js/html2canvas.js"></script>
  <style>
  #resizable { width: 150px; height: 150px; }
  </style>
  <script>
	$(function() {
		$( "#resizable" ).draggable();
	});
	$(function() {
		$( "#resizable" ).resizable();
	});
 function takeScreenShot(){
 html2canvas(window.parent.document.body, {
 onrendered: function(canvas) {
 var cand = document.getElementsByTagName('canvas');
// var cand = document.getElementById('testCanvas');
 if(cand[0] === undefined || cand[0] === null){
 
 }else{
 //cand[0].remove();
 document.body.removeChild(cand[0]);
 }
 document.body.appendChild(canvas);
 }
 });
 }
 
 function postImage(){
 var cand = document.getElementsByTagName('canvas');
// var cand = document.getElementsById('testCanvas');
 var canvasData = cand[0].toDataURL("image/png");
 var ajax = new XMLHttpRequest();
 ajax.open("POST",'<?php echo JURI::root();?>components/com_postcards/assets/testSave.php',false);
 ajax.setRequestHeader('Content-Type', 'application/upload');
 ajax.send(canvasData );
 alert('done');
 }
 
 takeScreenShot();
</script>
<div class="com_postcards postcard_greeting view_card" style="position: relative">
<?php
//	if ($this->bg_img) {
?>
	<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=familytree&layout=result&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >

		<div class="preview">
			<table class="form_table" width="100%">

				<tr>
					<td colspan="2">
						<img width="100%" src="<?php echo JURI::root() . $image->image; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_IMAGE'); ?>">
					</td>
				</tr>
			</table>
		</div>
		<br />
		<div style="position:absolute;top:50px;left:50px;border: 1px solid;overflow: hidden;width:150px;height:150px;">
			<div id="resizable" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[0]; ?>" width="100%" height="100%">
			</div>
		</div>
<input type="button" value="Capture Screenshot" onclick="takeScreenShot();">
<input type="button" value="Post Screenshot" onclick="postImage();">
	</form>
	

</div>
