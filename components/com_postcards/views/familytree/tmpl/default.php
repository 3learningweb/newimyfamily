<?php
/**
 * @version		: default.php 2015-12-15 21:06:39$
 * @author		EFATEK 
 * @package		postcards
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$component = JComponentHelper::getComponent('com_postcards');

$params = $app->getParams();
$catid = $params->get('catid');

$categories = JCategories::getInstance('Postcards');
$model=&$this->getModel('familytree');

// get subcategories 1st level
$children_lv1 = $model->getImages($catid);
?>
<div class="com_postcards postcard_greeting">

	<?php
	if ($children_lv1) {
		?>
		<form name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=familytree&layout=gform&Itemid=' . (int) $itemid); ?>">
			<div class="game_page-header">
				<div class="title">
					家庭樹
				</div>
			</div>
				<div class="cat_title">
					家庭猶如大樹，每個成員都代表一個個豐盛的果實。<br>家人之間的關心與互動，是促進大樹成長茁壯的最佳養分。<br>請找出家人的照片，製作出屬於自家的家庭樹。<br>
					<br>
				</div>
					<?php echo JText::_('COM_POSTCARDS_GREETING_THEME'); ?>

			<?php
			$check = $app->getUserState('form.postcards.cat_id', '');

			// get subcategories 1st level
			foreach ($children_lv1 as $key_lv1 => $child_lv1) {
				?>
				<div class="gimg_blocks">
						<div class="gimg_block">
							<div class="image">
								<img alt="<?php echo $child_lv1->title; ?>" src="<?php echo JURI::root() . $child_lv1->image; ?>" />
							</div>
							<div class="img_title">
								<input type="radio" id="cat_id_<?php echo $child_lv1->id; ?>" name="cat_id" value="<?php echo $child_lv1->id; ?>" <?php echo ($check == $child_lv1->id) ? "checked" : ""; ?>>
								<label for="cat_id_<?php echo $child_lv1->id; ?>"><?php echo $child_lv1->title; ?></label>


							</div>

						</div>
				</div>
			<?php } ?>
	</div>
	<div class="submit">
		<input type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>">
	</div>
	</form>
	<?php
} else {
	?>
	<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

	<?php
}
?>


