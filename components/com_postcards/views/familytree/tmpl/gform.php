<?php
/**
 * @version		: default.php 2015-12-15 21:06:39$
 * @author		EFATEK 
 * @package		postcard
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$model=&$this->getModel('familytree');
//$params='{"efu_parent":"images","efu_folder":"","efu_maxsize":"1000000","efu_custom":"1","efu_label":"\u8acb\u9078\u64c7\u6a94\u6848:","efu_button":"\u4e0a\u50b3\u6a94\u6848","efu_question":"Replace existing files with uploaded files?","efu_yes":"Yes","efu_no":"No","efu_filetypes":"image\/gif;image\/jpeg;image\/pjpeg;image\/png","efu_replace":"0","efu_variable":"fileToUpload","efu_multiple":"5","moduleclass_sfx":"","layout":"_:default","efu_user":"0","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}';
$nowtime=time();
$params=array("efu_parent"=>"images","efu_folder"=>"","efu_maxsize"=>"1000000","efu_custom"=>"1","efu_label"=>"\u8acb\u9078\u64c7\u6a94\u6848=>","efu_button"=>"\u4e0a\u50b3\u6a94\u6848","efu_question"=>"Replace existing files with uploaded files?","efu_yes"=>"Yes","efu_no"=>"No","efu_filetypes"=>"image\/gif;image\/jpeg;image\/pjpeg;image\/png","efu_replace"=>"0","efu_variable"=>"fileToUpload","efu_multiple"=>"8","moduleclass_sfx"=>"","layout"=>"_=>default","efu_user"=>"0","module_tag"=>"div","bootstrap_size"=>"0","header_tag"=>"h3","header_class"=>"","style"=>"0","nowtime"=>$nowtime);
//$result=$model->getFileToUpload($params);
$cat_id = JRequest::getInt('cat_id');
$image = $model->getImage($cat_id);
?>
<div class="com_postcards">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>

	<div class="<?php echo $moduleclass_sfx;?>">
		<!-- Display the Results of the file upload if uploading was attempted -->
		<?php //if (isset($_FILES[$params->get('efu_variable')])) : ?>
			<?php for ($j = 0; $j < count($result); $j++) : ?>
				<div class="efum-alert efum-<?php echo $result[$j]['type'];?>">
				<span class="close-btn" onclick="this.parentNode.style.display = 'none';">&times;</span>
				<?php echo $result[$j]['text']; ?>
				</div>
			<?php endfor; ?>
		<?php //endif; ?>
		<!-- Input form for the File Upload -->
		<form enctype="multipart/form-data" action="index.php?option=com_postcards&view=familytree&layout=combind&Itemid=<?php echo $itemid;?>" method="post">
			<?php if ($params['efu_multiple'] == "1"): ?>
			<label for=<?php echo '"'.$params['efu_variable'].'[]"'; ?>><?php echo $labelText; ?></label>
			<?php else: ?>
			上傳檔案(上傳圖片最佳尺寸為80x68像素，單張最大請勿超過1 MB。)<br />
			<?php endif; ?>
			<?php 
			$max = intval($params['efu_multiple']);
			for ($i = 0; $i < $max; $i++): echo $i+1;?>
			<input type="file" name=<?php echo '"'.$params['efu_variable'].'[]"'; ?> id=<?php echo '"'.$params['efu_variable'].'[]"'; ?> style="margin-top:1px; margin-bottom:1px;" enctype="multipart/form-data"/> 
			<br />
			稱謂:<input type="text" name="textarr[]" id="textarr[]">
			<br/>
			<?php endfor;
			if ($params['efu_replace'] == true): /* 1 means 'Yes' or true. 0 means 'No' or false. */ ?>
			<div>上傳檔案</div>
			<input type="radio" name="answer" value="1" /><?php echo $yesText; ?><br />
			<input type="radio" name="answer" value="0" checked /><?php echo $noText; ?><br />
			<br />
			<?php endif; ?>
			<input type="hidden" name="task" value="familytree.check">
			<input type="hidden" name="cat_id" value="<?php echo $cat_id;?>">
			<input class="btn" type="submit" name="submit" value="上傳檔案" />
		</form>
	</div>


</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		
		

	});
</script>
