<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		postcard
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
header ('Content-type:image/gif');
defined('_JEXEC') or die('Restricted access');
define('JPATH_BASE', dirname(__FILE__) );

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$cat_id = $app->input->getInt('cat_id');
$nowtime = $app->input->getInt('nowtime');
$model=&$this->getModel('familytree');

$bg_img=$app->getUserState('form.postcards.bg_img');
$filearr=$app->getUserState('form.postcards.filearr');
$textarr=$app->getUserState('form.postcards.textarr');
$image = $model->getImage($cat_id);




?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="components/com_postcards/assets/js/html2canvas.js"></script>
<script type="text/javascript" src="components/com_postcards/assets/js/jquery.plugin.html2canvas.js"></script>
<style>
	#resizable { width: 150px; height: 150px; }
</style>
<script>
	$(function() {
		$( "#resizable0" ).draggable();
		$( "#resizable1" ).draggable();
		$( "#resizable2" ).draggable();
		$( "#resizable3" ).draggable();
		$( "#resizable4" ).draggable();
		$( "#resizable5" ).draggable();
		$( "#resizable6" ).draggable();
		$( "#resizable7" ).draggable();
	});
	$(function() {
		$( "#resizable0" ).resizable();
		$( "#resizable1" ).resizable();
		$( "#resizable2" ).resizable();
		$( "#resizable3" ).resizable();
		$( "#resizable4" ).resizable();
		$( "#resizable5" ).resizable();
		$( "#resizable6" ).resizable();
		$( "#resizable7" ).resizable();
	});

function capture() {
	$('#target').html2canvas({
		onrendered: function (canvas) {
			//Set hidden field's value to image data (base-64 string)
			$('#img_val').val(canvas.toDataURL("image/png"));
			//Submit the form manually
			document.getElementById("myForm").submit();
		}
	});
}
</script>
<div style="width:80%;color: red;">圖片可拖拉縮放, 若操作失敗, 請重新整理頁面</div>
<div class="com_postcards familytree view_card" style="position: relative" id="target">
<?php
//	if ($this->bg_img) {
?>
	<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=familytree&layout=result&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >

		<div class="preview">
			<table class="form_table" width="100%">

				<tr>
					<td colspan="2">
						<img width="100%" src="<?php echo JURI::root() . $image->image; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_IMAGE'); ?>" style="max-width: none !important;width: initial;">
					</td>
				</tr>
			</table>
		</div>
		<br />
		<div style="position:absolute;left:345px;top:125px;overflow: hidden;width:85px;height:68px;">
			<?php if($filearr[0]){?>
			<div id="resizable0" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[0]; ?>" width="100%" height="100%">
			</div>
			<?php }?>
		</div>
		<?php if($textarr[0]){?>
		<div style="position:absolute;left:345px;top:197px;overflow: hidden;background-color: white;">
			<?php echo $textarr[0];?>
		</div>
		<?php }?>
		<div style="position:absolute;left:215px;top:200px;overflow: hidden;width:85px;height:68px;">
			<?php if($filearr[1]){?>
			<div id="resizable1" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[1]; ?>" width="100%" height="100%">
			</div>
			<?php }?>
		</div>
		<?php if($textarr[1]){?>
		<div style="position:absolute;left:215px;top:273px;overflow: hidden;background-color: white">
			<?php echo $textarr[1];?>
		</div>
		<?php }?>
		<div style="position:absolute;left:450px;top:185px;overflow: hidden;width:85px;height:68px;">
			<?php if($filearr[2]){?>
			<div id="resizable2" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[2]; ?>" width="100%" height="100%">
			</div>
			<?php }?>
		</div>
		<?php if($textarr[2]){?>
		<div style="position:absolute;left:450px;top:255px;overflow: hidden;background-color: white">
			<?php echo $textarr[2];?>
		</div>
		<?php }?>
		<div style="position:absolute;left:125px;top:300px;overflow: hidden;width:85px;height:68px;">
			<?php if($filearr[3]){?>
			<div id="resizable3" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[3]; ?>" width="100%" height="100%">
			</div>
			<?php }?>
		</div>
		<?php if($textarr[3]){?>
		<div style="position:absolute;left:125px;top:375px;overflow: hidden;background-color: white">
			<?php echo $textarr[3];?>
		</div>
		<?php }?>
		<div style="position:absolute;left:375px;top:310px;overflow: hidden;width:85px;height:68px;">
			<?php if($filearr[4]){?>
			<div id="resizable4" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[4]; ?>" width="100%" height="100%">
			</div>
			<?php }?>
		</div>
		<?php if($textarr[4]){?>
		<div style="position:absolute;left:375px;top:385px;overflow: hidden;background-color: white">
			<?php echo $textarr[4];?>
		</div>
		<?php }?>
		<div style="position:absolute;left:535px;top:315px;overflow: hidden;width:85px;height:68px;">
			<?php if($filearr[5]){?>
			<div id="resizable5" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[5]; ?>" width="100%" height="100%">
			</div>
			<?php }?>
		</div>
		<?php if($textarr[5]){?>
		<div style="position:absolute;left:535px;top:390px;overflow: hidden;background-color: white">
			<?php echo $textarr[5];?>
		</div>
		<?php }?>
		<div style="position:absolute;left:135px;top:450px;overflow: hidden;width:85px;height:68px;">
			<?php if($filearr[6]){?>
			<div id="resizable6" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[6]; ?>" width="100%" height="100%">
			</div>
			<?php }?>
		</div>
		<?php if($textarr[6]){?>
		<div style="position:absolute;left:135px;top:520px;overflow: hidden;background-color: white">
			<?php echo $textarr[6];?>
		</div>
		<?php }?>
		<div style="position:absolute;left:470px;top:450px;overflow: hidden;width:85px;height:68px;">
			<?php if($filearr[7]){?>
			<div id="resizable7" class="ui-widget-content">
				<img src="<?php echo JURI::root() .'components/com_postcards/upload/'.$nowtime.'/' . $filearr[7]; ?>" width="100%" height="100%">
			</div>
			<?php }?>
		</div>
		<?php if($textarr[7]){?>
		<div style="position:absolute;left:470px;top:525px;overflow: hidden;background-color: white">
			<?php echo $textarr[7];?>
		</div>
		<?php }?>
<?php /*<input type="button" value="Capture Screenshot" onclick="takeScreenShot();">
<input type="button" value="Post Screenshot" onclick="postImage();">*/?>
	</form>
	

</div>
<input type="submit" value="下載圖片" onclick="capture();" />
<form method="POST" enctype="multipart/form-data" id="myForm">
    <input type="hidden" name="img_val" id="img_val" value="" />
	<input type="hidden" name="task" value="familytree.saveCanvas">
	<input type="hidden" name="nowtime" value="<?php echo $nowtime;?>">

</form>