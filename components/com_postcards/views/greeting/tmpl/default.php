<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$component = JComponentHelper::getComponent('com_postcards');

$params = $app->getParams();
$catid = $params->get('catid');

$categories = JCategories::getInstance('Postcards');

// get subcategories 1st level
$cat_lv1 = $categories->get($catid);
$children_lv1 = $cat_lv1->getChildren();
?>
<div class="com_postcards postcard_greeting">

	<?php
	if ($children_lv1) {
		?>
		<form name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=gform&Itemid=' . (int) $itemid); ?>">
			<div class="game_page-header">
				<div class="title">
					祝福賀卡
				</div>
			</div>
				<div class="cat_title">
					每個節日慶典，透過簡短文字，對親朋好友表達情意，讓關係永連結。<br>選張卡片、寫下祝福，歡慶生活中的美好時刻。<br>
					<br>
				</div>
					<?php echo JText::_('COM_POSTCARDS_GREETING_THEME'); ?>

			<?php
			$check = $app->getUserState('form.postcards.cat_id', '');

			// get subcategories 1st level
			foreach ($children_lv1 as $key_lv1 => $child_lv1) {
				?>
				<div class="cat_title">
					<?php echo $child_lv1->title; ?>
				</div>
				<div class="gimg_blocks">
					<?php
					// get subcategories 2nd level
					$cat_lv2 = $categories->get($child_lv1->id);
					$children_lv2 = $cat_lv2->getChildren();

					foreach ($children_lv2 as $key_lv2 => $child_lv2) {
						if ($key_lv1 == 0 && $key_lv2 == 0 && $check == 0) {
							$check = $child_lv2->id;
						}

						// get category params image
						$cat_params = json_decode($child_lv2->params);
						$cat_image = $cat_params->image;
						?>
						<div class="gimg_block">
							<div class="image">
								<img alt="<?php echo $child_lv2->title; ?>" src="<?php echo JURI::root() . $cat_image; ?>" />
							</div>
							<div class="img_title">
								<input type="radio" id="cat_id_<?php echo $child_lv2->id; ?>" name="cat_id" value="<?php echo $child_lv2->id; ?>" <?php echo ($check == $child_lv2->id) ? "checked" : ""; ?>>
								<label for="cat_id_<?php echo $child_lv2->id; ?>"><?php echo $child_lv2->title; ?></label>


							</div>

						</div>
					<?php } ?>
				</div>
			<?php } ?>
	</div>
	<div class="submit">
		<input type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>">
	</div>
	</form>
	<?php
} else {
	?>
	<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

	<?php
}
?>


</div>