<?php
/**
 * @version		: richgame.php 2015-06-24 21:06:39$
 * @author		EFATEK 
 * @package		Richgame
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Pba
$controller = JControllerLegacy::getInstance('Richgame');


// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));

// Redirect if set by the controller
$controller->redirect();