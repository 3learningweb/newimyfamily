<?php

/**
 * @version		: controller.php 2015-12-10 21:06:39$
 * @author		EFATEK 
 * @package		Gathercard
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * CHOOSE Component Controller
 */
class GathercardController extends JControllerLegacy {

}