<?php
/**
 * @version		: default.php 2015-12-10 21:06:39$
 * @author		EFATEK 
 * @package		gathercard
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');
$topic = $app->input->getInt('topic');
$model=&$this->getModel('game');
$topic=$model->getTopic();

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

?>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}
</style>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {	
			$('#button1').click(function() {
				window.location="index.php?option=com_gathercard&view=game&layout=result&Itemid=<?php echo $itemid;?>"
			});
			$('#button2').click(function() {
				window.location="index.php?option=com_gathercard&view=game&Itemid=<?php echo $itemid;?>"
			});
		});
	})(jQuery);
	
</script>

<div class="com_gathercard">	
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
		<div class="game_block">
			<div class="game_page-header">
				<div class="title">
					家庭聚會卡
				</div>
			</div>
			
			<div class="task_block">
				<div>
					<div class="task">
						任務:<?php echo $topic['topic'];?>
					</div>
					<div class="score">
						達成獲得:<?php echo $topic['point'];?>分
					</div>
				</div>
			</div>
			<br />
			<div align="center">
			<input type="button" value="遊戲結束" name="button1" id="button1">
			<input type="button" value="任務達成" name="button2" id="button2">
			</div>
		</div>

		<div class="choose_footer">
		</div>
</div>

