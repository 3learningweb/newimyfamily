<?php
/**
 * @version		: default.php 2015-12-10 21:06:39$
 * @author		EFATEK 
 * @package		gathercard
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

?>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}
</style>

<div class="com_gathercard">	
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
		<div class="game_block">
			<div class="game_page-header">
				<div class="title">
					家庭聚會卡
				</div>
			</div>
				
			<div>
				<div class="intro_text">
					恭喜今天的獲勝小組！<br>
					遊戲中的五種關卡內容，是人們感受被愛的不同喜好，也就是「愛的語言」：<br>
					<br />
					<strong>把愛說出來</strong>：喜歡聽見別人對自己的「肯定話語」<br>
					<strong>我們的珍珠時光</strong>：希望跟愛的人一起做想做的事<br>
					<strong>用行動證明</strong>：別人照顧自己生活起居時，會感受被愛<br>
					<strong>讓我們相親相愛</strong>：喜歡透過親密的身體接觸，感受與傳達關懷<br>
					<strong>愛的小驚喜</strong>：收到禮物就能感受到對方的愛與掛念<br>
					<br />
					家庭聚會是為了凝聚家人之間的感情，共創愉快的回憶，透過使用「家庭聚會卡」，可以藉此觀察每位家人喜歡被愛的方式，增進家人之間的相互了解，讓家庭聚會除了共享美食之外，還能有效增進情感交流！				
				</div>
				<div class="readmore">
					<table border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td><img style="width: 49px; height: 44px;" src="filesys/files/speaker/Speaker_04.png" alt="Speaker 04" width="49" height="44" />
								</td>
								<td style="background: url('filesys/files/speaker/Speaker_05.png') repeat-x;"><a href="index.php?option=com_checklist&view=item2&Itemid=256" style="color: #333333;"><span style="font-size: 12pt;">想知道更多向家人表達愛的方法嗎?</span></a>
								</td>
								<td><img src="filesys/files/speaker/Speaker_06.png" alt="" width="16" height="44" />
								</td>
							</tr>
						</tbody>
					</table>
					<table border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td><img style="width: 49px; height: 44px;" src="filesys/files/speaker/Speaker_34.png" alt="Speaker 34" width="49" height="44" />
								</td>
								<td style="background: url('filesys/files/speaker/Speaker_05.png') repeat-x;"><a href="https://ilove.moe.edu.tw/index.php?option=com_vquiz&view=quizmanager&id=16&Itemid=266" style="color: #333333;" target="_blank"><span style="font-size: 12pt;">愛的語言：想知道自己的愛的語言嗎？</span></a>
								</td>
								<td><img src="filesys/files/speaker/Speaker_06.png" alt="" width="16" height="44" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="choose_footer">
		</div>
</div>

