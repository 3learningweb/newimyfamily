<?php
/**
 * @version		: default.php 2015-12-10 21:06:39$
 * @author		EFATEK 
 * @package		gathercard
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}
</style>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {	
			$('#choose1').click(function() {
				window.location="index.php?option=com_gathercard&view=game&layout=game2&Itemid=<?php echo $itemid;?>&topic=1"
			});
			$('#choose2').click(function() {
				window.location="index.php?option=com_gathercard&view=game&layout=game2&Itemid=<?php echo $itemid;?>&topic=2"
			});
			$('#choose3').click(function() {
				window.location="index.php?option=com_gathercard&view=game&layout=game2&Itemid=<?php echo $itemid;?>&topic=3"
			});
			$('#choose4').click(function() {
				window.location="index.php?option=com_gathercard&view=game&layout=game2&Itemid=<?php echo $itemid;?>&topic=4"
			});
			$('#choose5').click(function() {
				window.location="index.php?option=com_gathercard&view=game&layout=game2&Itemid=<?php echo $itemid;?>&topic=5"
			});
		});
	})(jQuery);
	
</script>

<div class="com_gathercard">	
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
		<div class="game_block">
			<div class="game_page-header">
				<div class="title">
					家庭聚會卡
				</div>
			</div>
				
			<div id="mychoose_block">
				<div class="mychoose" id="choose1">
					<img src="<?php echo JURI::root();?>components/com_gathercard/assets/images/choose1.png" alt="把愛說出來" />
				</div>
				<div class="mychoose" id="choose2">
					<img src="<?php echo JURI::root();?>components/com_gathercard/assets/images/choose2.png" alt="我們的珍珠時光" />
				</div>
				<div class="mychoose" id="choose3">
					<img src="<?php echo JURI::root();?>components/com_gathercard/assets/images/choose3.png" alt="行動證明我愛你" />
				</div>
				<div class="mychoose" id="choose4">
					<img src="<?php echo JURI::root();?>components/com_gathercard/assets/images/choose4.png" alt="讓我們相親相愛" />
				</div>
				<div class="mychoose" id="choose5">
					<img src="<?php echo JURI::root();?>components/com_gathercard/assets/images/choose5.png" alt="愛的小驚喜" />
				</div>
			</div>
	
		</div>

		<div class="choose_footer">
		</div>
</div>

