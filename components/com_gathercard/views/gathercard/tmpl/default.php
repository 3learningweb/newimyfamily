<?php
/**
 * @version		: default.php 2015-12-10 21:06:39$
 * @author		EFATEK 
 * @package		gathercard
 * @copyright	Copyright (C) 2015- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>
<script language="JavaScript">
	(function($) {
		$(document).ready(function() {	
			$('input[name=submit0]').click(function() {
				$("#step1").show();
				$("#step0").hide();
				$("#step2").hide();
				$("#step3").hide();
			});
			$('input[name=submit1]').click(function() {
				$("#step2").show();
				$("#step0").hide();
				$("#step1").hide();
				$("#step3").hide();
			});
			$('input[name=submit2]').click(function() {
				$("#step3").show();
				$("#step0").hide();
				$("#step1").hide();
				$("#step2").hide();
			});
			$('input[name=submit3]').click(function() {
				window.location="<?php echo JURI::root();?>index.php?option=com_gathercard&view=game&Itemid=<?php echo $itemid;?>";
			});
		});
	})(jQuery);
	
</script>

<div class="com_choose">
	<!-- social button -->
	<div class="btn-group pull-right">
		<ul class="dropdown-menu actions">
			<?php echo JHtml::_('toolsbar._components'); ?>
		</ul>
	</div>
	
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>

	<div class="component_intro" id="step0">
		<div class="intro_text">
			家族聚會的時候，除了聊天，還有很多有趣的事可以一起做，透過家庭聚會卡，炒熱聚會氣氛吧!<br>
			<input type="button" name="submit0" id="submit0" value="下一步">
		</div>
	</div>
	<div class="component_intro" id="step1" style="display: none">
		<div class="intro_text">
			以下透過分組競賽，進行任務積分遊戲：<br>
			Step1.請今日聚會成員進行分組，參加遊戲<br>
			★可以一個家庭一組，或是依照今日成員人數至少分為4組〈鼓勵每位家庭成員一同參與活動〉<br>
			<input type="button" name="submit1" id="submit1" value="下一步">
		</div>
	</div>
	<div class="component_intro" id="step2" style="display: none">
		<div class="intro_text">
			Step2.小組輪流抽取任務卡，完成任務，獲得積分<br>
			★共有5種關卡，關卡選擇後將隨機出現指定任務，請每組依序派人輪流抽卡，達成任務就能獲得分數，積分最高組將得到本次聚會的獎勵！<br>
			<input type="button" name="submit2" id="submit2" value="下一步">
		</div>
	</div>
	<div class="component_intro" id="step3" style="display: none">
		<div class="intro_text">
			Step3.全家共同討論，設定遊戲獲勝獎勵：<br>
			請大家一起討論：這次家族聚會的獲勝小組獎勵是：______________<br>
			<input type="button" name="submit3" id="submit3" value="下一步">
		</div>
	</div>
	

</div>