<?php

/**
 * @version		: game.php 2015-12-10 05:06:09$
 * @author		efatek 
 * @package		com_gathercard
 * @copyright	Copyright (C) 2015- efatek. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modellist');

class GathercardModelGame extends JModelList {

	/**
	 * @var object item
	 */
	protected $item;

	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState() {
		$app = JFactory::getApplication();

		$id	= $app->input->getInt('id');
		$this->setState('item.id', $id);

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

		$limit	= $app->input->getInt('limit',  $app->getCfg('list_limit', 0));
		$this->setState('list.limit', $limit);

		$limitstart	= $app->input->getInt('limitstart', 0);
		$this->setState('list.start', $limitstart);


	}
	function getTopic(){
		$app = JFactory::getApplication();
		$idx = $app->input->getInt('topic',0);
		//把愛說出來
		$topic[1][1]=array("topic"=>"感謝這次張羅家庭聚會的負責人", "point"=>"3");
		$topic[1][2]=array("topic"=>"選擇兩位在場的長輩，說聲讚美的話(例：您今天打扮的好美)", "point"=>"2");
		$topic[1][3]=array("topic"=>"對在場每位父親說一句感謝", "point"=>"2");
		$topic[1][4]=array("topic"=>"對在場每位母親說一句感謝", "point"=>"2");
		$topic[1][5]=array("topic"=>"選擇兩位在場的家人，大聲對他說「我愛你」", "point"=>"5");
		$topic[1][6]=array("topic"=>"選擇兩位在場的年輕人，說幾句鼓勵、祝福的話", "point"=>"5");
		$topci[1][7]=array("topic"=>"說出在場每位家人的一個優點", "point"=>"5");
		//我們的珍珠時光
		$topic[2][1]=array("topic"=>"練習用眼睛交流：找一位最親近的家人，與他四目交接十秒鐘", "point"=>"3");
		$topic[2][2]=array("topic"=>"邀請2~3位家人，一起合唱一首描述家人情感融洽的歌曲", "point"=>"3");
		$topic[2][3]=array("topic"=>"為大家高歌一曲(自選)", "point"=>"3");
		$topic[2][4]=array("topic"=>"全組一起唱一首兒歌", "point"=>"3");
		$topic[2][5]=array("topic"=>"才藝表演", "point"=>"5");
		$topic[2][6]=array("topic"=>"全組一起5連拍", "point"=>"5");
		//用行動證明
		$topic[3][1]=array("topic"=>"餐後收拾碗盤", "point"=>"3");
		$topic[3][2]=array("topic"=>"切水果給大家吃", "point"=>"3");
		$topic[3][3]=array("topic"=>"夾一道菜餵在場一位長輩吃", "point"=>"3");
		$topic[3][4]=array("topic"=>"安排拍攝今日聚會團體照", "point"=>"3");
		$topic[3][5]=array("topic"=>"幫每個人夾菜", "point"=>"5");
		$topic[3][6]=array("topic"=>"擔任下一次聚會的負責人", "point"=>"5");
		//讓我們相親相愛
		$topic[4][1]=array("topic"=>"幫今日聚會籌辦人按摩搥背1分鐘", "point"=>"3");
		$topic[4][2]=array("topic"=>"拍拍弟弟妹妹的肩膀，給予支持鼓勵", "point"=>"3");
		$topic[4][3]=array("topic"=>"幫今日最年輕家庭成員，按摩1分鐘", "point"=>"3");
		$topic[4][4]=array("topic"=>"幫今日最資深家庭成員，按摩1分鐘", "point"=>"3");
		$topic[4][5]=array("topic"=>"選一位家人，親他的臉頰", "point"=>"5");
		$topic[4][6]=array("topic"=>"給今日最資深家庭成員一個「 10秒以上的大擁抱」", "point"=>"5");
		//愛的小驚喜
		$topic[5][1]=array("topic"=>"說出在場小朋友最喜歡的零食，至少猜對三位", "point"=>"3");
		$topic[5][2]=array("topic"=>"分享：曾經收到由家人贈送，深刻感受到家人關心的禮物", "point"=>"3");
		$topic[5][3]=array("topic"=>"蒐集家庭聚會資訊：記錄每組想吃的料理，為下次家庭聚會做準備", "point"=>"5");
		$topic[5][4]=array("topic"=>"說出每位同組成員喜歡的蛋糕口味", "point"=>"5");
		
		$cnt=count($topic[$idx]);
		$rdn=rand(1,$cnt);
		return $topic[$idx][$rdn];
	}
	
}
