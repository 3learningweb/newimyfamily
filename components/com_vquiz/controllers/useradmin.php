<?php
/*------------------------------------------------------------------------
# com_vquiz - vquiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2014 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class VquizControllerUseradmin extends VquizController
{
	
	function __construct()
	{
		parent::__construct();
		
		$this->model = $this->getModel('useradmin');
		
		JRequest::setVar( 'view', 'useradmin' );
	
		// Register Extra engineers
		$this->registerTask( 'add'  , 	'edit' );
		$this->registerTask( 'unpublish',	'publish' );
		
 

	}

	/**
	 * display the edit form
	 * @return void
	 */
	function edit()
	{
		JRequest::setVar( 'view', 'useradmin' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		
		parent::display();
	}

	/**
	 * save a record (and redirect to main page)
	 * @return void
	 */
 
	
	function apply()
	{
		
		if($this->model->store()) {
			$msg = JText::_('RECORD_SAVED');
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=useradmin&task=edit&cid[]='.JRequest::getInt('id', 0)), $msg );
		} else {
			jerror::raiseWarning('', $this->model->getError());
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=useradmin&task=edit&cid[]='.JRequest::getInt('id', 0)) );
		}

	}
	
	/**
	 * Publish record(s)
	 * @return void
	 */
	function publish()
	{
		$task		= JRequest::getCmd( 'task' );
		$msg  	= $task == 'publish' ? JText::_( 'Record(s) Published successfully' ) : JText::_( 'Record(s) Unublished successfully' );
		if($this->model->publish()) {
			//$msg = JText::_( 'Record Published successfully' );
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=useradmin'), $msg );
		} else {
			jerror::raiseWarning('', $this->model->getError());
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=useradmin'));
		}

	}
	
	 

	/**
	 * remove record(s)
	 * @return void
	 */
	function remove()
	{
		
		if($this->model->delete()) {
			$msg = JText::_( 'Record deleted successfully' );
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=useradmin'), $msg );
		} else {
			jerror::raiseWarning('', $this->model->getError());
			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=useradmin'));
		}
		
	}

	/**
	 * cancel editing a record
	 * @return void
	 */
	function cancel()
	{
		$msg = JText::_( 'CANCEL' );
		$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=useradmin'), $msg );
	}
 
			
	public function login()
			{
			 $app = JFactory::getApplication();
				// Populate the data array:
				$data = array();
				//$data['return'] = base64_decode($app->input->post->get('return', '', 'BASE64'));
				$data['return'] = base64_decode(JRequest::getVar('return', '', 'POST', 'BASE64'));
				$data['username'] = JRequest::getVar('username', '', 'method', 'username');
				$data['password'] = JRequest::getString('password', '', 'post', JREQUEST_ALLOWRAW);
				//$data['secretkey'] = JRequest::getString('secretkey', '');
		
				// Set the return URL if empty.
				if (empty($data['return']))
				{
					//$data['return'] = 'index.php?option=com_users&view=profile';
					$data['return'] ='index.php?option=com_vquiz&view=useradmin&tmpl=component';
				}
		
				// Set the return URL in the user state to allow modification by plugins
				$app->setUserState('users.login.form.return', $data['return']);
		
				// Get the log in options.
				$options = array();
				$options['remember'] = $this->input->getBool('remember', false);
				$options['return'] = $data['return'];
		
				// Get the log in credentials.
				$credentials = array();
				$credentials['username']  = $data['username'];
				$credentials['password']  = $data['password'];
				//$credentials['secretkey'] = $data['secretkey'];
		
				// Perform the log in.
				if (true === $app->login($credentials, $options))
				{
					// Success
					if ($options['remember'] = true)
					{
						$app->setUserState('rememberLogin', true);
					}
		
					$app->setUserState('users.login.form.data', array());
					$msg= JText::_( 'You are Successfully  Logged In..!! ' );	
					$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=useradmin&layout=admindashbard'), $msg );

				}
				else
				{
					// Login failed !
					$data['remember'] = (int) $options['remember'];
					$app->setUserState('users.login.form.data', $data);
					$msg = JText::_( 'Username and password do not match or you do not have an account yet. ' );
					$this->setRedirect(JRoute::_( 'index.php?option=com_vquiz&view=useradmin'), $msg );
				}
 				
				
				
				
			}
			
			
		function drawLineChart()
		{ 
		
		//JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );
		
		$model = $this->getModel('useradmin');
		
		$obj = $model->getLinechart();	
		
		jexit(json_encode($obj));
		
		
		} 
	
	function drawpieChart()
			{ 
			
				//JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );
				
				$model = $this->getModel('useradmin');
		
				$obj = $model->getpiechart();	
			
				jexit(json_encode($obj));
		
		
			} 
	
		function drawflagChart()
			{ 
			
				//JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );
				
				$model = $this->getModel('useradmin');
		
				$obj = $model->getflagpiechart();	
			
				jexit(json_encode($obj));
		
		
			} 
	
	 function drawgeoChart()
			{ 
			
				//JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );
				
				$model = $this->getModel('useradmin');
		
				$obj = $model->getgeochart();	
			
				jexit(json_encode($obj));
		
		
			} 
			
			
		 
}