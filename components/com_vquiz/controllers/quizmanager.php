<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vquiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2014 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access
defined('_JEXEC') or die('Restricted access');

class VquizControllerQuizmanager extends VquizController {

	function __construct() {
		parent::__construct();
		$this->registerTask('add', 'edit');
	}

	function edit() {
		JRequest::setVar('view', 'quizmanager');
		JRequest::setVar('layout', 'quizzes');
		JRequest::setVar('layout', 'quizresult');
		JRequest::setVar('layout', 'description');
		JRequest::setVar('hidemainmenu', 1);
		parent::display();
	}

	function flagcount() {
		// Check for request forgeries
		JRequest::checkToken() or jexit('{"result":"error", "error":"' . JText::_('INVALID_TOKEN') . '"}');
		$model = $this->getModel('quizmanager');
		// ansswer querty and jo send karna hai sab ko
		$obj = $model->flagcount();
		jexit(json_encode($obj));
	}

	function penalityexpiredtime() {
		// Check for request forgeries
		JRequest::checkToken() or jexit('{"result":"error", "error":"' . JText::_('INVALID_TOKEN') . '"}');
		$model = $this->getModel('quizmanager');
		// ansswer querty and jo send karna hai sab ko
		$obj = $model->penalityexpiredtime();
		jexit(json_encode($obj));
	}

	function quizzesdesc() {
		// Check for request forgeries
		JRequest::checkToken() or jexit('{"result":"error", "error":"' . JText::_('INVALID_TOKEN') . '"}');
		$model = $this->getModel('quizmanager');
		// ansswer querty and jo send karna hai sab ko
		$obj = $model->quizzesdesc();
		jexit(json_encode($obj));
	}

	function nextslide() {
		// Check for request forgeries
		JRequest::checkToken() or jexit('{"result":"error", "error":"' . JText::_('INVALID_TOKEN') . '"}');
		$model = $this->getModel('quizmanager');
		// ansswer querty and jo send karna hai sab ko
		$obj = $model->nextslide();
		jexit(json_encode($obj));
	}

	function backslide() {

		JRequest::checkToken() or jexit('{"result":"error", "error":"' . JText::_('INVALID_TOKEN') . '"}');
		$model = $this->getModel('quizmanager');
		// ansswer querty and jo send karna hai sab ko
		$obj = $model->backslide();
		jexit(json_encode($obj));
	}

	function showresult() {
		// Check for request forgeries
		JRequest::checkToken() or jexit('{"result":"error", "error":"' . JText::_('INVALID_TOKEN') . '"}');

		$model = $this->getModel('quizmanager');

		$obj = $model->showresult();

		jexit(json_encode($obj));
	}

	function share_snapshot() {
		// Check for request forgeries
		JRequest::checkToken() or jexit('{"result":"error", "error":"' . JText::_('INVALID_TOKEN') . '"}');
		$model = $this->getModel('quizmanager');
		// ansswer querty and jo send karna hai sab ko
		$obj = $model->share_snapshot();
		jexit(json_encode($obj));
	}

	function sendmail() {
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->clear('quizoptions');
		$certificate_name = JRequest::getVar('certificate_name');
		$optinscoretype = JRequest::getInt('optinscoretype');
		$date = JFactory::getDate();
		$datetime = $date->toSQL();
		$db = JFactory::getDbo();
		$mailer = JFactory::getMailer();
		$config = JFactory::getConfig();

		$obj = new stdClass();
		$obj->result = "error";
		$email = JRequest::getVar('email');
		$mailer->setSender($config->get('mailfrom'));

		$query = 'select * from #__vquiz_configuration';
		$db->setQuery($query);
		$confiresult = $db->loadObject();

		if ($optinscoretype == 2)
			$text = $confiresult->mailformat2;
		else
			$text = $confiresult->mailformat;

		$mailsubject = $confiresult->subject;

		$query = 'select * from #__vquiz_quizresult where userid=' . $db->quote($user->id) . 'order by id desc';
		$db->setQuery($query);
		$quizesult = $db->loadObject();

		$score = $quizesult->score;
		$maxscore = $quizesult->maxscore;
		$spenttime = $quizesult->quiz_spentdtime;
		$quiztitle = $quizesult->quiztitle;
		$startdatetime = $quizesult->startdatetime;
		$enddatetime = $quizesult->enddatetime;
		$passed_score = $quizesult->passed_score;
		$flag = $quizesult->flag;


		if ($maxscore > 0)
			$persentagescore = round($score / $maxscore * 100, 2) > 100 ? 100 : round($score / $maxscore * 100, 2);
		else
			$persentagescore = $score;


		if ($persentagescore >= $passed_score)
			$passed_text = '<span style="color:green">Passed!!</span>';
		else
			$passed_text = '<span style="color:red">Failed!!</span>';

		if (!empty($certificate_name))
			$username = $certificate_name;
		else if (!$user->get('guest'))
			$username = $user->username;
		else
			$username = 'Guest';

		if (strpos($text, '{username}') !== false) {
			$text = str_replace('{username}', $username, $text);
		}
		if (strpos($text, '{quizname}') !== false) {
			$text = str_replace('{quizname}', $quiztitle, $text);
		}
		if (strpos($text, '{userscore}') !== false) {
			$text = str_replace('{userscore}', $score, $text);
		}
		if (strpos($text, '{maxscore}') !== false) {
			$text = str_replace('{maxscore}', $maxscore, $text);
		}
		if (strpos($text, '{starttime}') !== false) {
			$text = str_replace('{starttime}', $startdatetime, $text);
		}
		if (strpos($text, '{endtime}') !== false) {
			$text = str_replace('{endtime}', $enddatetime, $text);
		}
		if (strpos($text, '{spenttime}') !== false) {
			$text = str_replace('{spenttime}', $spenttime, $text);
		}
		if (strpos($text, '{passedscore}') !== false) {
			$text = str_replace('{passedscore}', $passed_score, $text);
		}
		if (strpos($text, '{percentscore}') !== false) {
			$text = str_replace('{percentscore}', $persentagescore, $text);
		}
		if (strpos($text, '{passed}') !== false) {
			$text = str_replace('{passed}', $passed_text, $text);
		}
		if (strpos($text, '{flag}') !== false) {
			$text = str_replace('{flag}', $flag, $text);
		}

		$body = $text;

		$mailer->setSubject($mailsubject);

		$mailer->setBody($body);

		$recievermail = $email;

		$mailer->addRecipient($recievermail);
		$mailer->IsHTML(true);
		$send = $mailer->send();

		if ($send == true)
			$obj->result = 1;
		else
			$obj->result = 0;

		jexit(json_encode($obj));
	}

	function certificate_text() {
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		//$session->clear('quizoptions');
		$date = JFactory::getDate();
		$datetime = $date->toSQL();
		$db = JFactory::getDbo();
		$certificate_name = JRequest::getVar('certificate_name');
		$optinscoretype = JRequest::getInt('optinscoretype');

		$obj = new stdClass();
		$obj->result = "error";


		$query = 'select * from #__vquiz_configuration';
		$db->setQuery($query);
		$confiresult = $db->loadObject();

		if ($optinscoretype == 2)
			$text = $confiresult->mailformat2;
		else
			$text = $confiresult->mailformat;

		//$text = $confiresult->mailformat;


		$query = 'select * from #__vquiz_quizresult where userid=' . $db->quote($user->id) . 'order by id desc';
		$db->setQuery($query);
		$quizesult = $db->loadObject();

		$score = $quizesult->score;
		$maxscore = $quizesult->maxscore;
		$spenttime = $quizesult->quiz_spentdtime;
		$quiztitle = $quizesult->quiztitle;
		$startdatetime = $quizesult->startdatetime;
		$enddatetime = $quizesult->enddatetime;
		$passed_score = $quizesult->passed_score;
		$flag = $quizesult->flag;


		if ($maxscore > 0)
			$persentagescore = round($score / $maxscore * 100, 2) > 100 ? 100 : round($score / $maxscore * 100, 2);
		else
			$persentagescore = $score;


		if ($persentagescore >= $passed_score)
			$passed_text = '<span style="color:green">Passed!!</span>';
		else
			$passed_text = '<span style="color:red">Failed!!</span>';

		if (!empty($certificate_name))
			$username = $certificate_name;
		else if (!$user->get('guest'))
			$username = $user->username;
		else
			$username = 'Guest';


		if (strpos($text, '{username}') !== false) {
			$text = str_replace('{username}', $username, $text);
		}
		if (strpos($text, '{quizname}') !== false) {
			$text = str_replace('{quizname}', $quiztitle, $text);
		}
		if (strpos($text, '{userscore}') !== false) {
			$text = str_replace('{userscore}', $score, $text);
		}
		if (strpos($text, '{maxscore}') !== false) {
			$text = str_replace('{maxscore}', $maxscore, $text);
		}
		if (strpos($text, '{starttime}') !== false) {
			$text = str_replace('{starttime}', $startdatetime, $text);
		}
		if (strpos($text, '{endtime}') !== false) {
			$text = str_replace('{endtime}', $enddatetime, $text);
		}
		if (strpos($text, '{spenttime}') !== false) {
			$text = str_replace('{spenttime}', $spenttime, $text);
		}
		if (strpos($text, '{passedscore}') !== false) {
			$text = str_replace('{passedscore}', $passed_score, $text);
		}
		if (strpos($text, '{percentscore}') !== false) {
			$text = str_replace('{percentscore}', $persentagescore, $text);
		}
		if (strpos($text, '{passed}') !== false) {
			$text = str_replace('{passed}', $passed_text, $text);
		}
		if (strpos($text, '{flag}') !== false) {
			$text = str_replace('{flag}', $flag, $text);
		}


		$obj->result = $text;


		jexit(json_encode($obj));
	}

}