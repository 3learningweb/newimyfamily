<?php
/*------------------------------------------------------------------------
# com_vquiz - vquiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2014 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class VquizController extends JControllerLegacy
{
	function display($cachable = false, $urlparams = false)
	{
     parent::display();
	}

	function showToolbar()
	{
		$view = JRequest::getVar('view', 'vquiz');
		
		JSubMenuHelper::addEntry( '<span class="add_item hasTip" title="'.JText::_('DASHBOARD').'">'.JText::_('DASHBOARD').'</span>' , 'index.php?option=com_vquiz&view=vquiz', $view == 'vquiz' );
		JSubMenuHelper::addEntry( '<span class="add_item hasTip" title="'.JText::_('CONFIGURATION').'">'.JText::_('CONFIGURATION').'</span>' , 'index.php?option=com_vquiz&view=configuration', $view == 'configuration' );
		JSubMenuHelper::addEntry( '<span class="add_item hasTip" title="'.JText::_('MAILINBOX').'">'.JText::_('MAILINBOX').'</span>' , 'index.php?option=com_vquiz&view=mailinbox', $view == 'mailinbox' );
		JSubMenuHelper::addEntry( '<span class="add_item hasTip" title="'.JText::_('QUIZ_MANAGER').'">'.JText::_('QUIZ_MANAGER').'</span>' , 'index.php?option=com_vquiz&view=quizmanager', $view == 'quizmanager' );

	}

 

}