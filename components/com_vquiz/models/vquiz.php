<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.modellist');
class VquizModelVquiz extends JModelList
{
    var $_total = null;
	var $_pagination = null;
	function __construct()
	{
		parent::__construct();		
		$db =JFactory::getDBO();
        $mainframe = JFactory::getApplication();
 
 		$app = JFactory::getApplication('site');
		$params = $app->getParams();
		$this->setState('params', $params);
		

		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');
		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
	
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}
	
	function _buildQuery()
	{								
	 $query='select * from #__vquiz_category  ';
			
		return $query;	
	}
	function _buildFilter()
	{
			$user = JFactory::getUser();
			$user = JFactory::getUser();
			$date = JFactory::getDate();
			$app = JFactory::getApplication();
			$lang = JFactory::getLanguage();
		    $where = array();
			
			$where[] = ' published =1';
                        $where[] = ' parent_id=1';
			$where[] = ' access in ('.implode(',', $user->getAuthorisedViewLevels()).')';
			if($app->getLanguageFilter())	{
			$where[] = ' language in ('.$this->_db->quote($lang->getTag()).', '.$this->_db->Quote('*').')';
			}
			$filter = ' where ' . implode(' and ', $where);
			
			return $filter;
		
	}
	
		function _buildOrderBy()
		{
			$orderby ='order by ordering asc';	
			return $orderby;
		}
	
	  function getTotal(){
		 
			if (empty($this->_total)) {
				$query = $this->_buildQuery();
				$query .= $this->_buildFilter();
				$query .= $this->_buildOrderBy();
				
				$this->_total = $this->_getListCount($query);       
			}
			return $this->_total;
		}
	
  function getPagination(){
		// Load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
		return $this->_pagination;
	}
	
		function getQuizcategory()
		{
				if (empty($this->_data)) {
				$query = $this->_buildQuery();
				$query .= $this->_buildFilter();
				$query .= $this->_buildOrderBy();
				$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));    
			}
			return $this->_data;
			
		}
		
 		
					function getConfiguration()
					{
						$query='select * from #__vquiz_configuration';
						$this->_db->setQuery($query);
						$result = $this->_db->loadObject();
					 return $result;
					}
						
						
						function child_category()
								{
								
								$parentid = JRequest::getInt('parentid');
								$itemid = JRequest::getInt('itemid');
								
								$query ='select a.id , a.quiztitle , a.level ';
								$query .=' from #__vquiz_category As a ';
								$query .=' LEFT join #__vquiz_category AS b ON a.lft > b.lft AND a.rgt < b.rgt';
								
 								$where = array();
  								if ($parentid)
								{
									if ($id = $itemid)
									{
									$query .=' LEFT join #__vquiz_category AS p ON p.id = ' .$id;
									//$where[] =' NOT(a.lft >= p.lft AND a.rgt <= p.rgt) ';
									$where[] =' NOT(a.lft <= p.lft AND a.rgt >= p.rgt) ';
  									}
								}
 								$where[] = 'a.id !=1';
								$where[] = 'a.parent_id='.$id;
								$where[] = 'a.published=1';
								$filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';
								$query .= $filter;
								$query .=' group by a.id, a.quiztitle, a.level, a.lft, a.rgt, a.parent_id order by a.lft ASC';
						 
	 
								$this->_db->setQuery($query);
								$result=$this->_db->loadObjectList();
  								
								return $result;
							}
		
 
		
	
}
 