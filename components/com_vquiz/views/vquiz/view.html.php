<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author Team WDMtech
# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support: Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );
class VquizViewVquiz extends JViewLegacy
{ 
 
    function display($tpl = null)
    {
		
		$document =  JFactory::getDocument();
		$this->state	= $this->get('State');
		$this->params = $this->state->get('params');
		
		$menukeyword=$this->params->get('menu-meta_keywords');
		$menudescription=$this->params->get('menu-meta_description');
		
		if($menukeyword)
			$document->setMetadata('keywords', $menukeyword);
		if($menudescription)
			$document->setDescription($menudescription);
		
		$this->quizcategory = $this->get('Quizcategory');
		$this->configuration = $this->get('Configuration');
		$pagination =$this->get('Pagination');
		$this->assignRef('pagination', $pagination);
		parent::display($tpl);
		
    }
 
}



			  

			  

