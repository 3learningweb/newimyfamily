<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.tooltip');
$document = JFactory::getDocument();
if(version_compare(JVERSION, '3.0', '>='))
JHtml::_('formbehavior.chosen', 'select'); 
/*$document->addScript('components/com_vquiz/assets/js/library.js');*/
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/adminpanel.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/icomoon.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');
/*$document->addScript('components/com_vquiz/assets/js/jquery-ui.js');
$document->addStyleSheet('components/com_vquiz/assets/css/dialog.css');*/

?>
<script  src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script> 
<script type="text/javascript">
var jq=jQuery.noConflict();
jq(document).ready(function(){
jq( "#flagreset" ).click(function() {
jq('input[name="flagcount"]').val(0);
jq('input[name="flagcount_show"]').val(0);
});

 
		var single=true;
		var multiple=false;
 
			var dropdownvalue=jq('#optiontype').val();
			if(dropdownvalue==2){
			multiple=true;	
			}
			else if(dropdownvalue==1){
			single=true;
			}

			jq("#optiontype").change(function(){
				

					if(jq(this).val()==1){	
					single=true;
                    jq(".options_answer").attr('type','radio');
					multiple=false;

					}

					if(jq(this).val()==2){	
					single=false;
					jq(".options_answer").attr('type','checkbox');
					multiple=true;

					}

			});
			
			
			
			var singleoption_score=true;
			var deiifrentoption_score=false;
			jq('.options_answer').css('display','none');
 
			var dropdownvalue='<?php echo $this->optinscoretype->optinscoretype;?>'
			if(dropdownvalue==2){
			deiifrentoption_score=true;	
			jq('.scoretype_tr').hide();
			jq('.penalitytype_tr').hide();
			jq('.questiontyp_tr').hide();
			jq('.options_score').css('display','block'); 
			jq('.options_answer').css('display','none');
			
			}
			else if(dropdownvalue==1){
			singleoption_score=true;
			jq('.scoretype_tr').show();
			jq('.penalitytype_tr').show();
			jq('.questiontyp_tr').show();
			jq('.options_answer').css('display','block');
			jq('.options_score').css('display','none');
			}

			var addDiv = jq('#addinput');
			var i =<?php echo (int)count($this->options); ?>;

			jq('#addNew').live('click', function() {
 
 				if(deiifrentoption_score==true)
				{
					var htt = '<input type="text" name="options_score[]"  value="" style="width:16%;" class="options_score"/>';
					
					if(multiple==true)
					{
						var html = '<input type="checkbox" name="correct_ans[]" value="'+i+'"  style=display:none;"  class="options_answer"/>';
					}
					else if(single==true)
					{
						var html = '<input type="radio" name="correct_ans"  value="'+i+'"  style=display:none;"  class="options_answer"/>';
					}
					
				}
				else if(singleoption_score==true)
				{
					var htt ='<input type="text" name="options_score[]"  value="" style="width:16%;display:none;" class="options_score"/>';
					
					if(multiple==true)
					{
						var html = '<input type="checkbox" name="correct_ans[]"  value="'+i+'" class="options_answer" />';
					}
					else if(single==true)
					{
						var html = '<input type="radio" name="correct_ans"  value="'+i+'" class="options_answer"  />';
					}
					
				}
	

jq('<tr><td></td><td valign="middle" align="center"><input style="width:96%; padding:1%; margin:1%;" type="text" id="qoption"  name="qoption[]" value=""  /></td><td valign="middle" align="center"><label class="sradio">'+html+'</label></td><td valign="middle" align="center" >'+htt+'</td><td valign="middle" align="center"><a href="javascript:void(0);" class="remNew btn">Remove</a></td>').appendTo(addDiv);

				i++;
				
				return false;
				
			});



			jq('.remNew').live('click', function() {
				
				

					jq(this).parent().parent().remove();
					
					jq('#addinput>p').each(function(i){
						
					jq(this).find('input:radio').val(i);
			
				   });

	       });

	
	
var checktotaltime='<?php echo $this->optinscoretype->total_timelimit;?>'

if(checktotaltime>0){
	 
	 jq('.question_timetr').show();
	 jq('.expiredpenality_tr').show();
}
else{
	 
	 jq('.question_timetr').hide();
	 jq('.expiredpenality_tr').hide();
}
	
	            
});

</script>

<script>
Joomla.submitbutton = function(task) {
		if (task == 'cancel') {
			
			Joomla.submitform(task, document.getElementById('adminForm'));
		} else {
			var text=jQuery("textarea#qtitle").val();
			if(text==''){
				alert('<?php echo JText::_('QUESTION_TITLE_NOT_BLANK', true); ?>');
				document.adminForm.qtitle.focus();
				return false;
			}						
			Joomla.submitform(task, document.getElementById('adminForm'));
			
		}
	}
</script>
<div class="manage_quizzes">
<div id="toolbar" class="btn-toolbar">
<div id="toolbar-apply" class="btn-wrapper">
	<button class="btn btn-small btn-success" onclick="Joomla.submitbutton('apply')">
	<span class="icon-apply icon-white"></span> <?php echo JText::_('SAVE');?></button>
</div>
<div id="toolbar-save" class="btn-wrapper">
	<button class="btn btn-small" onclick="Joomla.submitbutton('save')">
	<span class="icon-save"></span> <?php echo JText::_('SAVE_AND_CLOSE');?></button>
</div>
<div id="toolbar-cancel" class="btn-wrapper">
	<button class="btn btn-small" onclick="Joomla.submitbutton('cancel')">
	<span class="icon-cancel"></span> <?php echo JText::_('CANCEL');?></button>
</div>
</div>

<form action="<?php echo JRoute::_( 'index.php?option=com_vquiz'); ?>"method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col101 qform">
 
    <fieldset class="adminform">
    <legend><?php echo JText::_( 'DETAILS' ); ?></legend>

        <table class="adminform table table-striped">
        
<tr>
<td width="250"><?php echo JText::_('FLAGE_GIVEN_BY_USER'); ?></td>
<td><input type="text" name="flagcount_show"  id="flagcount_show" value="<?php echo $this->item->flagcount;?>" disabled="disabled"/>
<input type="hidden" name="flagcount"  id="flagcount" value="<?php echo $this->item->flagcount;?>"/>
<input type="button" class="btn" id="flagreset" value="<?php echo JText::_('Reset'); ?>"/>
</td>
</tr>            
<tr>
<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('QUIZCATEGORYTOLTIP'); ?>"><?php echo JText::_('QUIZ'); ?></label></td>
<td><input type="hidden" name="quizzesid" id="quizzesid" value="<?php echo $this->optinscoretype->id;?>" />
<label><?php echo $this->optinscoretype->quizzes_title;?></label>
</td>
  
</tr>
            
            
        <tr>
        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('STATUSTOLTIP'); ?>"><?php echo JText::_('PUBLISHEDCATEGORY'); ?></label></td>
        <td>
        <select  name="published" id="published" >
        <option value="1" <?php if($this->item->published==1) echo 'selected="selected"'; ?>><?php echo JText::_('published'); ?> </option>
        <option value="0" <?php if($this->item->published==0) echo 'selected="selected"'; ?>><?php echo JText::_('Unpublished'); ?> </option>
        </select>
        </td>
        </tr>

        <tr>
        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('ORDETING_TOLTIP'); ?>"><?php echo JText::_('ORDERING'); ?></label></td>
        <td>
        <input type="text" name="ordering" id="ordering" value="<?php echo $this->item->ordering;?>" />
        </td>
        </tr>


<tr class="question_timetr"> 
<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('QUESTIONTIMELIMITTOLTIP'); ?>"><?php echo JText::_('QUESTIONTIMELIMIT'); ?></label>

</td>
<td class="question_timetd">                    
<input type="text" name="question_timelimit"  id="question_timelimit" value="<?php echo $this->item->question_timelimit;?>" />
<select  name="questiontime_parameter" id="questiontime_parameter" >
<option value="seconds" <?php if($this->item->questiontime_parameter=='seconds') echo 'selected="selected"'; ?>><?php echo JText::_('SECNDS'); ?> </option>
<option value="minutes" <?php if($this->item->questiontime_parameter=='minutes') echo 'selected="selected"'; ?>><?php echo JText::_('MINUTES'); ?> </option>
</select>
</td>
</tr>
 

 		 
        <tr class="scoretype_tr">
        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('TOLTIP'); ?>"><?php echo JText::_('ENTER_SCORE'); ?></label></td>
        <td>
        <input type="text" name="score" id="score" value="<?php echo $this->item->score?>" />
        </td>
        </tr>
        
        <tr class="penalitytype_tr">
        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('PENALITYTOLTIP'); ?>"><?php echo JText::_('PENALITY'); ?></label></td>
        <td>
        <input type="text" name="penality" id="penality" value="<?php echo $this->item->penality?>" />
        </td>
        </tr>
        
        <tr class="expiredpenality_tr">
        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('TOLTIP'); ?>"><?php echo JText::_('PENALITY_AGTER_EXPIRED_QTIME'); ?></label></td>
        <td>
        <input type="text" name="expire_timescore" id="expire_timescore" value="<?php echo $this->item->expire_timescore?>" />
        </td>
        </tr>
        
        <tr class="questiontyp_tr">
        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('QUESTIONTYPETOLTIP'); ?>"><?php echo JText::_('QUESTIONTYPE'); ?></label></td>
        <td>
        <select  name="optiontype" id="optiontype" >
         
        <option value="1" <?php  if($this->item->optiontype==1) echo 'selected="selected"'; ?>><?php echo JText::_('SINGLECHOICE'); ?> </option>
        <option value="2" <?php  if($this->item->optiontype==2) echo 'selected="selected"'; ?> ><?php echo JText::_('MULTIPLECHOICE'); ?> </option>
        </select>
        </td>
        </tr>


	    <tr>
        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('QUESTIONTOLTIP'); ?>"><?php echo JText::_('QUESTION'); ?></label></td>
		<td>
				<?php 
                $editor = JFactory::getEditor();
                echo $editor->display("qtitle",  $this->item->qtitle, "400", "200", "20", "5", true, null, null, null, array('mode' => 'simple'));
				//echo $editor->display('qtitle', $this->item->qtitle, '400', '200', '20', '5',array('article' =>false,'sliders' =>false,'tabs' =>false,'pagebreak' =>false,'readmore' =>false));
                ?> 
        </td>
        </tr>

         <tr>
        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('EXPLANATIONTOLTIP'); ?>"><?php echo JText::_('EXPLANATION'); ?></label></td>
		<td>
 				<?php 
                $editor = JFactory::getEditor();
                echo $editor->display("explanation",  $this->item->explanation, "400", "200", "20", "5", true, null, null, null, array('mode' => 'simple'));
			   //echo $editor->display('explanation', $this->item->explanation, '400', '200', '20', '5',array('article' =>false,'sliders' =>false,'tabs' =>false,'pagebreak' =>false,'readmore' =>false));
                ?> 
        </td>
        </tr>
        </table>

	    

             <table width="100%" id="addinput">
             <tr>
             <th><input type="button" name="addoption" id="addNew" value="+" class="btn btn-success" /></th>
  
             <th><label class="hasTip" title="<?php echo JText::sprintf('OPTIONSTOLTIP'); ?>"><?php echo JText::_('OPTIONS'); ?></label></th>
             <th><label class="hasTip options_answer"  title="<?php echo JText::sprintf('ANSWEROLTIP'); ?>"><?php echo JText::_('Answer'); ?></label></th>
             <th><label class="hasTip options_score" title="<?php echo JText::sprintf('Score'); ?>"><?php echo JText::_('Score'); ?></label></th>
             <th><?php echo JText::_('Remove'); ?></th>

             </tr>

				<?php	for($i=0;$i<count($this->options);$i++)	 {?>
                <tr>
                <td></td>
               
                <td valign="middle" align="center"><input style="width:96%; padding:1%; margin:1%;" type="text" id="qoption"  name="qoption[]" value="<?php echo $this->options[$i]->qoption; ?>"/></td> <td valign="middle" align="center">
                <?php 
                if($this->item->optiontype==2)
                {?>
                <label class="mcheckbox" ><input type="checkbox" name="correct_ans[]" class="options_answer" value="<?php echo $i; ?>" 
                <?php if($this->options[$i]->correct_ans==1) { echo 'checked="checked"'; } ?>/></label>
                <?php }?>
                <?php if($this->item->optiontype==1)
                {?>
                <label class="sradio"  ><input type="radio" name="correct_ans"  class="options_answer" value="<?php echo $i; ?>"
                <?php if($this->options[$i]->correct_ans==1) { echo 'checked="checked"'; } ?>/></label>
                <?php }?></td>
                <td valign="middle" align="center"><input type="text" name="options_score[]"  value="<?php echo $this->options[$i]->options_score; ?>"  style="width:16%;"class="options_score"/></td>
                <td valign="middle" align="center">
                <a class="remNew btn" href="javascript:void(0);">Remove</a></td></tr>
                <?php   }?>
         </table>
        </fieldset>
 
</div> 

<div class="clr"></div>

<?php echo JHTML::_( 'form.token' ); ?>

<input type="hidden" name="option" value="com_vquiz" />

<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />

<input type="hidden" name="task" value="" />

<input type="hidden" name="view" value="quizquestion" />

</form>
</div>