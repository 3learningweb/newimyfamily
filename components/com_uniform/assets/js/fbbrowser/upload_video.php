<?php //這個範例讓你直接使用 fb graph api 上傳一段影片， 證明有效的！   可以刪除了！ 已經寫在 form.php了～

require __DIR__ . '/php-graph-sdk/vendor/autoload.php';

use Facebook\Facebook;
use Facebook\FacebookClient;
use Facebook\FacebookRequest;
use Facebook\Authentication\AccessToken;
use Facebook\GraphNodes\GraphEdge;


define("UPLOAD_FOLDER", $_SERVER["DOCUMENT_ROOT"]."/images/jsnuniform/jsnuniform_uploads/1/");


$app_id      = "172570969890294";
$app_secret  = "bcf1992c5435ca0f2d58853e46d7cd23";

/* fb會返回自己網站的網址 */
$my_url      = "http://imyfamily.lion.huhu.tw/components/com_uniform/assets/js/fbbrowser/upload_video.php";
//$my_url      = "http://imyfamily.lion.huhu.tw/index.php?option=com_uniform&view=form&form_id=1&show_form_title=1&show_form_description=1&Itemid=322&task=form.upload_fb_video";


$video_title = "YOUR_VIDEO_TITLE";
$video_desc  = "YOUR_VIDEO_DESCRIPTION";


/*=============================================
=            取得access token           =
=============================================*/

$code = $_REQUEST["code"]; //如果內容為空 代表 還沒登錄！

if (empty($code)) { //代表尚未登錄FB
    $dialog_url = "http://www.facebook.com/dialog/oauth?client_id=". $app_id . "&redirect_uri=" . urlencode($my_url) . "&scope=publish_actions,user_photos"; //導到FB 申請access token
    echo ("<script>top.location.href='" . $dialog_url . "'</script>"); // 這行並不會馬上跳到這個url網址， 而是整個頁面 程式都跑完之後 就是整個頁面都render完畢之後才執行 js code 進行轉向
}

$token_url = "https://graph.facebook.com/oauth/access_token?client_id=". $app_id . "&redirect_uri=" . urlencode($my_url) . "&client_secret=" . $app_secret . "&code=" . $code; //請求成功的話，$token_url就會是傳回原來的網址 加上 code 的內容


/* //取得此次登錄的access token   */
$access_token = file_get_contents($token_url);   

if($access_token == "") {
    echo "empty token";
    exit();
}

parse_str($access_token, $get_array);  // 此時$get_array['access_token'] 裡頭就是真正的token了！

//echo "token: " . $get_array['access_token'] . "<br>";

/*=============================================
=            新方法 證明有效！           =
=============================================*/

$fb = new Facebook([
  'app_id' => '172570969890294',
  'app_secret' => 'bcf1992c5435ca0f2d58853e46d7cd23',
  'default_graph_version' => 'v2.2',
  ]);

$data = [
  'title' => 'My Foo Video',
  'description' => 'This video is full of foo and bar action.',
  //'source' => $fb->videoToUpload(realpath('587a406bb0729.mp4')),
  'source' => $fb->videoToUpload(UPLOAD_FOLDER. '587d7ceef050c.mp4'),
  
];


try {
  $response = $fb->post('/me/videos', $data, $get_array['access_token']);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}


$graphNode = $response->getGraphNode();
var_dump($graphNode);

echo 'Video ID: ' . $graphNode['id'];