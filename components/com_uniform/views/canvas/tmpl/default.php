<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid	= $app->input->getInt('Itemid');

?>


<style>
	.pull-right {
		padding-top: 15px;
		float:right;
		width:50%;
		position: relative;
	}

	.email_send {
		display: none;
		margin-top: 30px;
	}

	.form_table input {
		width:400px;
		
	}

	.pull-left {

		float:left;
		width:50%;
	}

	.finish {
		width:100%;
		border: 5px solid #C1DF86;
	}

            .submit_block {
                background-color: #EDB14B;
                filter: progid: DXImageTransform.Microsoft.gradient(GradientType=0, startColorstr=#51BA68, endColorstr=#25933D);
                background: linear-gradient(to bottom, #51BA68 0%, #25933D 100%);
                right: 0;
                position: absolute;
            }

	.submit_block input[type="button"] {
                font-size: 18px;
                font-weight: bold;
                background-image: url('templates/system/images/submit_icon.png');
                background-color: #EDB14B;

                background-repeat: no-repeat;
                background-position: right;
                color: #fff;

                display: block;
                min-width: 185px;
                height: 42px;
                margin: 0px;
                border: 0px;
                line-height: 42px;
                padding-right: 45px;
                text-align: center;
                box-sizing: border-box;
            }

            #card_canvas {

                display: block;
                margin:0 auto;
            }

            div.canvas-container {
                margin:0 auto;
            }

            @media (max-width: 767px) {

                div.canvas-container {
                    width: 100% !important;
                    height: 400px !important;
                }

                #card_canvas.lower-canvas {
                    width: 100% !important;
                    height: auto !important;
                }

                .upper-canvas {
                    width: 100% !important;
                    height: auto !important;
                }

                .pull-right {
                    display: block !important;
                }

                h2 {padding-left: initial !important;}
             }

            @media (max-width: 380px) {

                div.canvas-container {
                    height: 390px !important;
                }
            }


             
</style>


<h2 style="font-size: 26px; padding:10px; text-align:center">卡片編輯區</h2>
<h2 style="font-size: 18px; padding:10px; text-align:center;padding-left:160px;">上傳一張家庭聚會照片：<input type="file" id="selectFile" name="selectFile" /></h2>
<div class="com_postcards">

	<form id="submitForm">

		<div class="preview" style="text-align:center;">

                                    <canvas id="card_canvas" width="640" height="840" ></canvas>
                                    <span class="highlight">*</span><span style="font-weight: bold;">您可拖曳或縮放照片和文字，調整至您喜歡的風格。</span><br>
                                    <span class="highlight">*</span><span style="font-weight: bold;">您可修改：<button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'}" style="background-image: none; background-color: rgb(255, 232, 67); color: rgb(0, 0, 0);"> 文字顏色 </button>，文字字型：<select name="FontStyleNumber" id="FontStyleNumber">
                                      <option value="Times New Roman">Times New Roman</option>
                                      <option value="Arial">Arial</option>
                                      <option value="Georgia">Georgia</option>
                                      <option value="細明體">細明體</option>
                                      <option value="新細明體">新細明體</option>
                                      <option value="標楷體">標楷體</option>
                                      <option value="微軟正黑體">微軟正黑體</option>
                                      <option value="微軟雅黑體">微軟雅黑體</option>
                                    </select></span><br><br>
			<div class="pull-right" style= "display: inline-flex; width:90%;" >
					<!--input type="button" id="email_btn" value="上傳照片"-->
                                                            
				<div class="submit_block" style="float : left; right: 50px;position: absolute;">
					<input type="button" id="download" value="下載圖片">
				</div>
			</div>
			<br><br><br><br><br><br><br><br>
			<div style="clear:both;"></div>


			
		</div>
	</form>


</div>


<script>



                var myAppModule = (function () {  //這是個function 一開始就會被載入執行
                    var outObj = {};

                    var file, fileReader, img;
                    var cImg;

                    var init = function (newFile, newFileReader) {
                        file = newFile;
                        fileReader = newFileReader;
                    };

                    var onloadImage = function () { // 讀進去圖片的時候 執行以下程式碼
                        cImg = new fabric.Image(img, {
                            left: 110,
                            top: 340,
                            angle: 0
                        });

                        cImg.scaleToWidth(440);

                        canvas.add(cImg);
                        canvas.moveTo(cImg, 1);
                    };

                    var onloadFile = function (e) {  //讀完檔案的時候 執行以下程式碼
                        img = new Image();
                        img.onload = onloadImage;
                        img.src = fileReader.result;
                    };

                    outObj.init = init;
                    outObj.OnloadFile = onloadFile;

                    return outObj;
                })();

                function handleFileSelect(evt) {
                    var files = evt.target.files;
                    var output = [];
                    for (var i = 0, f; f = files[i]; i++) {

                        if (!f.type.match('image.*')) {
                            continue;
                        }

                        var reader = new FileReader();

                        myAppModule.init(f, reader);  //將檔案名稱 以及reader 塞進去 init ， 

                        reader.onload = myAppModule.OnloadFile;

                        reader.readAsDataURL(f);

                    }
                }




                var canvas = new fabric.Canvas('card_canvas');
                var selectedObj;

                canvas.on('mouse:down', function(options) {
                  if (options.target) {
                    selectedObj = options.target;
                    console.log('an object was clicked! ', options.target.type);
                  }
                });



                /* 載入底圖 */
                fabric.Image.fromURL("<?php echo JURI::root() . "components/com_uniform/assets/images/background.jpg"; ?>", function(img) {
                    img.selectable = false;
                    img.scaleToWidth(640);
                    canvas.add(img);
                    canvas.moveTo(img, 0);
                });

                /* 加入文字 */
                var text_title = new fabric.Text('<?php echo $this->title; ?>', {   top: 140, fill: '#333', fontSize: 26, fontFamily: '標楷體' });
                // center it;
                text_title.setLeft(    (canvas.width - text_title.width) / 2 );
                // canvas.add(text_title);
                // canvas.moveTo(text_title, 2);

                var text_title1 = new fabric.Text('<?php echo $this->title1; ?>', {   top: 180, fill: '#333', fontSize: 26, fontFamily: '標楷體' });
                // center it;
                text_title1.setLeft(    (canvas.width - text_title1.width) / 2 );

                var group = new fabric.Group([ text_title, text_title1 ]);
                canvas.add(group);


                // canvas.add(text_title1);
                // canvas.moveTo(text_title1, 2);

                // var text_title1 = new fabric.Text('<?php echo $this->title1; ?>', {   top: 180, fill: '#333', fontSize: 26, fontFamily: '標楷體' });
                // // center it;
                // text_title1.setLeft(    (canvas.width - text_title1.width) / 2 );
                // canvas.add(text_title1);
                // canvas.moveTo(text_title1, 2);

                var text_title2 = new fabric.Text('<?php echo $this->title2; ?>', {   top: 220, fill: '#333', fontSize: 26, fontFamily: '標楷體' });
                text_title2.setLeft(    (canvas.width - text_title2.width) / 2 );
                canvas.add(text_title2);
                canvas.moveTo(text_title2, 2);




                var text_success = new fabric.Text('家庭聚會，大成功！', {  left: 180, top: 260, fill: '#f55', fontSize: 36, fontFamily: '標楷體' });
                canvas.add(text_success);
                canvas.moveTo(text_success, 2);

                

                /* 加入日期 */
                var text_success = new fabric.Text((new Date()).toISOString().slice(0,10), {  left: 500, top: 640, fill: '#333', fontSize: 18, fontFamily: '標楷體' });
                canvas.add(text_success);
                canvas.moveTo(text_success, 2);

                function setTextColor(picker) {  //改變文字顏色

                    selectedObj.setColor('#'+picker.toString());
                    canvas.renderAll();
                }

                var fontControl = jQuery('#FontStyleNumber');
                jQuery(document.body).on('change', '#FontStyleNumber', function () {
                    selectedObj.fontFamily = fontControl.val();
                    canvas.renderAll();
                });

                jQuery("#download").click(function() {
                    canvas.deactivateAll().renderAll();
                    jQuery("canvas").get(0).toBlob(function(blob){  //可以取得blob格式的圖檔
                        saveAs(blob, "myCard.jpg");
                    });
                });

	(function($) {
                $(document).ready(function () {
                    document.getElementById('selectFile').addEventListener('change', handleFileSelect, false);  //處理上傳檔案時的後續動作

                });
	})(jQuery);

</script>