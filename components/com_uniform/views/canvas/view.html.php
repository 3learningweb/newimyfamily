<?php

defined('_JEXEC') or die('Restricted access');


// import Joomla view library
jimport('joomla.application.component.view');

class JSNUniformViewCanvas extends JSNBaseView
{

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a JError object.
	 *
	 * @see     fetch()
	 * @since   11.1
	 */
	function display($tpl = null)
	{

		// Assign data to the view
		$this->_input = JFactory::getApplication()->input;
		$formId = $this->_input->get('form_id');
		$this->_state = $this->get('State');
		$this->_formId = $formId?$formId:$this->_state->get('form.id');

		$this->_formName = md5(date("Y-m-d H:i:s") . $this->_formId);

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		$document = JFactory::getDocument();
		$document->addScript(JURI::root(true) . '/media/jui/js/jquery.min.js');
		$document->addScript(JURI::root(true) . '/components/com_uniform/assets/fabric.touch.js');
		$document->addScript(JURI::root(true) . '/components/com_uniform/assets/jscolor.min.js');
		$document->addScript(JURI::root(true) . '/components/com_uniform/assets/FileSaver.min.js');
		$document->addScript(JURI::root(true) . '/components/com_uniform/assets/canvas-to-blob.min.js');

		$this->title = JFactory::getApplication()->getUserState("com_uniform.title", '');
		$this->title1 = JFactory::getApplication()->getUserState("com_uniform.title1", '');
		$this->title2 = JFactory::getApplication()->getUserState("com_uniform.title2", '');


		// Display the view
		parent::display($tpl);
	}

}
