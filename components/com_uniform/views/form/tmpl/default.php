<?php

/**
 * @version     $Id: default.php 19013 2012-11-28 04:48:47Z thailv $
 * @package     JSNUniform
 * @subpackage  Form
 * @author      JoomlaShine Team <support@joomlashine.com>
 * @copyright   Copyright (C) 2015 JoomlaShine.com. All Rights Reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.joomlashine.com
 * Technical Support:  Feedback - http://www.joomlashine.com/contact-us/get-support.html
 */
defined('_JEXEC') or die('Restricted access');
$showTitle = false;
$showDes = false;
$app = JFactory::getApplication();
$params = $app->getParams();
$getShowTitle = $this->_input->get('show_form_title');
$getShowDes = $this->_input->get('show_form_description');

$document = JFactory::getDocument();


$document->addCustomTag( '<meta property="fb:app_id" content="201537120320406" />');
if($this->_formId == 1) {//家庭成長紀錄
	$document->addCustomTag( '<meta property="og:url" content="https://imyfamily.moe.edu.tw/家庭紀錄簿/家庭成長紀錄" />' );
	$document->addCustomTag( '<meta property="og:type" content="website" />' );
	$document->addCustomTag( '<meta property="og:title" content="家庭記錄簿" />' );
	$document->addCustomTag( '<meta property="og:description" content="每位家人的人生重要時刻，都值得好好記錄。舉凡寶寶週歲、爸媽生日、兒子當兵、女兒畢業、爺爺奶奶的結婚紀念日…等，在重要的日子，記得拍照記錄，延續當下的美好畫面，挑選幾張照片，製成影片，與親朋好友分享家庭生活中的喜悅。" />' );
	$document->addCustomTag( '<meta property="og:image" content="'.JURI::root().'images/fb/card.jpg" />' );
}

if($this->_formId == 2) {//家庭聚會卡
	$document->addCustomTag( '<meta property="og:url" content="https://imyfamily.moe.edu.tw/家庭活動/家庭聚會卡-new" />' );
	$document->addCustomTag( '<meta property="og:type" content="website" />' );
	$document->addCustomTag( '<meta property="og:title" content="家庭聚會卡" />' );
	$document->addCustomTag( '<meta property="og:description" content="家族聚會的時候，除了聊天，還有很多有趣的事可以一起做，透過家庭聚會卡，炒熱聚會氣氛吧!" />' );
	$document->addCustomTag( '<meta property="og:image" content="'.JURI::root().'templates/system/images/familyfb.jpg" />' );
}

if (!empty($getShowTitle) && $getShowTitle == 1)
{
	$showTitle = true;
}
if (!empty($getShowDes) && $getShowDes == 1)
{
	$showDes = true;
}
if (JSNUniformHelper::checkStateForm($this->_formId))
{
	echo JSNUniformHelper::generateHTMLPages($this->_formId, $this->_formName,'','','',$showTitle,$showDes);
}