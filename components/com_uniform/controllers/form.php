<?php

/**
 * @version     $Id: form.php 19014 2012-11-28 04:48:56Z thailv $
 * @package     JSNUniform
 * @subpackage  Controller
 * @author      JoomlaShine Team <support@joomlashine.com>
 * @copyright   Copyright (C) 2015 JoomlaShine.com. All Rights Reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.joomlashine.com
 * Technical Support:  Feedback - http://www.joomlashine.com/contact-us/get-support.html
 */

use Facebook\Facebook;

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Form controllers of JControllerForm
 *
 * @package     Controllers
 * @subpackage  Form
 * @since       1.6
 */
class JSNUniformControllerForm extends JSNBaseController
{

    protected $option = JSN_UNIFORM;

    /**
     * Method to get a model object, loading it if required.
     *
     * @param   string  $name    The model name. Optional.
     * @param   string  $prefix  The class prefix. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return  object  The model.
     *
     * @since   1.6
     */
    public function getModel($name = '', $prefix = '', $config = array('ignore_request' => true))
    {
        return parent::getModel($name, $prefix, array('ignore_request' => false));
    }

    /**
     * Save data submission
     *
     * @return Html messages
     */
    public function save() // 這段code 在task=form.save 的時候會被執行， 其輸出都會導到iframe裡頭

    {
        JSession::checkToken() or die('Invalid Token');
        // Check for request forgeries.
        if (@$_SERVER['CONTENT_LENGTH'] < (int) (ini_get('post_max_size')) * 1024 * 1024) {
            $outputVideo = "";
            //   JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
            $return   = new stdClass;
            $input    = JFactory::getApplication()->input;
            $postData = $input->getArray($_POST);
            $model    = $this->getModel('form');
            if (!empty($postData['form_id']) && JSNUniformHelper::checkStateForm($postData['form_id'])) {
                $return = $model->save($postData);
            }
            if (isset($return->error)) {
                //echo json_encode(array('error' => $return->error));
                echo '<input type="hidden" name="error" value=\'' . htmlentities(json_encode($return->error), ENT_QUOTES, "UTF-8") . '\'/>';
                exit();
            } else {

                // 這邊開始處理影片好了！！

                $ffmpegInputFile      = [];
                $uniqidForThisSession = '';

                if ($postData['form_id'] == "1") {
                    //代表第一個表單，可讓人上傳fb相簿功能 開始訂製處理
                    //將抓到的所有檔案在這邊做處理：
                    //呼叫外部程式
                    //echo exec('whoami');

                    //收集所有照片的檔名、 取得音樂名稱、 取得字幕
                    //1. 先將照片正規化

                    //取得所有照片檔名
                    //$_FILES[5]

                    define("UPLOAD_FOLDER", getcwd() . "/images/jsnuniform/jsnuniform_uploads/1/");
                    define("MAX_PHOTO_NUM", "10");

                    //取得本機上傳相片檔名
                    foreach ($model->uploadRealFilename as $index => $value) {
                        //echo $value["link"];
                        //$output = UPLOAD_FOLDER . $value["link"]; file_put_contents("debug123.txt", "\n\$output = $output\n", FILE_APPEND | LOCK_EX); 
                        //die("test message");
                        $this->correctImageOrientation(UPLOAD_FOLDER . $value["link"]);
                        $ffmpegInputFile[] = $value["link"];
                    }

                    //取得fb相片檔名
                    foreach ($postData as $key => $value) {
                        if (strpos($key, 'serverPhotoFilename_') !== false) {
                            $ffmpegInputFile[] = $value;
                        }
                    }
                    //$postData['serverPhotoFilename_']
                    

                    $uniqidForThisSession = uniqid();

                    //將每張相片裁剪成固定的size  檔名格式， 統一就是 uniqid + _ 原來的檔名
                    foreach ($ffmpegInputFile as $key => $value) {
                        imagejpeg($this->crop_img(UPLOAD_FOLDER . $value, 1280, 720), UPLOAD_FOLDER . $uniqidForThisSession . "_" . $value);
                        $ffmpegInputFile[$key] = UPLOAD_FOLDER . $uniqidForThisSession . "_" . $value;
                    }

                    /*----------  此時$ffmpegInputFile 儲存的是已經更改尺寸後的絕對路徑檔案名稱  ----------*/

                    /*=============================================
                    =            準備頁首頁尾黑色空白圖片           =
                    =============================================*/
                    $head_filename = UPLOAD_FOLDER . $uniqidForThisSession . "_000.jpg";
                    $foot_filename = UPLOAD_FOLDER . $uniqidForThisSession . "_999.jpg";

                    exec('printf "1\n00:00:03,579 --> 00:00:06,652\n' . $postData[11] . '\n\n2\n00:00:25,560 --> 00:00:28,419\n' . $postData[14] . '" > ' . $subtitle);

                    exec('cp ' . UPLOAD_FOLDER . 'head.jpg ' . $head_filename);
                    exec('cp ' . UPLOAD_FOLDER . 'foot.jpg ' . $foot_filename);
                    /*=====  End of 準備頁首頁尾黑色空白圖片 ======*/

                    /*============================================================
                    =            如果照片不滿10張，就補足10張， 每張播放2.5秒，這樣就會是30秒了            =
                    ============================================================*/

                    if (count($ffmpegInputFile) < MAX_PHOTO_NUM) {
                        for ($i = 0; $i < MAX_PHOTO_NUM - count($ffmpegInputFile); $i++) {
                            exec('cp ' . $ffmpegInputFile[$i % count($ffmpegInputFile)] . ' ' . UPLOAD_FOLDER . $uniqidForThisSession . "_555" . $i . '.jpg');
                        }
                    }

                    /*=====  End of 如果照片不滿10張，就補足10張， 每張播放2.5秒，這樣就會是30秒了  ======*/

                    //指定背景音樂  日後音樂可在此修正
                    if ($postData[17] == "Corncob Country") {
                        $bgSound = getcwd() . "/media/music/Corncob_Country.mp3";
                    } else if ($postData[17] == "Sandbox") {
                        $bgSound = getcwd() . "/media/music/Sandbox.mp3";
                    } else if ($postData[17] == "On Hold") {
                        $bgSound = getcwd() . "/media/music/On_Hold.mp3";
                    } else if ($postData[17] == "Austrian Army") {
                        $bgSound = getcwd() . "/media/music/Austrian_Army.mp3";
                    } else if ($postData[17] == "Hot Rock") {
                        $bgSound = getcwd() . "/media/music/Hot_Rock.mp3";
                    } else if ($postData[17] == "Para Santo Domingo") {
                        $bgSound = getcwd() . "/media/music/Para_Santo_Domingo.mp3";
                    } else if ($postData[17] == "Pluto") {
                        $bgSound = getcwd() . "/media/music/Pluto.mp3";
                    } else if ($postData[17] == "Sunflower") {
                        $bgSound = getcwd() . "/media/music/Sunflower.mp3";
                    } else if ($postData[17] == "Wedding Invitation") {
                        $bgSound = getcwd() . "/media/music/Wedding_Invitation.mp3";
                    } else if ($postData[17] == "Namaste") {
                        $bgSound = getcwd() . "/media/music/Namaste.mp3";
                    } else if ($postData[17] == "Jack in the Box") {
                        $bgSound = getcwd() . "/media/music/Jack_in_the_Box.mp3";
                    } else if ($postData[17] == "The Mighty Kingdom") {
                        $bgSound = getcwd() . "/media/music/The_Mighty_Kingdom.mp3";
                    }

                    //產生字幕檔
                    if ($postData[11] == "Others") {
                        $postData[11] = $postData['fieldOthers'][11];
                    }

                    if ($postData[14] == "Others") {
                        $postData[14] = $postData['fieldOthers'][14];
                    }

                    if ($postData[14] == "日期") {
                        $postData[14] = date('Y 年 m 月 d 日');
                    }

                    $subtitle = UPLOAD_FOLDER . $uniqidForThisSession . ".srt";
                    exec('printf "1\n00:00:00,000 --> 00:00:02,452 X1:70 X2:600 Y1:200 Y2:250\n' . $postData[11] . '\n\n2\n00:00:26,060 --> 00:00:29,419 X1:70 X2:600 Y1:200 Y2:250\n' . $postData[14] . '" > ' . $subtitle);

                    //進行ffmpeg 轉檔

                    exec(UPLOAD_FOLDER . 'ffmpeg -loop 1 -framerate 1/2.5 -pattern_type glob -i \'' . UPLOAD_FOLDER . $uniqidForThisSession . '_*.jpg\' -i ' . $bgSound . ' -vf "subtitles=\'' . $subtitle . '\':force_style=\'FontName=文泉驛正黑,FontSize=37,PrimaryColour=&H1153B3ED\'" -c:v libx264 -r 30 -pix_fmt yuv420p -shortest ' . UPLOAD_FOLDER . $uniqidForThisSession . '.mp4');
                    //exec(UPLOAD_FOLDER. 'ffmpeg -loop 1 -framerate 1/2.5 -pattern_type glob -i \''. UPLOAD_FOLDER.  $uniqidForThisSession    .'_*.jpg\' -i '.  $bgSound   .' -vf "subtitles=\''.  $subtitle    .'" -c:v libx264 -r 30 -pix_fmt yuv420p -shortest '.UPLOAD_FOLDER.$uniqidForThisSession. '.mp4');
                    $outputVideo = JURI::root() . "images/jsnuniform/jsnuniform_uploads/1/" . $uniqidForThisSession . '.mp4';

                    // if(!$this->UR_exists($outputVideo))
                    //$outputVideo = JURI::root(). 'images/jsnuniform/jsnuniform_uploads/1/587d7ceef050c.mp4'; /* for 大朵主機測試用的 因他不能執行ffmpeg :~~~ */

                }

                if (isset($return->actionForm) && $return->actionForm == 'message') {
                    //  echo json_encode(array('message' => $return->actionFormData));

                    if ($postData['form_id'] == "1") { //唯獨 form_id ==  1  才執行以下的code
                        $return->actionFormData .= '<video width="100%" controls>
                                <source src="' . $outputVideo . '" type="video/mp4">
                                預覽影片
                            </video>';
                        $return->actionFormData .= "<button class='download_video' style='margin-right: 40px;margin-top:20px;' onclick=\" alert('請在影片上按滑鼠右鍵，再點擊「下載影片」即可。'); window.open('$outputVideo','popUpWindow','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false; \">下載影片</button>";
                        $uploadVideoUrl = JURI::root() . 'index.php?option=com_uniform&view=form&form_id=1&show_form_title=1&show_form_description=1&Itemid=322&task=form.upload_fb_video&uniqid=' . $uniqidForThisSession;
                        $return->actionFormData .= "<button class='share_to_fb' onclick=\" if(confirm('確定要將您的影片發佈至Facebook？')) {location.href='$uploadVideoUrl';} else {return false;} \"><img src='../images/fb/share_to_fb.png'></button>";
                    }

                    if ($postData['form_id'] == "2") { //唯獨 form_id ==  2  才執行以下的code

                        /*========================================
                        =            這邊加入bootstrap 3X            =
                        ========================================*/
                        $files  = JHtml::_('stylesheet', 'media/jui/css/bootstrap3.min.css', null, false, true);
                        $result_text .= "<link rel=\"stylesheet\" href=\"{$files}\" type=\"text/css\" />";
                        //$result_text .= "<script src=\"". JURI::root() . "media/jui/js/bootstrap3.min.js" . "\" type=\"text/javascript\"></script>";
                        JHtml::_('script', 'media/jui/js/bootstrap3.min.js', false, true);
                        /*=====  End of 這邊加入bootstrap 3X  ======*/
                        
                        //JHTML::_('behavior.modal');
                        
                        $result_text .= "<link rel=\"stylesheet\" href=\"". JURI::root() . "media/system/css/modal.css". "\" type=\"text/css\" />";
                        //$result_text .= "<script src=\"". JURI::root() . "tree/tree_asset/js/jquery.min.js" . "\" type=\"text/javascript\"></script>";
                        //$result_text .= '<script src="'. JURI::root() . 'media/system/js/modal.js' . '" type="text/javascript"></script>';
                        $rootUrl = JURI::root();

$result_text .= <<<DATA
                        <script src="{$rootUrl}media/system/js/mootools-core.js" type="text/javascript"></script>
                          <script src="{$rootUrl}media/system/js/core.js" type="text/javascript"></script>
                          <script src="{$rootUrl}media/system/js/mootools-more.js" type="text/javascript"></script>
                          <script src="{$rootUrl}media/system/js/modal.js" type="text/javascript"></script>

                          <script>
                          
                          jQuery(function($) {
                                    SqueezeBox.initialize({});
                                    SqueezeBox.assign($('a.modal').get(), {
                                        parse: 'rel'
                                    });
                                });

                                window.jModalClose = function () {
                                    SqueezeBox.close();
                                };
                                
                                // Add extra modal close functionality for tinyMCE-based editors
                                document.onreadystatechange = function () {
                                    if (document.readyState == 'interactive' && typeof tinyMCE != 'undefined' && tinyMCE)
                                    {
                                        if (typeof window.jModalClose_no_tinyMCE === 'undefined')
                                        {   
                                            window.jModalClose_no_tinyMCE = typeof(jModalClose) == 'function'  ?  jModalClose  :  false;
                                            
                                            jModalClose = function () {
                                                if (window.jModalClose_no_tinyMCE) window.jModalClose_no_tinyMCE.apply(this, arguments);
                                                tinyMCE.activeEditor.windowManager.close();
                                            };
                                        }
                                
                                        if (typeof window.SqueezeBoxClose_no_tinyMCE === 'undefined')
                                        {
                                            if (typeof(SqueezeBox) == 'undefined')  SqueezeBox = {};
                                            window.SqueezeBoxClose_no_tinyMCE = typeof(SqueezeBox.close) == 'function'  ?  SqueezeBox.close  :  false;
                                
                                            SqueezeBox.close = function () {
                                                if (window.SqueezeBoxClose_no_tinyMCE)  window.SqueezeBoxClose_no_tinyMCE.apply(this, arguments);
                                                tinyMCE.activeEditor.windowManager.close();
                                            };
                                        }
                                    }
                                };

                          </script>




DATA;

                        $return->actionFormData = str_replace("{\$winner}",$postData['winner'], $return->actionFormData);
                        $return->actionFormData = str_replace("{\$score}",$postData['score'], $return->actionFormData);
                        $return->actionFormData = str_replace("{\$prize}",$postData['prize'], $return->actionFormData);

                        //分析postData['game_result'] 字串  產生比賽結果表格 ， 然後取代掉 原本樣板文章裡頭預埋的變數！
                        $game_result = json_decode($postData['game_result']);
                        $teamname = json_decode($postData["teamname"]);
                        $result_text .= '<br><h2 class="text-center">積分板</h2> <hr />';

                        $result_text .= '<div class="container" style="width:100%;"><div class="row text-center" style="display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display:         flex; flex-wrap: wrap;">';
                        
                        $app = JFactory::getApplication();
                        $app->setUserState("com_uniform.title", "今天獲勝的小組是"); 
                        $app->setUserState("com_uniform.title1", $postData['winner']);
                        $app->setUserState("com_uniform.title2", "可以獲得大獎：". $postData['prize']);
                        
                        for($i=0; $i<$postData['team_num']; $i++) {  //各組的分數分析
                            $j = $i+1;
                            $color = ($i % 2) ? "#FFFBF2" : "#F2FDFF";
                            $result_text .= "<div class=\"col-md-3 col-sm-6 col-xs-12\" style=\"border: 1px solid; margin:4px; display: flex; flex-direction: column;padding:10px;background: {$color}\"><h4 style='color:#454FCD;'>第{$j}組({$teamname[$i]})：總分{$this->countTotalScorePerGroup($game_result[$i])}分</h4>";
                            
                            //取出該組各項題型的積分
                            if(count($game_result[$i]) != 0 ) {
                                    foreach ($game_result[$i] as $key => $value) {  // 顯示一個題型積分
                                      $result_text .= '<div class="score-item clearfix"  style="border-bottom: 1px solid;  ">';
                                      $result_text .= "<span style=\"text-align: left; float:left;\">◇ {$value->type}</span>";
                                      $result_text .= "<span style=\"text-align: right; float:right;\">(得分: {$value->score})</span>";
                                      $result_text .= '</div>';
                                    }
                            }
                            $result_text .= "</div>";
                        }

                        $result_text .= "</div></div>";

                        $return->actionFormData = str_replace("{\$scoretable}",$result_text, $return->actionFormData);
                    }

                    echo '<input type="hidden" name="message" value=\'' . htmlentities($return->actionFormData, ENT_QUOTES, "UTF-8") . '\'/>'; //這裡的echo 會放到ifrmae body裡頭
                    exit();
                } elseif (isset($return->actionForm) && $return->actionForm == 'url') {
                    //echo "<div class=\"src-redirect\">{$return->actionFormData}</div>";
                    echo '<input type="hidden" name="redirect" value=\'' . htmlentities($return->actionFormData, ENT_QUOTES, "UTF-8") . '\'/>';
                    exit();
                } else {
                    exit();
                }
            }
        } else {
            $postMaxSize = (int) ini_get('post_max_size');
            if ($postMaxSize > (int) (ini_get('upload_max_filesize'))) {
                $postMaxSize = (int) (ini_get('upload_max_filesize'));
            }
            //echo json_encode(array('error' => array('max-upload' => JText::sprintf('JSN_UNIFORM_POST_MAX_SIZE', $postMaxSize))));
            echo '<input type="hidden" name="error" value=\'' . htmlentities(json_encode(array('max-upload' => JText::sprintf('JSN_UNIFORM_POST_MAX_SIZE', $postMaxSize))), ENT_QUOTES, "UTF-8") . '\'/>';
            exit();
        }
    }

    /**
     *     get html form
     *
     * @return string
     */

    public function countTotalScorePerGroup($scorePerGrp) {
        $totalScore = 0;
        foreach ($scorePerGrp as $key => $value) {

            $totalScore += $value->score;
        }
        return $totalScore;
    }
    public function getHtmlForm()
    {
        $formId = JRequest::getVar('form_id', '');
        if ($formId) {
            $formName = md5(date("Y-m-d H:i:s")) . rand(0, 1000);
            echo JSNUniformHelper::generateHTMLPages($formId, $formName, "ajax");
            exit();
        }
    }

    public function UR_exists($url)
    {
        $headers = get_headers($url);
        return stripos($headers[0], "200 OK") ? true : false;
    }

    public function crop_img($img, $box_w, $box_h)
    {
        //create the image, of the required size

        $img = imagecreatefromjpeg($img);
        // $img = imagecreatefrompng($img);
        $new = imagecreatetruecolor($box_w, $box_h);
        if ($new === false) {
            //creation failed -- probably not enough memory
            return null;
        }

        //Fill the image with a light grey color
        //(this will be visible in the padding around the image,
        //if the aspect ratios of the image and the thumbnail do not match)
        //Replace this with any color you want, or comment it out for black.
        //I used grey for testing =)
        $fill = imagecolorallocate($new, 0, 0, 5);
        imagefill($new, 0, 0, $fill);

        //compute resize ratio
        $hratio = $box_h / imagesy($img);
        $wratio = $box_w / imagesx($img);
        $ratio  = min($hratio, $wratio);

        //if the source is smaller than the thumbnail size,
        //don't resize -- add a margin instead
        //(that is, dont magnify images)
        if ($ratio > 1.0) {
            $ratio = 1.0;
        }

        //compute sizes
        $sy = floor(imagesy($img) * $ratio);
        $sx = floor(imagesx($img) * $ratio);

        //compute margins
        //Using these margins centers the image in the thumbnail.
        //If you always want the image to the top left,
        //set both of these to 0
        $m_y = floor(($box_h - $sy) / 2);
        $m_x = floor(($box_w - $sx) / 2);

        //Copy the image data, and resample
        //
        //If you want a fast and ugly thumbnail,
        //replace imagecopyresampled with imagecopyresized
        if (!imagecopyresampled($new, $img,
            $m_x, $m_y, //dest x, y (margins)
            0, 0, //src x, y (0,0 means top left)
            $sx, $sy, //dest w, h (resample to this size (computed above)
            imagesx($img), imagesy($img)) //src w, h (the full size of the original)
        ) {
            //copy failed
            imagedestroy($new);
            return null;
        }
        //copy successful
        return $new;

    }

    function correctImageOrientation($filename) {
      if (function_exists('exif_read_data')) {
        $exif = exif_read_data($filename);
        if($exif && isset($exif['Orientation'])) {
          $orientation = $exif['Orientation'];
          if($orientation != 1){
            $img = imagecreatefromjpeg($filename);
            $deg = 0;
            switch ($orientation) {
              case 3:
                $deg = 180;
                break;
              case 6:
                $deg = 270;
                break;
              case 8:
                $deg = 90;
                break;
            }
            if ($deg) {
              $img = imagerotate($img, $deg, 0);        
            }
            // then rewrite the rotated image back to the disk as $filename 
            imagejpeg($img, $filename, 95);
          } // if there is some rotation necessary
        } // if have the exif orientation info
      } // if function exists      
    }

    public function upload_fb_video()
    {

        require getcwd() . '/components/com_uniform/assets/js/fbbrowser/php-graph-sdk/vendor/autoload.php'; // 載入facebook SDK

        define("UPLOAD_FOLDER", $_SERVER["DOCUMENT_ROOT"] . "/images/jsnuniform/jsnuniform_uploads/1/");

        $app_id     = "201537120320406";
        $app_secret = "3ea2ef81b6103ae87bb44ff943706864";
        $uniqid     = JFactory::getApplication()->input->getString('uniqid'); //$uniqidForThisSession

        /* 去fb申請code之後 會返回自己網站的網址 */
        $my_url = JURI::root() . "index.php?option=com_uniform&view=form&form_id=1&show_form_title=1&show_form_description=1&Itemid=322&task=form.upload_fb_video&uniqid=" . $uniqid;

        $video_title = "YOUR_VIDEO_TITLE";
        $video_desc  = "YOUR_VIDEO_DESCRIPTION";

        /*=============================================
        =            取得access token           =
        =============================================*/

        $code = $_REQUEST["code"]; //如果內容為空 代表 還沒登錄！

        if (empty($code)) { //代表尚未登錄FB
            $dialog_url = "https://www.facebook.com/dialog/oauth?client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url) . "&scope=publish_actions"; //導到FB 申請access token
            echo ("<script>top.location.href='" . $dialog_url . "'</script>"); // 這行並不會馬上跳到這個url網址， 而是整個頁面 程式都跑完之後 就是整個頁面都render完畢之後才執行 js code 進行轉向
        }

        $token_url = "https://graph.facebook.com/oauth/access_token?client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url) . "&client_secret=" . $app_secret . "&code=" . $code; //請求成功的話，$token_url就會是傳回原來的網址 加上 code 的內容

        /* //取得此次登錄的access token   */


        //$access_token = file_get_contents($token_url);  //這邊若 allow_url_open 關閉的話 就會執行異常！抓不到token

        
        //if (function_exists('curl_version'))
        if (0)
        {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $token_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $access_token = curl_exec($curl);
            curl_close($curl);
        }
        else if (file_get_contents(__FILE__) && ini_get('allow_url_fopen'))
        {
            $access_token = file_get_contents($token_url);
        }
        else
        {
            echo 'You have neither cUrl installed nor allow_url_fopen activated. Please setup one of those!';
        }



        if ($access_token == "") {
            echo "empty token";
            exit();
        }

        parse_str($access_token, $get_array); // 此時$get_array['access_token'] 裡頭就是真正的token了！

        /*===================================
        =            上傳使用者製作的影片        =
        ===================================*/

        if (empty($uniqid)) {
            //echo "分享影片失敗！";
            JFactory::getApplication()->enqueueMessage('<h3 style="font-size:18px;">分享影片失敗！</h3>', 'message');

        } else {

            /*=============================================
            =            新方法 證明有效！           =
            =============================================*/

            $fb = new Facebook([
                'app_id'                => '201537120320406',
                'app_secret'            => '3ea2ef81b6103ae87bb44ff943706864',
                'default_graph_version' => 'v2.8',
            ]);

            $data = [
                'title'       => '家庭成長紀錄｜iMyfamily家庭記錄簿',
                'description' => '挑選幾張照片，製成影片，與親朋好友分享家庭生活中的喜悅。',
                //'source' => $fb->videoToUpload(realpath('587a406bb0729.mp4')),
                'source'      => $fb->videoToUpload(UPLOAD_FOLDER . $uniqid . '.mp4'),
            ];

            try {
                $response = $fb->post('/me/videos', $data, $get_array['access_token']);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            JFactory::getApplication()->enqueueMessage('<h3 style="font-size:18px;">已成功將您的影片分享至Facebook</h3>', 'message');

            // $graphNode = $response->getGraphNode();
            // var_dump($graphNode);

            // echo 'Video ID: ' . $graphNode['id'];
        }
    }

}
