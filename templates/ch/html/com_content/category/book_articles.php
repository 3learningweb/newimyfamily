<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Create some shortcuts.
$params		= &$this->item->params;
$n			= count($this->items);
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));


// 指定網頁文章分享於facebook時的呈現設定
$doc = JFactory::getDocument();
$app = JFactory::getApplication();
$sitename = $app->getTemplate(true)->params->get('sitetitle');

$doc->setMetaData( 'og:title', $title. " |". $sitename);		// 標題
$description = $this->item->introtext;
$doc->setMetaData( 'og:description', JFilterOutput::cleanText($description));	// 描述
$doc->setMetaData('og:image', JURI::root(). 'filesys/images/system/logo.png');	// 圖片




// Check for at least one editable article
$isEditable = false;
if (!empty($this->items))
{
	foreach ($this->items as $article)
	{
		if ($article->params->get('access-edit'))
		{
			$isEditable = true;
			break;
		}
	}
}
$isEditable = false;
?>



<?php if (empty($this->items)) : ?>
	
	<?php if ($this->params->get('show_no_articles', 1)) : ?>
	<p><?php echo JText::_('COM_CONTENT_NO_ARTICLES'); ?></p>
	<?php endif; ?>

<?php else : ?>

<style>
@media screen and (min-width: 720px) {
    .block {
        height:175px;
    }
	.block img{
		max-width:98%; 
		max-height:168px;
	}
	.book_list2{
		width:19%; 
		height:250px; 
		float: left; 
		padding-left:1%;
	}
}
@media screen and (max-width: 720px) {
    .block {
        height:125px;
    }
	.block img{
		max-width:98%; 
		max-height:110px;
	}
	.book_list2{
		width:49%; 
		height:180px; 
		float: left; 
		padding-left:1%;
		padding-top:10px;
	}
}
</style>

<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm" class="form-inline">
	<?php if ($this->params->get('show_headings') || $this->params->get('filter_field') != 'hide' || $this->params->get('show_pagination_limit')) :?>
	
	<fieldset class="filters btn-toolbar clearfix">
		<?php if ($this->params->get('filter_field') != 'hide') :?>
			<div class="btn-group">
				<label class="filter-search-lbl element-invisible" for="filter-search">
					<?php echo JText::_('搜尋：'); ?>
				</label>
				<input type="text" name="filter-search" id="filter-search" value="<?php echo $this->escape($this->state->get('list.filter')); ?>" class="inputbox" onchange="document.adminForm.submit();" title="<?php echo JText::_('COM_CONTENT_FILTER_SEARCH_DESC'); ?>" placeholder="<?php echo JText::_('COM_CONTENT_'.$this->params->get('filter_field').'_FILTER_LABEL'); ?>" />
			</div>
		<?php endif; ?>
		<?php if ($this->params->get('show_pagination_limit')) : ?>
			<div class="btn-group pull-right">
				<label for="limit" class="element-invisible">
					<?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>
				</label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
		<?php endif; ?>

		<input type="hidden" name="filter_order" value="" />
		<input type="hidden" name="filter_order_Dir" value="" />
		<input type="hidden" name="limitstart" value="" />
		<input type="hidden" name="task" value="" />
	</fieldset>
	<?php endif; ?>

	<div style="width:100%">
		<?php foreach ($this->items as $i => $article) : ?>
			
				<div class="book_list2">
					<?php if (in_array($article->access, $this->user->getAuthorisedViewLevels())) : ?>
						<div class="block"><a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catid)); ?>">
						<?php 
					  $imagex = explode("&lt;br /&gt;",$this->escape($article->introtext));	
					  $image = explode("&lt;p&gt;",$imagex[0]);		
					  $img_width = strpos($image[1], 'width');
					  $img_style = strpos($image[1], 'style');
					  if($img_style !=0)
					  {
						$img_src = strpos($image[1], 'src');
						$img_real = '<img  '.substr($image[1],$img_src,($img_width-$img_src));
					  }
					  else if($img_width !=0)
					  {
						$img_real = substr($image[1],0,$img_width).'  />';
					  } 
					  else
					  {
						$img_real = "";
					  }
					  
						echo htmlspecialchars_decode($img_real);	
						//echo $img_real;
						?>
						</a><br></div>
						<?php echo $this->escape($article->title);?>
					<?php else: ?>
						<?php
						echo $this->escape($article->title).' : ';
						$menu		= JFactory::getApplication()->getMenu();
						$active		= $menu->getActive();
						$itemId		= $active->id;
						$link = JRoute::_('index.php?option=com_users&view=login&Itemid='.$itemId);
						$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($article->slug));
						$fullURL = new JUri($link);
						$fullURL->setVar('return', base64_encode($returnURL));
						?>
						<a href="<?php echo $fullURL; ?>" class="register">
							<?php echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE'); ?>
						</a>
					<?php endif; ?>
					<?php if ($article->state == 0) : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JUNPUBLISHED'); ?>
						</span>
					<?php endif; ?>
					<?php if (strtotime($article->publish_up) > strtotime(JFactory::getDate())) : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JNOTPUBLISHEDYET'); ?>
						</span>
					<?php endif; ?>
					<?php if ((strtotime($article->publish_down) < strtotime(JFactory::getDate())) && $article->publish_down != '0000-00-00 00:00:00') : ?>
						<span class="list-published label label-warning">
							<?php echo JText::_('JEXPIRED'); ?>
						</span>
					<?php endif; ?>
				</div>
				<?php if ($this->params->get('list_show_date')) : ?>
					<td headers="categorylist_header_date" class="list-date small">
						<?php
						echo JHtml::_(
							'date', $article->displayDate,
							$this->escape($this->params->get('date_format', JText::_('DATE_FORMAT_LC4')))
						); ?>
					</td>
				<?php endif; ?>
				<?php if ($this->params->get('list_show_author', 1)) : ?>
					<td headers="categorylist_header_author" class="list-author">
						<?php if (!empty($article->author) || !empty($article->created_by_alias)) : ?>
							<?php $author = $article->author ?>
							<?php $author = ($article->created_by_alias ? $article->created_by_alias : $author);?>
							<?php if (!empty($article->contact_link) && $this->params->get('link_author') == true) : ?>
								<?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', JHtml::_('link', $article->contact_link, $author)); ?>
							<?php else: ?>
								<?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', $author); ?>
							<?php endif; ?>
						<?php endif; ?>
					</td>
				<?php endif; ?>
				<?php if ($this->params->get('list_show_hits', 1)) : ?>
					<td headers="categorylist_header_hits" class="list-hits">
						<span class="badge badge-info">
							<?php echo $article->hits; ?>
						</span>
					</td>
				<?php endif; ?>
				<?php if ($isEditable) : ?>
					<td headers="categorylist_header_edit" class="list-edit">
						<?php if ($article->params->get('access-edit')) : ?>
							<?php echo JHtml::_('icon.edit', $article, $params); ?>
						<?php endif; ?>
					</td>
				<?php endif; ?>
			</tr>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<?php // Code to add a link to submit an article. ?>
<?php if ($this->category->getParams()->get('access-create')) : ?>
	<?php echo JHtml::_('icon.create', $this->category, $this->category->params); ?>
<?php  endif; ?>

<?php // Add pagination links ?>
<?php if (!empty($this->items)) : ?>
	<?php if (($this->params->def('show_pagination', 2) == 1  || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
	<div class="pagination" style="width: 100%;float: right;">

		

		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
	<?php endif; ?>
</form>
<?php  endif; ?>

