<?php

// no direct access
defined('_JEXEC') or die;

$Itemid_home = explode(',',$params->get('itemid_home'));
$Itemid1 = explode(',',$params->get('itemid_1'));
$Itemid2 = explode(',',$params->get('itemid_2'));
$Itemid3 = explode(',',$params->get('itemid_3'));
$Itemid4 = explode(',',$params->get('itemid_4'));
$Itemid5 = explode(',',$params->get('itemid_5'));
$Itemid6 = explode(',',$params->get('itemid_6'));
$Itemid7 = explode(',',$params->get('itemid_7'));
$Itemid8 = explode(',',$params->get('itemid_8'));

$mid = JRequest::getCmd('Itemid');
$menuid = $menu_item->id;

//array(0編號, 1搜尋列淺色, 2搜尋列深色, 3表格標題上方邊框, 4表格標題深色, 5表格標題淺色, 6sectiontitle前置色, 7文章上方橫條色, 8訊息刊登深底色, 9訊息刊登淺底色, 10訊息刊登分隔線色, 11預設超連結顏色)

if(in_array($menuid,$Itemid_home)):
$ms = array(shome, 'f0f0f0', 'd8d8d8', '999999', '656565', '707070','666666', 'aaaaaa','','','');

elseif(in_array($menuid, $Itemid1)):
$ms = array(s1, 'ca8c8c', 'b57070', 'ad5c5c', '872d2d', '953232', '9e2828', 'aa5858','6b1b1b','973232','852929','c43b3b');

elseif(in_array($menuid, $Itemid2)):
$ms = array(s2, 'ca8cc8', 'b570b1', 'ad5ca9', '872d82', '953290', '9e2896', 'aa57a4','','','','c43bbb');

elseif(in_array($menuid, $Itemid3)):
$ms = array(s3, '8c8cca', '7170b4', '5c5cad', '2d2d87', '323295', '28289e', '5757aa','','','','4545c4');

elseif(in_array($menuid, $Itemid4)):
$ms = array(s4, '91b1ce', '7799b9', '5c84ad', '2d5987', '326395', '28639e', '5781aa','','','','2d70b2');

elseif(in_array($menuid, $Itemid5)):
$ms = array(s5, '8ccabe', '71b4a7', '5cad9b', '2d8773', '32957f', '289e84', '57aa98','','','','17997d');

elseif(in_array($menuid, $Itemid6)):
$ms = array(s6, '92ca8c', '77b470', '61ad5c', '33872d', '399532', '2f9e28', '5daa57','','','','32912c');

elseif(in_array($menuid, $Itemid7)):
$ms = array(s7, 'cbcb89', 'b6b56c', 'adad5b', '87872c', '959531', '9e9e28', 'aaaa57','666617','999932','848427','7f8020');

elseif(in_array($menuid, $Itemid8)):
$ms = array(s8, 'cba28a', 'b6876d', 'ad775c', '874b2d', '955332', '9e4f28', 'aa7357','6b361b','995532','864829','ab5022');

else :
$ms = array(s0, 'a6a6a6', '8b8b8b', '999999', '656565', '707070','666666', 'aaaaaa','','','');
endif;

?>
<style>
.head_search {
	background-color:#<?php echo $ms[1]; ?>;
	filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#<?php echo $ms[1]; ?>, endColorstr=#<?php echo $ms[2]; ?>);
	background:linear-gradient(to bottom, #<?php echo $ms[1]; ?> 0%, #<?php echo $ms[2]; ?> 30%,#<?php echo $ms[1]; ?> 100%);
}

.section_title {
	border-left: 5px solid #<?php echo $ms[6]; ?>;
}

.page-header {
	border-top: 5px solid #<?php echo $ms[7]; ?>;
}

.blog .item .intro .page-header,
.blog .item .intro .page-header a {
	color: #<?php echo $ms[7] ?>;
}

table.datatable {
	border-top: 5px solid #<?php echo $ms[3]; ?>;
}

/*table.datatable thead th:nth-child(odd),*/
table.datatable thead th.odd {
	background: #<?php echo $ms[4]; ?>;
}

/*table.datatable thead th:nth-child(even),*/
table.datatable thead th.even {
	background: #<?php echo $ms[5]; ?>;
}

.post_inner {
	background: #<?php echo $ms[9]; ?>;
}

.post .icon {
	background-color: #<?php echo $ms[8]; ?>;
}

.post .icon {
	border-right: 1px solid #<?php echo $ms[10]; ?>;
}

.content a {
	color: #<?php echo $ms[11]; ?>;
}
</style>
