<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;


// Get template params
$sitetitle = $app->getTemplate(true)->params->get('sitetitle');


// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');


//rename title (if home page)
$doc->setTitle($doc->getTitle() . " | " . $sitetitle);


// Remove meta generator tag
$this->setGenerator($sitetitle);

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' . $this->template . '/js/template.js');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

$filter = JFilterInput::getInstance();
$uri = JURI::getInstance();
$uri = $filter->clean($uri, 'string');
$uri = htmlspecialchars($uri);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<jdoc:include type="head" />
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<?php
		// Add CSS & JS
		$doc->addStyleSheet('templates/system/css/reset.css');
		$doc->addStyleSheet('templates/' . $this->template . '/css/style.css');
		?>
		<!--[if lt IE 9]>
			<script src="templates/system/js/html5shiv.js"></script>
			<script src="/media/jui/js/html5.js"></script>
		<![endif]-->
		<script type="text/javascript" src="media/jui/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="media/jui/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<link rel="stylesheet" type="text/css" href="media/jui/js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	</head>
	<body class="<?php
		echo $option
		. ' view-' . $view
		. ($layout ? ' layout-' . $layout : ' no-layout')
		. ($task ? ' task-' . $task : ' no-task')
		. ($itemid ? ' itemid-' . $itemid : '');
		?>">
		<!-- Body -->
		
		<div class="all">
			<div class="head">
				<img src="templates/timeline/images/head.png" alt="頁首" />
			</div>
			<jdoc:include type="message" />
			<jdoc:include type="component" />
			<div class="footer">
				<img src="templates/timeline/images/footer.png" alt="頁尾" />
			</div>
		</div>
	</body>
</html>
