<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Mail controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.6
 */
class ApplicationsControllerMail extends JControllerForm {

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array()) {
		$user = JFactory::getUser();
		$categoryId = JArrayHelper::getValue($data, 'catid', $this->input->getInt('filter_category_id'), 'int');
		$allow = null;

		if ($categoryId) {
			// If the category has been passed in the URL check it.
			$allow = $user->authorise('core.create', $this->option . '.category.' . $categoryId);
		}

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		}else{
			return $allow;
		}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;

		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		}else{
			// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}

	/**
	 * Method to run batch operations.
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean   True if successful, false otherwise and internal error is set.
	 *
	 * @since   1.7
	 */
	public function batch($model = null) {

		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('Mail', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_applications&view=mails' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		$task = $this->getTask();

		if ($task == 'save')
		{
			$this->setRedirect(JRoute::_('index.php?option=com_applications&view=mails', false));
		}
	}
	
	function cancel() {
		$this->setRedirect(JRoute::_('index.php?option=com_applications&view=mails', false));
	}
	
	function save($key = null, $urlVar = null) {
		$task	= $this->getTask();
		$db		= JFactory::getDbo();
		$post	= JRequest::get('post');

		$appid	= $post['appid'];
		$mailid = $post['mailid'];
		$state	= $post['state'];

		$query = $db->getQuery(true);
		$query->update('#__applications');
		$query->set('state = "' . $state . '"');
		$query->where('id = "' . $appid . '"');

		$db->setQuery($query);

		if(!$db->execute()) {
			$msg = "資料庫存取失敗。";
		}else{
			$msg = JText::_('COM_APPLICATIONS_N_ITEMS_ORDERING');
		}
		
		// send statemail
		require_once JPATH_COMPONENT.'/controller.php';
		ApplicationsController::sendstate($appid, $state);
		
		switch($task) {
			case 'save':
				$this->setRedirect("index.php?option=" . $this->option . "&view=" . $this->view_list . $this->getRedirectToListAppend(), $msg);
				break;
		}
	}

	function sendmail() {
		$db		= JFactory::getDbo();
		$post	= JRequest::get('post');
		$user	= JFactory::getUser();
		$uid	= $user->get('id');
				
		$appid	 = $post['appid'];
		$mailid  = $post['mailid'];
		$subject = JFilterOutput::cleanText($post['mail_subject']);
		$content = JFilterOutput::cleanText($post['mail_content']);

		$config = JFactory::getConfig();
		$adminName = $config->get('fromname');  // 系統管理者名字
		$adminEmail = $config->get('mailfrom');  // 系統管理者mail
		$sitename = $config->get('sitename');  // 系統名稱		
		
		$query = $db->getQuery(true);
		$query->select("m.*, a.*");
		$query->from("#__applications_mail as m");
		$query->leftJoin("#__applications as a ON a.application_id = m.id");
		$query->where("a.id = $appid");
		$db->setQuery($query);
		$keys = array('name', 'email', 'tel', 'address');
        $result = JUtility::batchDecode($db->loadObject(), $keys);		

		$content = "親愛的 {$result->name}您好，您於{$result->created}申請，案號為{$result->doc_num}的信件之回覆如下：<br /><br />".$content."<br /><br />祝身體健康、萬事如意<br />". $sitename. " 敬上";
		$sendEmailStatus = JUtility::sendMail($adminEmail, $adminName, $result->email, $subject, $content, 1);
		$sendEmailStatus = JUtility::sendMail($adminEmail, $adminName, $adminEmail, $subject, $content, 1);		//傳副本
		$query = $db->getQuery(true);
		
		// update #__applications replay_date、admin_id
		$query_update = $db->getQuery(true);
		$query_update->update('#__applications');
		$query_update->set('reply_date = now()');
		$query_update->set('admin_id = "' . $uid . '"');
		$query_update->where('id = "' . $appid . '"');
		
		
		if( $sendEmailStatus ){
			$msg = '寄送成功!';
			$db->setQuery($query_update);
			$db->execute();
		}else{
			$msg = '寄送失敗!';
		}
		$link = "index.php?option=" . $this->option . "&task=" . $this->view_item . ".edit&id=" . $mailid;
		$this->setRedirect($link, $msg);
		
	}

}
