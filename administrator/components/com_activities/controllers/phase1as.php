<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * phase1as list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesControllerPhase1as extends JControllerAdmin {

	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Guide', $prefix = 'ActivitiesModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}

	function getList() {
		$this->setRedirect("index.php?option=com_activities&view=phase1as");
	}
	public function recommend() 
	{
		$db = JFactory::getDbo();
        /*
		// reset all recommend state
		$query = $db->getQuery(true);
        $query->update('#__youvideo');
        $query->set('recommend = 0');
        $query->where('recommend = 1');
		$db->setQuery($query);
		$db->query();		
*/		
		$query = $db->getQuery(true);
		$query ='select count(*) as cnt from #__contribute_list2015 where state=1';
		
		// From the table
//		$query->from("#__youvideo");
//		$query->where('recommend=1');
//		$query->order("ordering {$direction}");

        $db->setQuery($query);
    	$cnt = $db->loadResult();
        if($cnt<15){
        // set recommend item
		$cid	= JRequest::getVar('cid', array(), '', 'array');
		$pk = $cid[0];
		
        $query = $db->getQuery(true);
		$query->update('#__contribute_list2015');
		$query->set('state = 1');		
		$query->where('id = ' .(int)$pk);		
		
		$db->setQuery($query);
		
		$msg = ($db->query())?'設定成功':'設定失敗';
        }
        else{
            $msg='入選件數不能超過15件';
        }
		$this->setRedirect('index.php?option=com_activities&views=phase1as', $msg);
					
	}
	public function unrecommend() 
	{
		$db = JFactory::getDbo();
        /*
		// reset all recommend state
		$query = $db->getQuery(true);
        $query->update('#__youvideo');
        $query->set('recommend = 0');
        $query->where('recommend = 1');
		$db->setQuery($query);
		$db->query();		
*/		
        // set recommend item
		$cid	= JRequest::getVar('cid', array(), '', 'array');
		$pk = $cid[0];
		
        $query = $db->getQuery(true);
		$query->update('#__contribute_list2015');
		$query->set('state = 0');		
		$query->where('id = ' .(int)$pk);		
		
		$db->setQuery($query);
		
		$msg = ($db->query())?'設定成功':'設定失敗';
		$this->setRedirect('index.php?option=com_activities&views=phase1as', $msg);
					
	}
	
}
