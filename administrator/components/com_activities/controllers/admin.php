<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Admin controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.6
 */
class ApplicationsControllerAdmin extends JControllerForm {

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array()) {
		$user = JFactory::getUser();
		$categoryId = JArrayHelper::getValue($data, 'catid', $this->input->getInt('filter_category_id'), 'int');
		$allow = null;

		if ($categoryId) {
			// If the category has been passed in the URL check it.
			$allow = $user->authorise('core.create', $this->option . '.category.' . $categoryId);
		}

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		}else{
			return $allow;
		}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;

		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		}else{
			// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}

	/**
	 * Method to run batch operations.
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean   True if successful, false otherwise and internal error is set.
	 *
	 * @since   1.7
	 */
	public function batch($model = null) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('Admin', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_applications&view=admins' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		$task = $this->getTask();

		if ($task == 'save')
		{
			$this->setRedirect(JRoute::_('index.php?option=com_applications&view=admins', false));
		}
	}

	function save($key = null, $urlVar = null) {
		$task	= $this->getTask();
		$app = JFactory::getApplication();
		$id = $app->input->getInt('id');
		$db		= JFactory::getDbo();
		$post	= JRequest::get('post');

		$name		 = $post['jform']['name'];
		$email		 = $post['jform']['email'];
		$name_agent  = $post['jform']['name_agent'];
		$email_agent = $post['jform']['email_agent'];

		$query = $db->getQuery(true);
		$query->update('#__applications_admin');
		$query->set('name = "' . $name . '"');
		$query->set('email = "' . $email . '"');
		$query->set('name_agent = "' . $name_agent . '"');
		$query->set('email_agent = "' . $email_agent . '"');
		$query->where('id = "' . $id . '"');

		$db->setQuery($query);

		if(!$db->execute()) {
			$msg = "資料庫存取失敗。";
		}else{
			$msg = JText::_('COM_APPLICATIONS_N_ITEMS_ORDERING');
		}
	
		switch($task) {
			case 'apply':
				// Redirect back to the edit screen.
				$this->setRedirect("index.php?option=" . $this->option . "&task=" . $this->view_item . ".edit&id=" . $id);  // view=applications&layout=edit => task=applications.edit
				break;
			case 'save':
				$this->setRedirect("index.php?option=" . $this->option . "&view=" . $this->view_list . $this->getRedirectToListAppend(), $msg);
				break;
		}
	}
}
