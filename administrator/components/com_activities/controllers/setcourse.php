<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * setcourse list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.6
 */
class ApplicationsControllerSetcourse extends JControllerAdmin {

	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Setcourse', $prefix = 'ApplicationsModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}

	function getData() {
		$this->setRedirect("index.php?option=com_applications&view=setcourse");
	}
	
	function cancel() {
		$this->setRedirect("index.php?option=com_applications");
	}
	
	function save() {
		$task	= $this->getTask();
		$db		= JFactory::getDBO();
		$app 	= JFactory::getApplication();

		$post = $app->input->get('jform', '', 'array');

		$query = $db->getQuery(true);

		// 清空資料
		$query = "TRUNCATE TABLE #__applications_setcourse";
		$db->setQuery($query);
		if(!$db->execute()) {
			$msg = "資料庫存取失敗。";
		}else{
			$msg = JText::_('COM_APPLICATIONS_N_ITEMS_ORDERING');

			$columns = array('title', 'fit', 'teach_time');

			for ($i = 0; $i < count($post["title"]); $i++) {
				$query = $db->getQuery(true);
				$values = array($db->quote($post["title"][$i]), $db->quote($post["fit"][$i]), $db->quote($post["teach_time"][$i]));

				$query
					->insert($db->quoteName('#__applications_setcourse'))
					->columns($db->quoteName($columns))
					->values(implode(',', $values));

				$db->setQuery($query);
				$db->query();
			}
		}

		
		switch($task) {
			case 'save':
				$this->setRedirect("index.php?option=com_applications" , $msg);
				break;
		}		
	}
}
