<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * setmail list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.6
 */
class ApplicationsControllerSetmail extends JControllerAdmin {

	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Setmail', $prefix = 'ApplicationsModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}

	function getData() {
		$this->setRedirect("index.php?option=com_applications&view=setmail");
	}
	
	function cancel() {
		$this->setRedirect("index.php?option=com_applications");
	}
	
	function save() {
		$task	= $this->getTask();
		$db		= JFactory::getDBO();
		$app 	= JFactory::getApplication();
		$post	= JRequest::get('post');
		$subject = $post['subject'];
		
		$stext = $app->input->getString('stext');
		$content = ereg_replace("\n", "<br />\n", $stext);
		
		// $post['stext'] = AddSlashes(JRequest::getVar('stext', '', 'post', 'string', JREQUEST_ALLOWRAW));
		// $stext	 = nl2br($post['stext']);
		
		$query = $db->getQuery(true);
		$query->update('#__applications_setmail');
		$query->set('subject = "' . $subject . '"');
		$query->set('content = "' . $content . '"');
		$query->where('id = "1"');
		
		$db->setQuery($query);	
		if(!$db->execute()) {
			$msg = "資料庫存取失敗。";
		}else{
			$msg = JText::_('COM_APPLICATIONS_N_ITEMS_ORDERING');
		}

		switch($task) {
			case 'save':
				$this->setRedirect("index.php?option=com_applications" , $msg);
				break;
		}		
	}
}
