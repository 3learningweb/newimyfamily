<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


class ApplicationsControllerGuidenotice extends JControllerAdmin
{

     /**
	 * Proxy for getModel.
	 * @since   1.6
	 */
     public function getModel($name = 'Guidenotice', $prefix = 'ApplicationsModel', $config = array('ignore_request' => true)) {
	 $model = parent::getModel($name, $prefix, $config);
	 return $model;
    }

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}

	function cancel() {
		$this->setRedirect("index.php?option=com_applications");
	}


	function save($key = null, $urlVar = null) {
		$task	= $this->getTask();	
	    $noticeContent = JRequest::getVar('notice_content', '', 'post', 'string', JREQUEST_ALLOWRAW);
 		
		$db		= JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->update('#__applications_notice');
		$query->set('notice_content = ' . $db->quote($noticeContent) );
		$query->where('type = "guide" ');

		$db->setQuery($query);

		if(!$db->execute()) {
			$msg = JText::_('COM_APPLICATIONS_ERROR_DBACCESS');
		}else{
			$msg = JText::_('COM_APPLICATIONS_N_ITEMS_ORDERING');
		}

		switch($task) {
			case 'save':
			$link = "index.php?option=" . $this->option ;
			$this->setRedirect( $link, $msg);
			break;
		}
		
	}


}
