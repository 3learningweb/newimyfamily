<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$id = $app->input->getInt('id');

$states = array('處理中', '已通過', '已取消');
$states_color = array( '#FFF68F', '#CAFF70', '#FFEBCD' );

?>

<div class="accordion-group">
	<form method="post" name="adminForm" id="admin-form" class="form-validate">
		<input type="hidden" name="appid" id="appid" value="<?php echo $this->application->id; ?>" />
		<input type="hidden" name="mailid" id="mailid" value="<?php echo $id; ?>" />
		<div class="row-fluid">
			<!-- Begin applications -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>		
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_APPLICATIONS_FORM_DETAILS', true)); ?>
					<?php if($this->application->admin_id && $this->application->reply_date){ ?>
						<h4 style="color: #666666; padding-left: 5px;">回覆資訊：管理者<?php echo $this->admin->username; ?> 已於 <?php echo $this->application->reply_date; ?> 回覆給使用者</h4>
					<?php } ?>
					<table width="100%" class="admintable" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="key">處理狀態</td>
							<td>
								<select id="state" name="state" style="background: <?php echo $states_color[$this->application->state]; ?>">
								<?php foreach($states as $key => $state) { ?>
									<option value="<?php echo $key;?>" <?php if ($key == $this->application->state) echo "selected"; ?> style="background:<?php echo $states_color[$key];?>" ><?php echo $state;?></option>
								<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="key">姓名</td>
							<td><?php echo $this->mail->name; ?></td>
						</tr>
<!--						<tr>
							<td class="key">電話</td>
							<td><?php echo $this->mail->tel; ?></td>
						</tr>-->
						<tr>
							<td class="key">電子郵件</td>
							<td><?php echo $this->mail->email; ?></td>
						</tr>
<!--						<tr>
							<td class="key">聯絡地址</td>
							<td><?php echo $this->mail->address; ?></td>
						</tr>-->
						<tr>
							<td class="key">留言標題</td>
							<td><?php echo $this->mail->title; ?></td>
						</tr>
						<tr>
							<td class="key">留言內容</td>
							<td><?php echo $this->mail->text; ?></td>
						</tr>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'sendmail', JText::_('COM_APPLICATIONS_SENDMAIL', true)); ?>
					<table class="admintable" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="key">主旨</td>
							<td><input type="text" size="60" id="mail_subject" name="mail_subject" style="width: 300px;" /></td>
						</tr>
						<tr>
							<td class="key">內容</td>
							<td><textarea cols="50" rows="10" id="mail_content" name="mail_content" style="width: 350px;"></textarea></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><input type="button" id="sendmail_btn" onclick="sendMail()" value="送出" /></td>
						</tr>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="lang" value="<?php echo JFactory::getLanguageId(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
			</fieldset>
			</div>
		</div>
		<!-- End applications -->
	</form>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#state_chzn").css("display", "none");
		jQuery("#state").css("display", "block");
	})

	Joomla.submitbutton = function(task)
	{
		if (task == 'mail.cancel' || document.formvalidator.isValid(document.id('admin-form')))
		{
			Joomla.submitform(task, document.getElementById('admin-form'));
		}
	}
	
	function sendMail() {
		if(!jQuery("#mail_subject").val()) {
			alert("請填寫線上回覆主旨");
			return false;
		}else if(!jQuery("#mail_content").val()) {
			alert("請填寫線上回覆內容");
			return false;
		}else {
			if(confirm("確定要送出回覆？")) {
				Joomla.submitbutton('mail.sendmail');
			}
		}
	}
	
</script>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	.admintable .key {
		background-color: #f6f6f6;
		text-align: right;
		width: 140px;
		color: #666;
		font-weight: bold;
		border-bottom: 1px solid #e9e9e9;
		border-right: 1px solid #e9e9e9;		
	}
	.admintable td {
		padding: 3px;
	}
</style>