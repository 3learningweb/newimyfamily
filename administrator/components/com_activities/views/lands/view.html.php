<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of lands.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.5
 */
class ApplicationsViewLands extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items	= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		$this->states = array('處理中', '已通過', '已取消');
		ApplicationsHelper::addSubmenu('lands');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/applications.php';

		$state	= $this->get('State');
		$canDo	= ApplicationsHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_APPLICATIONS'), 'appform.png');

		if ($canDo->get('core.edit'))
		{
			JToolbarHelper::editList('land.edit');
		}

		JHtmlSidebar::setAction('index.php?option=com_applications&view=admins');

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_state',
			JHtml::_('select.options', $this->states, 'value', 'text', $this->state->get('filter.state'), true)
		);

		
	}


	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.state' => '處理狀態',
			'a.doc_num' => '案件編號',
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}

}
