<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
$editor = &JFactory::getEditor();
// print_r($this->setcourse);
?>

<div class="accordion-group">
	<form method="post" name="adminForm" id="admin-form" class="form-validate">
		<div class="row-fluid">
			<!-- Begin applications -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>		
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_APPLICATIONS_FORM_DETAILS', true)); ?>
					<table width="800" class="admintable" border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="key" width="50">編號</td>
								<td class="key" width="80">課程名稱</td>
								<td class="key" width="80">適合年級</td>
								<td class="key" width="80">課程時間</td>
								<td class="key" width="50">&nbsp;</td>
							</tr>
						</thead>
				
						<tbody id="course_list">

						</tbody>
						<tfoot>
							<tr>
								<td>&nbsp;</td>
								<td>
									<input type="text" id="title" value="" style="width: 200px;" />
								</td>
								<td>
									<input type="text" id="fit" value="" style="width: 200px;" />
								</td>
								<td>
									<input type="text" id="teach_time" value="" style="width: 200px;" />
								</td>
								<td>
									<a href="javascript: add_list()">新增</a>
								</td>
							</tr>
						</tfoot>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="lang" value="<?php echo JFactory::getLanguageId(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
			</fieldset>
			</div>
		</div>
		<!-- End applications -->
	</form>
</div>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'setcourse.cancel' || document.formvalidator.isValid(document.id('admin-form')))
		{
			Joomla.submitform(task, document.getElementById('admin-form'));
		}
	}
	
	
	var course_list_title = [];
	var course_list_fit = [];
	var course_list_teach_time = [];
	<?php
		if (count($this->setcourse) > 0) {
			foreach ($this->setcourse as $i => $course) {
				echo "course_list_title[". $i. "]=\"". $course->title. "\";\n";
				echo "course_list_fit[". $i. "]=\"". $course->fit. "\";\n";
				echo "course_list_teach_time[". $i. "]=\"". $course->teach_time. "\";\n";
			}
		}
	?>

	// 新增
	function add_list() {
		$ = jQuery;
		if ($("#title").val() == "") {
			alert('請填入課程名稱！');
			return;
		}

		if ($("#fit").val() == "") {
			alert('請填入適合年級！');
			return;
		}

		if ($("#teach_time").val() == "") {
			alert('請填入課程時間！');
			return;
		}

		course_list_title.push($("#title").val());
		course_list_fit.push($("#fit").val());
		course_list_teach_time.push($("#teach_time").val());

		show_list();
		$("#title").val('');
		$("#fit").val('');
		$("#teach_time").val('');
	}

	// 由清單中移除
	function del_list(_id) {
		course_list_title.splice(_id, 1);
		course_list_fit.splice(_id, 1);
		course_list_teach_time.splice(_id, 1);

		show_list();
	}

	// 列出所有清單
	function show_list() {
		$ = jQuery;
		$("#course_list").empty();

		if (course_list_title.length == 0) return;

		for (var i = 0; i < course_list_title.length; i++) {
			$("#course_list").append("<tr>" +
				"<td>" + (i+1) + "</td>" +
				"<td>" + course_list_title[i] + "</td>" +
				"<td>" + course_list_fit[i] + "</td>" +
				"<td>" + course_list_teach_time[i] + "</td>" +
				"<td><a href=\"javascript: del_list((" + i + "));\">刪除</a>\n\
				<input name=\"jform[title][]\"  value=\"" + course_list_title[i] + "\" type=\"hidden\">\n\
				<input name=\"jform[fit][]\"  value=\"" + course_list_fit[i] + "\" type=\"hidden\">\n\
				<input name=\"jform[teach_time][]\"  value=\"" + course_list_teach_time[i] + "\" type=\"hidden\">\n\
				</td></tr>");

		}
	}


	jQuery(document).ready(function(){
		show_list();

	});
	
</script>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	.admintable .key {
		background-color: #f6f6f6;
		text-align: center;
		color: #666;
		font-weight: bold;
		border-bottom: 1px solid #e9e9e9;
		border-right: 1px solid #e9e9e9;		
	}
	.admintable td {
		padding: 3px;
	}
	.admintable #stext {
		width: 500px;
	}
</style>