<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$id = $app->input->getInt('id');

$states = array('處理中', '已通過', '已取消');
$states_color = array( '#FFF68F', '#CAFF70', '#FFEBCD' );

?>

<div class="accordion-group">
	<form method="post" name="adminForm" id="admin-form" class="form-validate">
		<input type="hidden" name="appid" id="appid" value="<?php echo $this->application->id; ?>" />
		<input type="hidden" name="courseid" id="courseid" value="<?php echo $id; ?>" />
		<div class="row-fluid">
			<!-- Begin applications -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>		
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_APPLICATIONS_FORM_DETAILS', true)); ?>
				
					<table width="100%" class="admintable" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="key">處理狀態</td>
							<td>
								<select id="state" name="state" style="background: <?php echo $states_color[$this->application->state]; ?>">
								<?php foreach($states as $key => $state) { ?>
									<option value="<?php echo $key;?>" <?php if ($key == $this->application->state) echo "selected"; ?> style="background:<?php echo $states_color[$key];?>" ><?php echo $state;?></option>
								<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="key">申請項目 </td>
							<td><?php echo $this->course->apply_item; ?></td>
						</tr>
						<tr>
							<td class="key">學校名稱</td>
							<td><?php echo $this->course->school_name; ?></td>
						</tr>
						<tr>
							<td class="key">聯絡人</td>
							<td><?php echo $this->course->contact; ?></td>
						</tr>
						<tr>
							<td class="key">手機話碼</td>
							<td><?php echo $this->course->phone; ?></td>
						</tr>
						<tr>
							<td class="key">電子郵件</td>
							<td><?php echo $this->course->email; ?></td>
						</tr>
						<tr>
							<td class="key">聯絡電話</td>
							<td>
								<?php echo ($this->course->tel_day) ? "TEL(日):". $this->course->tel_day. "<br>" : ""; ?>
								<?php echo ($this->course->tel_night) ? "TEL(夜):". $this->course->tel_night. "<br>" : ""; ?>
								<?php echo ($this->course->tel_fax) ? "FAX:". $this->course->tel_fax : ""; ?>
							</td>
						</tr>
						<tr>
							<td class="key">聯絡地址</td>
							<td><?php echo $this->course->address; ?></td>
						</tr>
						<tr>
							<td class="key">申請人數</td>
							<td>
								<?php
								echo $this->course->apply_num;
								?>
							</td>
						</tr>
						<tr>
							<td class="key">預定時間 </td>
							<td>
								<?php
									$apply_time = json_decode($this->course->apply_time);
									foreach ($apply_time as $i => $time) {
										echo $time. "<br>";
									}
								?>
							</td>
						</tr>
						<tr>
							<td class="key">是否同意活動照片供本處宣傳使用</td>
							<td><?php echo ($this->course->is_agree_pic == 1) ? "是" : "否"; ?></td>
						</tr>

						<tr>
							<td colspan="2">環教課程功能無提供線上回覆，需與申請者聯絡處理。</td>
						</tr>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="lang" value="<?php echo JFactory::getLanguageId(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
			</fieldset>
			</div>
		</div>
		<!-- End applications -->
	</form>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#state_chzn").css("display", "none");
		jQuery("#state").css("display", "block");
	})

	Joomla.submitbutton = function(task)
	{
		if (task == 'course.cancel' || document.formvalidator.isValid(document.id('admin-form')))
		{
			Joomla.submitform(task, document.getElementById('admin-form'));
		}
	}
	
	function sendCourse() {
		if(!jQuery("#course_subject").val()) {
			alert("請填寫線上回覆主旨");
			return false;
		}else if(!jQuery("#course_content").val()) {
			alert("請填寫線上回覆內容");
			return false;
		}else {
			if(confirm("確定要送出回覆？")) {
				Joomla.submitbutton('course.sendcourse');
			}
		}
	}
	
</script>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	.admintable .key {
		background-color: #f6f6f6;
		text-align: right;
		width: 140px;
		color: #666;
		font-weight: bold;
		border-bottom: 1px solid #e9e9e9;
		border-right: 1px solid #e9e9e9;		
	}
	.admintable td {
		padding: 3px;
	}
</style>