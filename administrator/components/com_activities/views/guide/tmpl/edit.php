<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$id = $app->input->getInt('id');

$states = array('處理中', '已通過', '已取消');
$states_color = array( '#FFF68F', '#CAFF70', '#FFEBCD' );
$guidetypes = array( '解說', '影片' );
// print_r($this->application);
// print_r($this->guide);
?>

<div class="accordion-group">
	<form method="post" name="adminForm" id="admin-form" class="form-validate">
		<input type="hidden" name="appid" id="appid" value="<?php echo $this->application->id; ?>" />
		<input type="hidden" name="guideid" id="guideid" value="<?php echo $id; ?>" />
		<div class="row-fluid">
			<!-- Begin applications -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>		
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_APPLICATIONS_FORM_DETAILS', true)); ?>
					<table width="100%" class="admintable" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="key">案號</td>
							<td><?php echo $this->application->serial_num; ?></td>
						</tr>
						<tr>
							<td class="key">處理狀態</td>
							<td>
								<select id="state" name="state" style="background: <?php echo $states_color[$this->application->state]; ?>">
								<?php foreach($states as $key => $state) { ?>
									<option value="<?php echo $key;?>" <?php if ($key == $this->application->state) echo "selected"; ?> style="background:<?php echo $states_color[$key];?>" ><?php echo $state;?></option>
								<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="key">團體名稱</td>
							<td><?php echo $this->guide->team; ?></td>
						</tr>
						<tr>
							<td class="key">人數</td>
							<td><?php echo $this->guide->num; ?></td>
						</tr>
						<tr>
							<td class="key">聯絡人</td>
							<td>
					        <?php
					        	if( $this->application->state == 1 || $this->application->state == 2 ){
					                echo substr_replace( $this->guide->name, '***', -3 );
					        	}else{
					                echo $this->guide->name;
					        	}
					        ?>								
							</td>
						</tr>
						<tr>
							<td class="key">性別</td>
							<td><?php echo $this->guide->sex; ?></td>
						</tr>
						<tr>
							<td class="key">電話</td>
							<td>
					        <?php
					        	if( $this->application->state == 1 || $this->application->state == 2 ){
					                echo substr_replace( $this->guide->tel, '***', -3 );
					        	}else{
					                echo $this->guide->tel;
					        	}
					        ?>
							</td>
						</tr>
						<tr>
							<td class="key">電子郵件</td>
							<td>
					        <?php
					        	if( $this->application->state == 1 || $this->application->state == 2 ){
					                echo substr_replace( $this->guide->email, '***', 0, 3 );
					        	}else{
					                echo $this->guide->email;
					        	}
					        ?>
							</td>
						</tr>
						<tr>
							<td class="key">聯絡地址</td>
							<td><?php echo $this->guide->address; ?></td>
						</tr>
						<tr>
							<td class="key">解說項目</td>
							<td>
							<?php 
								echo ($this->item->type_ps) ? $this->item->type_ps . ":" . $this->item->type_ps_lot . "<br/>" : "";
								echo ($this->item->type_vd) ? $this->item->type_vd . ":" . $this->item->type_vd_lot : "";
							?>
							</td>
						</tr>
						<tr>
							<td class="key">預約日期</td>
							<td><?php echo $this->guide->date; ?></td>
						</tr>
						<tr>
							<td class="key">預約時間</td>
							<td><?php echo $this->guide->time; ?></td>
						</tr>
						<tr>
							<td class="key">備註</td>
							<td><?php echo $this->guide->notes; ?></td>
						</tr>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="lang" value="<?php echo JFactory::getLanguageId(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
			</fieldset>
			</div>
		</div>
		<!-- End applications -->
	</form>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#state_chzn").css("display", "none");
		jQuery("#state").css("display", "block");
	})

	Joomla.submitbutton = function(task)
	{
		if (task == 'guide.cancel' || document.formvalidator.isValid(document.id('admin-form')))
		{
			Joomla.submitform(task, document.getElementById('admin-form'));
		}
	}
</script>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	.admintable .key {
		background-color: #f6f6f6;
		text-align: right;
		width: 140px;
		color: #666;
		font-weight: bold;
		border-bottom: 1px solid #e9e9e9;
		border-right: 1px solid #e9e9e9;		
	}
	.admintable td {
		padding: 3px;
	}
</style>