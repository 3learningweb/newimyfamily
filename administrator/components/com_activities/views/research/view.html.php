<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View to edit a research.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.5
 */
class ApplicationsViewResearch extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');
		$this->application = $this->get('Application');

		$keys = array('organize', 'name', 'id_num', 'address', 'tel', 'email');
		$this->research = JUtility::batchDecode($this->item, $keys);	

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);
		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		// Since we don't track these assets at the item level, use the category id.
		$canDo		= ApplicationsHelper::getActions($this->item->catid, 0);

		JToolbarHelper::title(JText::_('COM_APPLICATIONS_SUBMENU_RESEARCHES'), 'generic.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit')||(count($user->getAuthorisedCategories('com_applications', 'core.create')))))
		{
			JToolbarHelper::save('research.save');
		}
		if (!$checkedOut && (count($user->getAuthorisedCategories('com_applications', 'core.create')))){
			JToolbarHelper::save2new('research.save2new');
		}

		if (empty($this->item->id))
		{
			JToolbarHelper::cancel('research.cancel');
		}
		else
		{
			JToolbarHelper::cancel('research.cancel', 'JTOOLBAR_CLOSE');
		}

	}
}
