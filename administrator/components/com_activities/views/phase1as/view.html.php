<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of phase1as.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesViewPhase1as extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->cnt			= $this->get('Cnt');

		$this->states = array('處理中', '已通過', '已取消');
		$this->item = array('解說' => '解說', '影片' => '影片');

		ActivitiesHelper::addSubmenu('phase1as');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/activities.php';

		$state	= $this->get('State');
		$canDo	= ActivitiesHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_ACTIVITIES'), 'appform.png');

		if ($canDo->get('core.edit'))
		{
			JToolbarHelper::editList('guide.edit');
		}
		
		JHtmlSidebar::setAction('index.php?option=com_activities&view=admins');

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_state',
			JHtml::_('select.options', $this->states, 'value', 'text', $this->state->get('filter.state'), true)
		);

		JHtmlSidebar::addFilter(
			"申請項目",
			'filter_type',
			JHtml::_('select.options', $this->item, 'value', 'text', $this->state->get('filter.type'), true)
		);
	}


	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.state' => '處理狀態',
			'a.doc_num' => '案件編號',
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}

}
