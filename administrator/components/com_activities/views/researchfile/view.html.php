<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View to edit a researchfile.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.5
 */

class ApplicationsViewResearchfile extends JViewLegacy
{
	protected $state;

	protected $researchfile;

	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->researchfile		= $this->get('Item');
		$this->form		= $this->get('Form');


		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hideresearchfilemenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->researchfile->id == 0);
		$checkedOut	= !($this->researchfile->checked_out == 0 || $this->researchfile->checked_out == $user->get('id'));
		// Since we don't track these assets at the researchfile level, use the category id.
		$canDo		= ApplicationsHelper::getActions($this->researchfile->catid, 0);

		JToolbarHelper::title(JText::_('COM_APPLICATIONS'), 'appform.png');

		// If not checked out, can save the researchfile.
		if (!$checkedOut && ($canDo->get('core.edit')||(count($user->getAuthorisedCategories('com_applications', 'core.create')))))
		{
			JToolbarHelper::apply('researchfile.apply');
			JToolbarHelper::save('researchfile.save');
		}
		if (!$isNew){
			JToolbarHelper::save2new('researchfile.save2new');
		}
		// If an existing researchfile, can save to a copy.
		if (!$isNew)
		{
			JToolbarHelper::save2copy('researchfile.save2copy');
		}
		if (empty($this->researchfile->id))
		{
			JToolbarHelper::cancel('researchfile.cancel');
		}
		else
		{
			JToolbarHelper::cancel('researchfile.cancel', 'JTOOLBAR_CLOSE');
		}

		
	}
}
