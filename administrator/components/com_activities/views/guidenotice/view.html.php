<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of guide notice.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.5
 */
class ApplicationsViewGuidenotice extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		// $this->state	= $this->get('State');
		$this->form		= $this->get('Form');
		$this->guidenotice		= $this->get('Item');
		// $this->guidenotice 	= $this->get('Guidenotice');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->setDocument();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);
		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		// Since we don't track these assets at the item level, use the category id.
		$canDo		= ApplicationsHelper::getActions($this->item->catid, 0);

		JToolbarHelper::title(JText::_('COM_APPLICATIONS_SUBMENU_GUIDENOTICE'), 'generic.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit')||(count($user->getAuthorisedCategories('com_applications', 'core.create')))))
		{
			// JToolbarHelper::apply('guidenotice.apply');
			JToolbarHelper::save('guidenotice.save');
		}
		if (!$checkedOut && (count($user->getAuthorisedCategories('com_applications', 'core.create')))){
			JToolbarHelper::save2new('guidenotice.save2new');
		}

		if (empty($this->item->id))
		{
			JToolbarHelper::cancel('guidenotice.cancel');
		}
		else
		{
			JToolbarHelper::cancel('guidenotice.cancel', 'JTOOLBAR_CLOSE');
		}
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_APPLICATIONS_ADMINISTRATION'));
	}
}
