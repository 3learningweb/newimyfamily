<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$id = $app->input->getInt('id');
$type = array("mail" => "意見信箱", "guide" => "解說及影片預約申請", "research" => "學術研究及採集證申請", "land" => "土地分區使用證明申請", "setmail" => "通知信函設定", "course" => "環教課程申請");
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'admin.cancel' || document.formvalidator.isValid(document.id('admin-form')))
		{
			Joomla.submitform(task, document.getElementById('admin-form'));
		}
	}
	
</script>
<div class="accordion-group">
	<form action="<?php echo JRoute::_('index.php?option=com_applications&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="admin-form" class="form-validate">
	
		<div class="row-fluid">
			<!-- Begin applications -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('type'); ?></div>
					<div class="controls"><input type="text" value="<?php echo $type[$this->item->type]; ?>" readonly="readonly"/></div>
				</div>	
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('email'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('email'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('name_agent'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('name_agent'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('email_agent'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('email_agent'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('menuid'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('menuid'); ?></div>
				</div>

				<input type="hidden" name="task" value="" />
				<input type="hidden" name="lang" value="<?php echo JFactory::getLanguageId(); ?>" />
				<input type="hidden" name="jform[lang]" value="<?php echo JFactory::getLanguageId(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
		
			</fieldset>
			</div>
		</div>
		<!-- End applications -->
	</form>
</div>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	
</style>