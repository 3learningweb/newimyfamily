<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$id = $app->input->getInt('id');

$states = array( '處理中', '已通過', '已取消' );
$states_color = array( '#FFF68F', '#CAFF70', '#FFEBCD' );
// print_r($this->land);
?>

<div class="accordion-group">
	<form method="post" name="adminForm" id="admin-form" class="form-validate">
		<input type="hidden" name="appid" id="appid" value="<?php echo $this->application->id; ?>" />
		<input type="hidden" name="landid" id="landid" value="<?php echo $id; ?>" />
		<div class="row-fluid">
			<!-- Begin applications -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>		
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_APPLICATIONS_FORM_DETAILS', true)); ?>
					<table width="100%" class="admintable" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="key">案號</td>
							<td><?php echo $this->application->doc_num; ?></td>
						</tr>
						<tr>
							<td class="key">處理狀態</td>
							<td>
								<select id="state" name="state" style="background: <?php echo $states_color[$this->application->state]; ?>">
								<?php foreach($states as $key => $state) { ?>
									<option value="<?php echo $key;?>" <?php if ($key == $this->application->state) echo "selected"; ?> style="background:<?php echo $states_color[$key];?>" ><?php echo $state;?></option>
								<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="key">申請人姓名</td>
							<td>
					        <?php
					        	if( $this->application->state == 1 || $this->application->state == 2 ){
					                echo substr_replace( $this->land->name, '***', -3 );
					        	}else{
					                echo $this->land->name;
					        	}
					        ?>
							</td>
						</tr>
						<tr>
							<td class="key">聯絡電話</td>
							<td>
					        <?php
					        	if( $this->application->state == 1 || $this->application->state == 2 ){
					                echo substr_replace( $this->land->tel, '***', -3 );
					        	}else{
					                echo $this->land->tel;
					        	}
					        ?>
							</td>
						</tr>
						<tr>
							<td class="key">電子郵件</td>
							<td>
							<?php
						        if( $this->application->state == 1 || $this->application->state == 2 ){
						        	echo substr_replace( $this->research->email, '***', 0, 3 );
						        }else{
						        	echo $this->land->email;
						        }
							?>
							</td>
						</tr>
						<tr>
							<td class="key">聯絡地址</td>
							<td><?php echo $this->land->address; ?></td>
						</tr>
						<tr>
							<td class="key" rowspan="2">申請土地座落</td>
							<td><hr style="border-color: gray; border-top: 0px; margin: 5px 0px;"></td>
						</tr>
						<tr>
							<td>
								<table border="1" cellspacing="0">
								<?php
									for($i=1; $i<=5; $i++):
										$land = 'land'.$i;
										if($this->item->$land):
								?>
									<tr>
										<td><?php echo $this->land->$land;?></td>
									</tr>
								<?php
										endif;
									endfor;
								?>
								</table>
							</td>
						</tr>
						<tr>
							<td class="key">應附附件</td>
							<td><hr style="border-color: gray; border-top: 0px; margin: 5px 0px;"></td>
						</tr>
						<tr>
							<td class="key">(1)地籍圖謄本</td>
							<td>
							<?php if($this->land->file1) { ?>
								<a href="<?php echo JURI::root() . $this->land->file1; ?>" target="_blank" ><?php echo JURI::root() . $this->land->file1; ?></a>
							<?php } ?>
							</td>
						</tr>
						<tr>
							<td class="key">(2)土地登記謄本</td>
							<td>
							<?php if($this->land->file2) { ?>
								<a href="<?php echo JURI::root() . $this->land->file2; ?>" target="_blank" ><?php echo JURI::root() . $this->land->file2; ?></a>
							<?php } ?>
							</td>
						</tr>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="lang" value="<?php echo JFactory::getLanguageId(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
			</fieldset>
			</div>
		</div>
		<!-- End applications -->
	</form>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#state_chzn").css("display", "none");
		jQuery("#state").css("display", "block");
	})

	Joomla.submitbutton = function(task)
	{
		if (task == 'land.cancel' || document.formvalidator.isValid(document.id('admin-form')))
		{
			Joomla.submitform(task, document.getElementById('admin-form'));
		}
	}
	
</script>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	.admintable .key {
		background-color: #f6f6f6;
		text-align: right;
		width: 140px;
		color: #666;
		font-weight: bold;
		border-bottom: 1px solid #e9e9e9;
		border-right: 1px solid #e9e9e9;		
	}
	.admintable td {
		padding: 3px;
	}
</style>