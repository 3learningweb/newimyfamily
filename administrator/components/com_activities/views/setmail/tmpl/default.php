<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
$editor = &JFactory::getEditor();
// print_r($this->setmail);
?>

<div class="accordion-group">
	<form method="post" name="adminForm" id="admin-form" class="form-validate">
		<div class="row-fluid">
			<!-- Begin applications -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>		
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_APPLICATIONS_FORM_DETAILS', true)); ?>
					<table width="100%" class="admintable" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="key">E-mail主旨</td>
							<td><input type="text" name="subject" id="subject" value="<?php echo $this->setmail->subject; ?>" style="width: 500px;" /></td>
						</tr>
						<tr>
							<td class="key">E-mail內容</td>
							<td>
								<?php $str = preg_replace("/<br \/>\n/", "\n", $this->setmail->content); ?>
								<textarea name="stext" id="stext" rows="14"><?php echo $str; ?></textarea>
							</td>
						</tr>
						<tr>
							<td class="key">參數</td>
							<td>
								{name}:申請人姓名<br/>
								{created_date}:申請日期<br/>
								{referance_num}:案號<br/>
								{today_date}:處理日期<br/>
								{state}:處理狀態<br/>
								{sitename}:管理處<br/>
								以上為程式所帶出之訊息，若無必要請勿更動。
							</td>
						</tr>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="lang" value="<?php echo JFactory::getLanguageId(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
			</fieldset>
			</div>
		</div>
		<!-- End applications -->
	</form>
</div>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'setmail.cancel' || document.formvalidator.isValid(document.id('admin-form')))
		{
			Joomla.submitform(task, document.getElementById('admin-form'));
		}
	}
	
</script>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	.admintable .key {
		background-color: #f6f6f6;
		text-align: right;
		width: 140px;
		color: #666;
		font-weight: bold;
		border-bottom: 1px solid #e9e9e9;
		border-right: 1px solid #e9e9e9;		
	}
	.admintable td {
		padding: 3px;
	}
	.admintable #stext {
		width: 500px;
	}
</style>