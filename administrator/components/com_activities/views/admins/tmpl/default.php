<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// multi language setting
$append = JFactory::getLanguageId(true);
$user	= JFactory::getUser();

$type = array("mail" => "處長信箱", "guide" => "解說及影片預約申請", "research" => "學術暨研究採集證申請", "land" => "土地分區使用證明申請", "setmail" => "通知信函設定" );

?>
<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>


<form action="<?php echo JRoute::_('index.php?option=com_applications&view=admins'.$append); ?>" method="post" name="adminForm" id="adminForm">
		<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10"> 
<?php else : ?>
	<div id="j-main-container"> 
<?php endif;?>
		<div class="clearfix"> </div>

		<table class="table table-striped" id="weblinkList">
			<thead>
				<tr>
					<th width="1%" class="hidden-phone">
						<?php echo JHtml::_('grid.checkall'); ?>
					</th>

					<th class="title">項目</th>
					<th width="10%">負責人姓名</th>
					<th>負責人E-mail</th>
					<th width="10%">代理人姓名</th>
					<th>代理人E-Mail</th>

					<th width="1%" class="nowrap center hidden-phone">ID</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="10">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
		 	<tbody>
		 	<?php foreach($this->items as $i => $item): ?>
		 		<tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $item->catid?>">
					
					<td class="center hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>

					<td class="nowrap has-context">
						<a href="<?php echo JRoute::_('index.php?option=com_applications&task=admin.edit&id='.(int) $item->id).$append; ?>">
						<?php echo $this->escape($type[$item->type]); ?>
						</a>
					</td>

		 			<td><?php echo $this->escape($item->name); ?></td>
		 			<td><?php echo $this->escape($item->email); ?></td>
		 			<td><?php echo $this->escape($item->name_agent); ?></td>
		 			<td><?php echo $this->escape($item->email_agent); ?></td>

					<td class="center hidden-phone">
						<?php echo (int) $item->id; ?>
					</td>
		 		</tr>
		 	<?php endforeach; ?>
		 	</tbody>
		 	
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
</form>
