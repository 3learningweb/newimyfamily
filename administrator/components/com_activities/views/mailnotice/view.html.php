<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of mail notice.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.5
 */
class ApplicationsViewMailnotice extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->form		= $this->get('Form');
		$this->mailnotice		= $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->setDocument();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);
		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		// Since we don't track these assets at the item level, use the category id.
		$canDo		= ApplicationsHelper::getActions($this->item->catid, 0);

		JToolbarHelper::title(JText::_('COM_APPLICATIONS_SUBMENU_MAILNOTICE'), 'generic.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit')||(count($user->getAuthorisedCategories('com_applications', 'core.create')))))
		{
			JToolbarHelper::save('mailnotice.save');
		}
		if (!$checkedOut && (count($user->getAuthorisedCategories('com_applications', 'core.create')))){
			JToolbarHelper::save2new('mailnotice.save2new');
		}

		if (empty($this->item->id))
		{
			JToolbarHelper::cancel('mailnotice.cancel');
		}
		else
		{
			JToolbarHelper::cancel('mailnotice.cancel', 'JTOOLBAR_CLOSE');
		}
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_APPLICATIONS_ADMINISTRATION'));
	}
}
