<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
$editor = JFactory::getEditor();

?>

<div class="accordion-group">
	<form method="post" name="adminForm" id="admin-form" class="form-validate">
		<div class="row-fluid">
			<!-- Begin applications -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>		
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_APPLICATIONS_FORM_MAILNOTICE', true)); ?>
				 
					<table width="100%" class="admintable" border="0" cellspacing="0" cellpadding="0">
		
					<tr>
						<td>
							 <?php 
							 $noticeContent = $this->mailnotice->notice_content;
                             echo $editor->display( 'notice_content',  $noticeContent , '100%', '250', '75', '20' ) ;
							 ?>
						</td>
									
					</tr>
	
					</table>
				

				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="lang" value="<?php echo JFactory::getLanguageId(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
			</fieldset>
			</div>
		</div>
		<!-- End applications -->
	</form>
</div>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'mailnotice.cancel' || document.formvalidator.isValid(document.id('admin-form')))
		{
			Joomla.submitform(task, document.getElementById('admin-form'));
		}
	}
	
</script>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	.admintable .key {
		background-color: #f6f6f6;
		text-align: right;
		width: 140px;
		color: #666;
		font-weight: bold;
		border-bottom: 1px solid #e9e9e9;
		border-right: 1px solid #e9e9e9;		
	}
	.admintable td {
		padding: 3px;
	}
	.admintable #stext {
		width: 500px;
	}
</style>