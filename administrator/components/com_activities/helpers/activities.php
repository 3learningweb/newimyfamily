<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Activities helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesHelper {

	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'phase1as') {
		// multi langeage setting
		$append = JFactory::getLanguageId(true);

		
		JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_PHASE1AS'),
			'index.php?option=com_activities&view=phase1as'.$append,
			$vName == 'phase1as'
		);
 	
		JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_RESEARCHES'),
			'index.php?option=com_activities&view=researches'.$append,
			$vName == 'researches'
		);
		
		JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_LANDS'),
			'index.php?option=com_activities&view=lands'.$append,
			$vName == 'lands'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_MAILS'),
			'index.php?option=com_activities&view=mails'.$append,
			$vName == 'mails'
		);
		/*
		JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_COURSES'),
			'index.php?option=com_activities&view=courses'.$append,
			$vName == 'courses'
		);
		
		JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_SETCOURSE'),
			'index.php?option=com_activities&view=setcourse'.$append,
			$vName == 'setmail'
		);
		*/

        /* 解說 服務申請須知 */
        JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_GUIDENOTICE'),
			'index.php?option=com_activities&view=guidenotice'.$append,
			$vName == 'guidenotice'
		);


        /* 學術 服務申請須知 */
        JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_RESEARCHNOTICE'),
			'index.php?option=com_activities&view=researchnotice'.$append,
			$vName == 'researchnotice'
		);

		/* 土地 服務申請須知 */
        JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_LANDNOTICE'),
			'index.php?option=com_activities&view=landnotice'.$append,
			$vName == 'landnotice'
		);

		/* 處長信箱 服務申請須知 */
        JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_MAILNOTICE'),
			'index.php?option=com_activities&view=mailnotice'.$append,
			$vName == 'mailnotice'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_SETMAIL'),
			'index.php?option=com_activities&view=setmail'.$append,
			$vName == 'setmail'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_ACTIVITIES_SUBMENU_ADMINS'),
			'index.php?option=com_activities&view=admins'.$append,
			$vName == 'admins'
		);


	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0) 
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($categoryId))
		{
			$assetName = 'com_activities';
			$level = 'component';
		}
		else
		{
			$assetName = 'com_activities.category.'.(int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_activities', $level);

		foreach ($actions as $action)
		{
			$result->set($action->name,	$user->authorise($action->name, $assetName));
		}

		return $result;
	}

}
