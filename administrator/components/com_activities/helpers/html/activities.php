<?php
/**
 * @version		$Id: youvideo.php 20228 2011-08-23 00:52:54Z eddieajau $
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * @package		Joomla.Administrator
 * @subpackage	com_content
 */
abstract class JHtmlActivities
{
	/**
	 * change recommend state
	 */
	static function recommend($recommend, $i, $prefix = '', $checkbox = 'cb')
	{	
		$task0 = 'unrecommend';
		$task = 'recommend';
		$translate = true;
		$tip = false;
		$value=$recommend;
		$img1 = 'tick.png';
		$img0 = 'publish_x.png';
		$img = $value ? $img1 : $img0;
		$text = ($recommend)? '選入的項目' : '未選入的項目';
		$active_title = ($recommend)? '選入的項目' : '設為選入項目';
		$inactive_title = ($recommend)? '選入的項目' : '設為選入項目';
		$inactive_class = ($recommend)? 'publish' : 'unpublish';
		if (is_object($value))
		{
			$value = $value->published;
		}

//		$img = $value ? $img1 : $img0;
//		$task = $value ? 'unpublish' : 'publish';
//		$alt = $value ? JText::_('JPUBLISHED') : JText::_('JUNPUBLISHED');
//		$action = $value ? JText::_('JLIB_HTML_UNPUBLISH_ITEM') : JText::_('JLIB_HTML_PUBLISH_ITEM');
		$alt=$text;
		$action=($recommend)? '選入的項目' : '設為選入的項目';
		if($recommend){
			$output = '<a href="#" onclick="return listItemTask(\'cb' . $i . '\',\'' . $prefix . $task0 . '\')" title="' . $action . '">'
			. JHtml::_('image', 'admin/' . $img, $alt, null, true) . '</a>';
		} else {
			$output = '<a href="#" onclick="return listItemTask(\'cb' . $i . '\',\'' . $prefix . $task . '\')" title="' . $action . '">'
			. JHtml::_('image', 'admin/' . $img, $alt, null, true) . '</a>';
		}

		return $output;;
/*
		$task0 = 'unrecommend';
		$task = 'recommend';
		$translate = true;
		$tip = false;
		
		$text = ($recommend)? '選入的項目' : '未選入的項目';
		$active_title = ($recommend)? '選入的項目' : '設為選入項目';
		$inactive_title = ($recommend)? '選入的項目' : '設為選入項目';
		$inactive_class = ($recommend)? 'publish' : 'unpublish';
		
		if($recommend){
			$output =  '<a class="jgrid'.($tip?' hasTip':'').'" href="#" onclick="return listItemTask(\''.$checkbox.$i.'\',\''.$prefix.$task0.'\')" title="'.addslashes(htmlspecialchars($translate?JText::_($inactive_title):$inactive_title, ENT_COMPAT, 'UTF-8')).'">
                        <span class="jgrid'.($tip?' hasTip':'').'" title="'.addslashes(htmlspecialchars($translate?JText::_($active_title):$active_title, ENT_COMPAT, 'UTF-8')).'"><span class="state '.$inactive_class.'"><span class="text">'.($translate?JText::_($text):$text).'</span></span></span>';
		} else {
			$output =  '<a class="jgrid'.($tip?' hasTip':'').'" href="#" onclick="return listItemTask(\''.$checkbox.$i.'\',\''.$prefix.$task.'\')" title="'.addslashes(htmlspecialchars($translate?JText::_($active_title):$active_title, ENT_COMPAT, 'UTF-8')).'">
						<span class="jgrid'.($tip?' hasTip':'').'" title="'.addslashes(htmlspecialchars($translate?JText::_($inactive_title):$inactive_title, ENT_COMPAT, 'UTF-8')).'"><span class="state '.$inactive_class.'"><span class="text">'.($translate?JText::_($text):$text).'</span></span></span>
						</a>';
		}
		
		return $output;
        */
	}
	
}
