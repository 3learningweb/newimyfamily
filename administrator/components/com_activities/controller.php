<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Items Item Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean			$cachable	If true, the view output will be cached
	 * @param   array  $urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController		This object to support chaining.
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		
		require_once JPATH_COMPONENT.'/helpers/activities.php';

		$view   = $this->input->get('view', 'phase1as');
		$layout = $this->input->get('layout', 'default');
		$id     = $this->input->getInt('id');
		JRequest::setVar('view', $view);

		$jinput = JFactory::getApplication()->input;
		$lang = $jinput->get('lang');

		// Check for edit form.
		if ($view == 'admin' && $layout == 'edit' && !$this->checkEditId('com_activities.edit.admin', $id))
		{
			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_activities&view=admins', false));

			return false;
		}		
		
		parent::display();

		return $this;
	}
	
	function sendstate($appid, $state) {
		$db = JFactory::getDBO();
		$state_array = array( '處理中', '已通過', '已取消' );
		$created = date('Y-m-d');
		
		/* 系統通知函 */
		$config = JFactory::getConfig();
		$adminName = $config->get('fromname');  // 系統管理者名字
		$adminEmail = $config->get('mailfrom');  // 系統管理者mail
		$sitename = $config->get('sitename');  // 系統名稱		
		
		/* 取得承辦人、代理人、系統管理者的E-Mail */
		$query_application = $db->getQuery(true);
		$query_application->select("type, application_id, serial_num, created");
		$query_application->from("#__applications");
		$query_application->where("id = '{$appid}'");
		$db->setQuery($query_application);
		$application = $db->loadObject();
		
		$type = $application->type;
		$application_id = $application->application_id;
		$serial_num = $application->serial_num;
		$cre_date = $application->created;

		/* 取得民眾資訊 */
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from("#__applications_$type");
		$query->where("id = '{$application_id}'");
		$db->setQuery($query);
		
		if($type == "mail") { $keys = array('name', 'email', 'tel', 'address'); }
		elseif($type == "guide"){ $keys = array('team', 'name', 'email', 'address'); }
		elseif($type == "research"){ $keys = array('organize', 'name', 'id_num', 'address', 'tel', 'email'); }
		elseif($type == "land"){ $keys = array('name', 'tel', 'address', 'email'); }
		// elseif($type == "course"){ $keys = array('contact', 'phone', 'email'); }

		$userdata = JUtility::batchDecode($db->loadObject(), $keys);

		 /* 取得負責人資訊 */
		$query_admin = $db->getQuery(true);
		$query_admin->select("*");
		$query_admin->from("#__applications_admin");
		$query_admin->where("type = '{$type}'");

		$db->setQuery($query_admin);
		$admin = $db->loadObject();

		$alert_emails = array(
			$admin->name          => $admin->email,
			$admin->name_agent    => $admin->email_agent
		);

		$typename = array("mail" => "「意見信箱」", "guide" => "「解說及影片預約申請」", "research" => "「學術研究及採集證申請」", "land" => "「土地分區使用證明申請」", "course" => "「環教課程申請」");

		$itemid = $admin->menuid;
		
		/* 發送系統通知信 */
		foreach($alert_emails as $key => $value)
		{
			$subject = $sitename.$typename["$type"].'線上申請處理狀態通知函，案號'.$serial_num;
			$alert_msg = $key.' 您好：<br><br>';
			$alert_msg.= $userdata->name."於{$cre_date}{$typename["$type"]}，案號".$serial_num.'的申請，已於'.$created.'處理<br>';
			$alert_msg.= '申請處理結果為：'.$state_array[$state].'<br><br>';
			$alert_msg.= "{$sitename} 敬上<br>";
			$alert_msg.= '◎備註：此信件由系統自動發出，請不要回覆。';
			
			$sendEmailStatus = JUtility::sendMail($adminEmail, $adminName, $value, $subject, $alert_msg, 1);
		}

		$query_setmail = $db->getQuery(true);
		$query_setmail->select("*");
		$query_setmail->from("#__applications_setmail");
		$query_setmail->where("id = '1'");
		$db->setQuery($query_setmail);
		$mail = $db->loadObject();
		
		$subject = $mail->subject;
		$subject = str_replace('{referance_num}', $serial_num, $subject);
		$alert_msg = $mail->content;
		$alert_msg = str_replace('{name}', $userdata->name, $alert_msg);
		$alert_msg = str_replace('{created_date}', $cre_date, $alert_msg);
		$alert_msg = str_replace('{referance_num}', $serial_num, $alert_msg);
		$alert_msg = str_replace('{today_date}', $created, $alert_msg);
		$alert_msg = str_replace('{state}', $state_array[$state], $alert_msg);
		$alert_msg = str_replace('{sitename}', $sitename, $alert_msg);
		$alert_msg = str_replace('線上申請進度', '<a href="'. JURI::root(). 'index.php?option=com_applications&view=applications&layout=inquire&Itemid='. $itemid. '" target="_blank">線上申請進度</a>', $alert_msg);
		$alert_msg = str_replace('href="index.php', 'href="'. JURI::root(). 'index.php', $alert_msg);
		$alert_msg = str_replace('src="filesys', 'src="'. JURI::root(). 'filesys', $alert_msg);

		$sendEmailStatus = JUtility::sendMail($adminEmail, $adminName, $userdata->email, $subject, $alert_msg, 1);
		
	}
}
