<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesModelPhase1as extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
//				'serial_num', 'a.serial_num',
				'state', 'a.state'
			);
		}

		parent::__construct($config);
	}


	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		$type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '');
		$this->setState('filter.type', $type);


		// Load the parameters.
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	public function getListQuery()
	{
		// Create a new query object.
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id AS appid, a.*'
			)
		);

		$query->from($db->quoteName('#__contribute_list2015') . ' AS a ');
		
		
		// Filter by state.
		$state = $this->getState('filter.state');
		if (is_numeric($state)) {
			$query->where('a.state = '.(int) $state);
		}


		// Filter by search
//		$search = $this->getState('filter.search');
//		if (!empty($search)) {
//			$search = $db->quote('%' . $db->escape($search, true) . '%');
//			$query->where('(a.serial_num LIKE ' . $search . ')');

//		}


		// Filter by type.
		$type = $this->getState('filter.type');
		if ($type) {
			$query->where('(g.type_ps = '. $db->quote($type). ' or g.type_vd = '. $db->quote($type). ')');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		
		return $query;
	}
	public function getCnt()
	{
		// Create a new query object.
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		
		// Select the required fields from the table.
		$query->select('count(*) as cnt');

		$query->from($db->quoteName('#__contribute_list2015') . ' AS a ');
		
		
		$query->where('a.state = 1');

		$db->setQuery($query);
		$cnt = $db->loadRow();
		
		return $cnt;
	}
}
