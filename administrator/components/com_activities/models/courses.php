<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.6
 */
class ApplicationsModelCourses extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'serial_num', 'a.serial_num',
				'state', 'a.state'
			);
		}

		parent::__construct($config);
	}


	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		$type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '');
		$this->setState('filter.type', $type);


		// Load the parameters.
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.serial_num', 'desc');
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	public function getListQuery()
	{
		// Create a new query object.
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id AS appid, a.type AS apptype, a.*, c.*'
			)
		);

		$query->from($db->quoteName("#__applications") . " AS a");
		$query->leftJoin("#__applications_course AS c ON a.application_id=c.id");
		$query->where("a.type = " . "'course'");
		

		// Filter by state.
		$state = $this->getState('filter.state');
		if (is_numeric($state)) {
			$query->where('a.state = '.(int) $state);
		}

		// Filter by type.
		$type = $this->getState('filter.type');
		if (!empty($type)) {
			$query->where("c.apply_item = '{$type}'");
		}


		// Filter by search
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where('(a.serial_num LIKE ' . $search . ')');

		}


		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		$query->order($db->escape($orderCol . ' ' . $orderDirn));


		return $query;
	}


	public function getApplyItem() {
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$query->select("title as text, title as value");
		$query->from("#__applications_setcourse");
		$query->where("1");

		$db->setQuery($query);
		$setcourse = $db->loadObjectList();

		return $setcourse;
	}

}
