<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Applications model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.5
 */
class ApplicationsModelMail extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_APPLICATIONS';

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Mail', $prefix = 'ApplicationsTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_applications.mail', 'mail', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_applications.edit.mail.data', array());

		if (empty($data))
		{
			$data = $this->getItem();
			
			// Prime some default values.
			if ($this->getState('mail.id') == 0)
			{
				$app = JFactory::getApplication();
				$data->set('catid', $app->input->get('catid', $app->getUserState('com_applications.mails.filter.category_id'), 'int'));
			}			
		}

		$this->preprocessData('com_applications.mail', $data);

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			if (!empty($item->id))
			{
				$item->tags = new JHelperTags;
				$item->tags->getTagIds($item->id, 'com_applications.mail');
			}
		}

		return $item;
	}
	
	public function getApplication() {
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		$id = $app->input->getInt('id');  // #__application_mail id
		$post = JRequest::get('post');
		$type = "mail";

		$query = $db->getQuery(true);
		$query->select("*");
		$query->from("#__applications");
		$query->where("type = '{$type}' AND application_id = '{$id}'");

		$db->setQuery($query);
		$application = $db->loadObject();

		return $application;
	}

	public function getAdmin() {
		$db = JFactory::getDBO();
		$application = self::getApplication();
		
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from("#__users");
		$query->where("id = $application->admin_id");
		
		$db->setQuery($query);
		$admin = $db->loadObject();
		
		return $admin;
	}
}
