<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_applications
 * @since       1.6
 */
class ApplicationsModelLandnotice extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{

		parent::__construct($config);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	public function getItem($pk = null)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$query->select("*")
		->from("#__applications_notice")
		->where(" type = 'land' ");

		$db->setQuery($query);
		$item = $db->loadObject();

		return $item;
	}
}
