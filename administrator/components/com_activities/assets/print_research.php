<?php
define( '_JEXEC', 1 );

define('JPATH_BASE', '../../../../');

define( 'DS', DIRECTORY_SEPARATOR );

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

JDEBUG ? $_PROFILER->mark( 'afterLoad' ) : null;
$mainframe =& JFactory::getApplication('site');
$mainframe->initialise();

header ("Content-Type:text/html;charset=utf-8");
header ("Cache-Control: no-cache, must-revalidate");

jimport( 'joomla.factory');

require_once ( "html2doc.php" );
require_once (JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'utilities' . DS . 'utility.php');

$db =& JFactory::getDBO();

$id = JRequest::getVar( 'id', '', 'get', 'int' );

if($id){
	$query_update = $db->getQuery(true);
	$query_update->update('#__applications_research');
	$query_update->set('printState = printState + 1 ');
	$query_update->where('id = "' . $id . '"');
	
	$db->setQuery($query_update);
	if(!$db->execute()) {
		JError::raiseError( 500, $db->stderr());
	}

	
	$query = $db->getQuery(true);
	$query->select("a.*, aa.created, aa.doc_num");
	$query->from("#__applications AS aa");
	$query->leftJoin("#__applications_research AS a ON aa.uid=a.id");
	$query->where("aa.type = 'research' AND a.id = '{$id}'");
	$db->setQuery($query);
	
    $up = array('organize', 'name', 'id_num', 'address', 'tel', 'email');
    $row = JUtility::batchDecode($db->loadObject(), $up);
	$htmltodoc= new HTML_TO_DOC();

	$text = nl2br($row->tcomment);


	$activeDate = substr($row->activeDate, 0, 16);

	$deadlineDate = substr($row->deadlineDate, 0, 10);
	$str_area_o=array(
                        0=>"一般管制區",
                        1=>"史蹟保存區",
                        2=>"特別景觀區 (軍事管制區)",
                        3=>"遊憩區"
                        );
	$str_area=array(
			0=>"□一般管制區",
			1=>"□史蹟保存區",
			2=>"□特別景觀區 (軍事管制區)",
			3=>"□遊憩區"
			);
	
	if(strlen($row->research_area)>15){
		$s=explode(",",$row->research_area);
			for($i=0;$i<4;$i++){
				for($j=0;$j<4;$j++){
					if($s[$i]==$str_area_o[$j]){
						 $str_area[$j] = str_replace("□", "■", $str_area[$j]);
					}
				}
			}
	}else{
		for($i=0;$i<4;$i++){
			if($row->research_area == $str_area_o[$i]){
				$str_area[$i] = str_replace("□", "■", $str_area[$i]);
			}
		}
	}


	$str_tran_o=array(
                        0=>"無(步行)",
                        1=>"機車",
                        2=>"汽車",
                        3=>"飛行器",
                        4=>"研究船(動力)",
                        5=>"研究船(無動力)"
                        );
        $str_tran=array(
                        0=>"□無(步行)",
                        1=>"□機車",
                        2=>"□汽車",
                        3=>"□飛行器",
                        4=>"□研究船(動力)",
                        5=>"□研究船(無動力)"
                        );
	
	if(strlen($row->transportation)>7){
                $s=explode(",",$row->transportation);
			if($s!=null){
                        	for($i=0;$i<6;$i++){
                                	for($j=0;$j<6;$j++){
                                        	if($s[$i]==$str_tran_o[$j]){
                                                	 $str_tran[$j] = str_replace("□", "■", $str_tran[$j]);
                                        		 if($j==3){
                                                                 $str_tran[$j] .= "($row->npow_tra)";
                                                         }
						}
                               		}
                        	}
			}else{
				for($i=0;$i<6;$i++){
                        		if($row->transportation == $str_tran_o[$i]){
                                		$str_tran[$i] = str_replace("□", "■", $str_tran[$i]);
                        			if($i==3){
                                                                 $str_tran[$i] .= "($row->npow_tra)";
                                                }
					}
                		}
			}
        }else{
                for($i=0;$i<4;$i++){
                        if($row->transportation == $str_tran_o[$i]){
                                $str_tran[$i] = str_replace("□",  "■",  $str_tran[$i]);
                        }
                }
        }
	


	$str_gtype_o=array(
                        0=>"植物",
                        1=>"動物",
                        2=>"岩石",
                        3=>"礦物",
						4=>"人文古物",
						5=>"古生物化石",
						6=>"其它"
                        );
	
	$str_gtype=array(
                        0=>"□植物",
                        1=>"□動物",
                        2=>"□岩石",
                        3=>"□礦物",
                        4=>"□人文古物",
                        5=>"□古生物化石",
                        6=>"□其它"
                        );

        if(strlen($row->gather_type)>7){
                $s=explode(",",$row->gather_type);
                        if($s!=null){
                                for($i=0;$i<7;$i++){
                                        for($j=0;$j<7;$j++){
                                                if($s[$i]==$str_gtype_o[$j]){
                                                         $str_gtype[$j] = str_replace("□",  "■", $str_gtype[$j]);
                                                }
                                        }
                                }
                        }else{
                                for($i=0;$i<7;$i++){
                                        if($row->gather_type == $str_gtype_o[$i]){
                                                $str_gtype[$i] = str_replace("□", "■", $str_gtype[$j]);
                                        }
                                }
                        }
        }else{
                for($i=0;$i<7;$i++){
                        if($row->gather_type == $str_gtype_o[$i]){
                                $str_gtype[$i] = str_replace("□",  "■",  $str_gtype[$i]);
                        }
                }
        }

	if($str_gtype[6]=="■其它" && $row->gather_type_other!=null)
                $str_gtype[6]="$str_gtype[6]:<br />($row->gather_type_other)";


	$date=explode("～",$row->time);

	if($row->gather != 0){
                $ga_str = "是";
        }else{
                $ga_str = "否";
        }
	
	if($row->reason_title){
		$reason_str = "($row->reason_title)";
	}

	$file = '
<p align="center"><strong><span style="font-size:20.0pt">海洋國家公園學術研究暨採集證申請書</span></strong></p>
<table width="675" border="0">
  <tr>
        <td>
                <p align="right"><span style="font-size:12.0pt">申請日期：'.$row->created.'</span></p>
                <p align="right"><span style="font-size:12.0pt">申請案號：'.$row->doc_num.'</span></p>
        </td>
  </tr>
</table>
<table width="675" border="1">
  <tr>
    <td colspan="2"><div align="center">申請單位</div></td>
    <td colspan="2"><div align="center">計畫主持人暨申請人姓名</div></td>
    <td colspan="2"><div align="center">身分證字號</div></td>
  </tr>
  <tr>
    <td colspan="2">'.$row->organize.'</td>
    <td colspan="2">'.$row->name.'</td>
    <td colspan="2">'.$row->id_num.'</td>
  </tr>
  <tr>
    <td colspan="2"><div align="center">聯絡地址</div></td>
    <td colspan="2"><div align="center">聯絡電話</div></td>
    <td colspan="2"><div align="center">電子郵件</div></td>
  </tr>
  <tr>
    <td colspan="2">'.$row->address.'</td>
    <td colspan="2">'.$row->tel.'</td>
    <td colspan="2">'.$row->email.'</td>
  </tr>

  <tr>
    <td width="24" rowspan="4">
		<p align="center">申</p>
		<p align="center">請</p>
		<p align="center">事</p>
		<p align="center">由</p>
	</td>
    <td colspan="3" rowspan="4">'.$row->reason.$reason_str.'</td>
		<td width="24" rowspan="4">
		<p align="center">研</p>
		<p align="center">究</p>
		<p align="center">地</p>
		<p align="center">區</p>
	</td>
	<td width="188" height="23">'.$str_area[0].';</td>
  </tr>
  <tr>
    <td>'.$str_area[1].'</td>
  </tr>
  <tr>
    <td>'.$str_area[2].'</td>
  </tr>
  <tr>
    <td>'.$str_area[3].'</td>
  </tr>
  
  <tr>
    <td rowspan="6"><div align="center">
      <p>研</p>
      <p>究</p>
      <p>期</p>
      <p>間</p>
    </div></td>
    <td width="169" rowspan="6">自
      <p>'.$date[0].'</p>
      <p>至</p>
      <p>'.$date[1].'</p></td>
    <td width="24" rowspan="6"><div align="center">
      <p>研</p>
      <p>究</p>
      <p>使</p>
      <p>用</p>
      <p>載</p>
      <p>具</p>
    </div></td>
    <td width="206" height="32">'.$str_tran[0].'</td>
    <td rowspan="6"><div align="center">
      <p>應</p>
      <p>附</p>
      <p>附</p>
      <p>件</p>
    </div></td>
    <td rowspan="6"><p>1. 學術研究計畫書 <br />
      2. 標本採集計畫書<br />
      （若需標本採集）<br /> 3. 人員名冊（含相<br />
   片、身分證影本）</p>
</td>
  </tr>
  <tr>
    <td height="34">'.$str_tran[1].'</td>
  </tr>
  <tr>
    <td height="33">'.$str_tran[2].'</td>
  </tr>
  <tr>
    <td height="27">'.$str_tran[3].'</td>
  </tr>
  <tr>
    <td height="27">'.$str_tran[4].'</td>
  </tr>
  <tr>
    <td height="27">'.$str_tran[5].'</td>
  </tr>
  <tr>
    <td colspan="6">標本採集：'.$ga_str.'</td>
  </tr>
  <tr>
    <td rowspan="2"><div align="center">
      <p>採</p>
      <p>集</p>
      <p>種</p>
      <p>類</p>
    </div></td>
    <td rowspan="2"><p>'.$str_gtype[0].'</p>
    <p>'.$str_gtype[1].'</p>
    <p>'.$str_gtype[2].'</p>
    <p>'.$str_gtype[3].'</p>
    <p>'.$str_gtype[4].'</p>
    <p>'.$str_gtype[5].'</p>
    <p>'.$str_gtype[6].'</p>
    </td>
    <td height="130" colspan="2">採集數量</td>
    <td colspan="2">'.$row->gather_num.'</td>
  </tr>
  <tr>
    <td height="100" colspan="2">申請人簽章</td>
    <td colspan="2" align="center">線上申請免簽</td>
  </tr>
  <tr>
    <td height="51"><div align="center">附註</div></td>
    <td colspan="5"><table border="1" cellspacing="0" cellpadding="0" width="576">
      <tr>
        <td width="540" valign="top">
		<p>
			1.    持證人請確實遵守國家公園法及各項有關法令，若有違反即取消本研究許可。 <br />
			2.    若有採集騷擾野生動物者，請依野生動物保育法規定申請許可。 <br />
			3.    持證人請隨身攜帶身分證及有關證明文件。 <br />
			4.    本件可自行影印使用。
		  </p>
	</td>
      </tr>
    </table></td>
  </tr>
</table>
		';

	$htmltodoc->createDoc($file, $row->doc_num, true);
}
?>
