<?php
/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
$document->addStyleSheet('components/com_vquiz/assets/css/popup.css');
$document->addScript('components/com_vquiz/assets/js/library.js');
$document->addScript('components/com_vquiz/assets/js/popup.js');

JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');

$function = JFactory::getApplication()->input->getCmd('function', 'jSelectQuizzes');


if (version_compare(JVERSION, '3.0', '>='))
	JHtml::_('formbehavior.chosen', 'select');
$user = JFactory::getUser();
$listOrder = $this->lists['order'];
$listDirn = $this->lists['order_Dir'];
$canOrder = $user->authorise('core.edit.state', 'com_vquiz.quizmanager');
$saveOrder = $listOrder == 'ordering';

$saveOrder = ($listOrder == 'ordering' && strtolower($listDirn) == 'asc');


$quizid_from_chart = JRequest::getInt('quid');
?>
<script type="text/javascript">
	$( document ).ready(function() {
		if(<?php echo $quizid_from_chart ?>){
			jQuery('input[name="search"]').val(<?php echo $quizid_from_chart; ?>);
			document.adminForm.submit();
		}
	});

	Joomla.submitbutton = function(task) {
	 
		if (task == 'cancel') {
			Joomla.submitform(task, document.getElementById('adminForm'));
		}
				
		else if(task=='copy') {
			var form = document.adminForm;
			if (document.adminForm.boxchecked.value==0)  {
				alert("<?php echo JText::_('PLZCHOOSEQUESTION'); ?>");
				return false;
			}
			else{
				lightbox_copy();
				return false;
			}
		}
		else if(task=='move') {
			var form = document.adminForm;
			if (document.adminForm.boxchecked.value==0)  {
				alert("<?php echo JText::_('PLZCHOOSEQUESTION'); ?>");
				return false;
			}
			else{
				lightbox_move();
				return false;
			}
					
		}
		Joomla.submitform(task, document.getElementById('adminForm'));
			
	}
</script>
<?php jimport('joomla.html.pagination'); ?>
<form action="index.php?option=com_vquiz&view=quizmanager" method="post" name="adminForm" id="adminForm" onSubmit="return true">

	<div class="filter-select fltrt" style="float:right;">
		<select name="categorysearch" id="categorysearch" class="inputbox" onchange="this.form.submit()">
			<option value=""><?php echo JText::_('All Category'); ?></option>
			<?php
			for ($i = 0; $i < count($this->category); $i++) {
				?>
				<option value="<?php echo $this->category[$i]->id; ?>"  <?php if ($this->category[$i]->id == $this->lists['categorysearch']) echo 'selected="selected"'; ?> ><?php echo $this->category[$i]->quiztitle; ?></option>
				<?php
			}
			?>
		</select>
		<input type="hidden" name="qcategoryid" id="qcategoryid" value="" class="text_area" onchange="document.adminForm.submit();" />

		<select name="publish_item" id="publish_item"class="inputbox" onchange="this.form.submit()">
			<option value=""><?php echo JText::_('All State'); ?></option>
			<option value="p" <?php if ('p' == $this->lists['publish_item']) echo 'selected="selected"'; ?>><?php echo JText::_('Published'); ?></option>
			<option value="u" <?php if ('u' == $this->lists['publish_item']) echo 'selected="selected"'; ?>><?php echo JText::_('Unpublished'); ?></option>
		</select>
	</div>

	<div class="search_buttons">
        <div class="btn-wrapper input-append">
			<input placeholder="Search" type="text" name="search" id="search" value="<?php echo $this->lists['search']; ?>" class="text_area" onchange="document.adminForm.submit();" />
			<button class="btn" onclick="this.form.submit();"><i class="icon-search"></i><span class="search_text"><?php echo JText::_('SEARCH'); ?></button>
			<button class="btn" onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button></div></div>

	<div id="editcell">
		<table class="adminlist table table-striped table-hover">

			<thead>

				<tr>

					<th width="5">

						<?php echo JText::_('Num'); ?>

					</th>

					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
					</th>

					<th width="30">
						<?php echo JText::_('JSTATUS'); ?>
					</th>

					<th>
						<?php echo JHTML::_('grid.sort', 'TITLE', 'i.quizzes_title', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>

					<th><?php echo JHTML::_('grid.sort', 'CATEGORY', 'x.quiztitle', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>

					<th width="10%">

						<?php echo JHTML::_('grid.sort', 'JGRID_HEADING_ORDERING', 'ordering', @$this->lists['order_Dir'], @$this->lists['order']); ?>

						<?php if ($canOrder && $saveOrder) : ?>

							<?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'saveorder'); ?>

						<?php endif; ?>

					</th>
					<th>
						<?php echo JHTML::_('grid.sort', 'STARTPUBLISHDATE', 'i.startpublish_date', @$this->lists['order_Dir'], @$this->lists['order']); ?>

					</th>

					<th>
						<?php echo JText::_('QUESTIONS'); ?>
					</th>


<!--					<th>
						<?php echo JText::_('FEATURED'); ?>
					</th>-->

					<th>
						<?php echo JText::_('STATISTICS'); ?>
					</th>

					<th width="5">
						<?php echo JHTML::_('grid.sort', 'ID', 'i.id', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>

				</tr>

			</thead>



			<?php
			$k = 0;

			for ($i = 0, $n = count($this->items); $i < $n; $i++) {

				$row = &$this->items[$i];
				$checked = JHTML::_('grid.id', $i, $row->id);

				$published = JHTML::_('jgrid.published', $row->published, $i);
				$link = JRoute::_('index.php?option=com_vquiz&view=quizmanager&task=edit&cid[]=' . $row->id);

				$ordering = ($listOrder == 'ordering');
				$canCreate = $user->authorise('core.create', 'com_vquiz.quizmanager.' . $row->id);
				$canEdit = $user->authorise('core.edit', 'com_vquiz.quizmanager.' . $row->id);
				$canCheckin = $user->authorise('core.manage', 'com_checkin') || $row->checked_out == $user->get('id') || $row->checked_out == 0;
				$canEditOwn = true; //$user->authorise('core.edit.own',		'com_djimageslider.category.'.$item->catid) && $item->created_by == $userId;
				$canChange = $user->authorise('core.edit.state', 'com_vquiz.quizmanager.' . $row->id) && $canCheckin;
				?>

				<tr class="<?php echo "row$k"; ?>">

					<td><?php echo $this->pagination->getRowOffset($i); ?></td>
					<td>
						<?php echo $checked; ?>
					</td>

					<td align="center"><?php echo $published; ?></td>

					<td>
						<a href="<?php echo $link; ?>" onclick="if (window.parent) window.parent.<?php echo $this->escape($function); ?>('<?php echo $row->id; ?>','<?php echo $this->escape(addslashes($row->quizzes_title)); ?>',null);"><?php echo substr($row->quizzes_title, 0, 50); ?></a>
					</td>

					<td>
						<?php echo $row->categoryname; ?>
					</td>

					<td class="order" nowrap="nowrap">
						<?php if ($canChange) : ?>
							<?php if ($saveOrder) : ?>
								<?php if ($listDirn == 'asc') : ?>
									<span><?php echo $this->pagination->orderUpIcon($i, (@$item->catid == @$this->items[$i - 1]->catid), 'orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
									<span><?php echo $this->pagination->orderDownIcon($i, $n, (@$item->catid == @$this->items[$i + 1]->catid), 'orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
								<?php elseif ($listDirn == 'desc') : ?>
									<span><?php echo $this->pagination->orderUpIcon($i, (@$item->catid == @$this->items[$i - 1]->catid), 'quizmanager.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
									<span><?php echo $this->pagination->orderDownIcon($i, $n, (@$item->catid == @$this->items[$i + 1]->catid), 'quizmanager.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
								<?php endif; ?>
							<?php endif; ?>
							<?php $disabled = $saveOrder ? '' : 'disabled="disabled"'; ?>

							<input  style="width:20px;" type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $disabled ?> class="text-area-order input-mini" />

						<?php else : ?>

							<?php echo $row->ordering; ?>

						<?php endif; ?>

					</td>
					<td>
						<?php echo $row->startpublish_date; ?>
					</td>
					<?php /* ?>            <td>
					  <?php echo $row->endpublish_date; ?>
					  </td> <?php */ ?>
					<td>
						<a href="index.php?option=com_vquiz&view=quizquestion&quizzesid=<?php echo $row->id; ?>">
							<input class="btn btn-small" type="button" value="<?php echo JText::_('View') ?>(<?php echo $row->totalquestion; ?>)" />
						</a>
					</td>


<!--					<td>
						<?php if ($row->featured == 1) { ?>
							<a class="btn btn-micro active hasTooltip" title="Unfeatured Item" onclick="return listItemTask('cb<?php echo $i; ?>','unfeatured')" href="javascript:void(0);" data-original-title="featured Item">
								<i class="icon-featured"></i>
							</a>

						<?php } else { ?>
							<a class="btn btn-micro active hasTooltip" title="Featured Item" onclick="return listItemTask('cb<?php echo $i; ?>','featured')" href="javascript:void(0);" data-original-title="unfeatured Item">
								<i class="icon-unfeatured"></i>
							</a>
						<?php } ?>
					</td>-->

					<td>

						<a class="modal" id="modal" title="Select" href="<?php echo 'index.php?option=com_vquiz&view=quizmanager&layout=chartview&tmpl=component&chart=1&quizzesid=' . $row->id; ?>" rel="{handler: 'iframe', size: {x: 800, y: 500}}"><input class="btn btn-small" type="button" value="<?php echo JText::_('View Chart') ?>" /></a>

					</td>

					<td>
						<?php echo $row->id; ?>
					</td>

				</tr>

				<?php
				$k = 1 - $k;
			}
			?>
			<tfoot>
				<tr>
					<td colspan="11"><?php echo $this->pagination->getListFooter(); ?></td>
				</tr>
			</tfoot>
		</table>




	</div>

	<input type="hidden" name="option" value="com_vquiz" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="view" value="quizmanager" />
	<input type="hidden" name="tmpl" value="<?php echo JRequest::getVar('tmpl', 'index'); ?>" />
	<input type="hidden" name="function" value="<?php echo JRequest::getVar('function', ''); ?>" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />



	<div id="light_move">
		<a href="javascript:void(0);"  onclick="lightbox_close2();" class="closepop">X</a>
		<div style="text-align:center;">
			<p><?php echo JText::_('SELECTCATEGORY'); ?></p>
			<p><select name="quizcategoryidmove" >
					<option value=""><?php echo JText::_('---Select---'); ?></option>
					<?php
					for ($i = 0; $i < count($this->category); $i++) {
						?>
						<option value="<?php echo $this->category[$i]->id; ?>"  <?php if ($this->category[$i]->id == !empty($this->item->quiz_categoryid) ? $this->item->quiz_categoryid : 0) echo 'selected="selected"'; ?> ><?php echo $this->category[$i]->quiztitle; ?></option>
						<?php
					}
					?>
                </select>
			</p>
			<p> <input type="button" value="Move" name="move"  class="btn btn-small" onclick="Joomla.submitform('movequestion');"></p>
		</div>
	</div>

	<div id="light_copy">
		<a href="javascript:void(0);"  onclick="lightbox_close2();" class="closepop">X</a>
		<div style="text-align:center;">
			<p><?php echo JText::_('SELECTCATEGORY'); ?></p>
			<p><select name="quizcategoryidcopy">
					<option value=""><?php echo JText::_('---Select---'); ?></option>
					<?php
					for ($i = 0; $i < count($this->category); $i++) {
						?>
						<option value="<?php echo $this->category[$i]->id; ?>"  <?php if ($this->category[$i]->id == !empty($this->item->quiz_categoryid) ? $this->item->quiz_categoryid : 0) echo 'selected="selected"'; ?> ><?php echo $this->category[$i]->quiztitle; ?></option>
						<?php
					}
					?>
                </select>
            </p>
            <p> <input type="button" value="Copy" name="copy" class="btn btn-small"  onclick="Joomla.submitform('copyquestion');"></p>
		</div>
	</div>


	<div id="fade" onClick="lightbox_close2();"></div>


</form>



