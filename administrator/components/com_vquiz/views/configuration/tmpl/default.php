<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
$document = JFactory::getDocument();
$document->addScript('components/com_vquiz/assets/js/library.js');
$document->addScript('components/com_vquiz/assets/js/jquery-ui.js');
$document->addStyleSheet('components/com_vquiz/assets/css/jquery-ui.css');
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
$editor = JFactory::getEditor();
?>
<style type="text/css">
.radio_certificate
{
	float:left;
	margin: 0 10px
}
</style>
<script>	
		$(function() {
		$( "#tabs" ).tabs();
		});
</script>
<script> 
$(document).ready(function(){ 


			$("#mflip").css("background-color","#999999");
			$("#mpanel").css("background-color","#999999");
			$("#mpanel").slideDown("slow");
			
			$("#tflip").css("background-color","#999999");
			$("#tpanel").css("background-color","#999999");
			$("#tpanel").slideDown("slow");
			

			$("#tflip").click(function(){
			$("#tflip").css("background-color","#999999");
			$("#tpanel").css("background-color","#999999");
			$("#tflip2").css("background-color","#cccccc");	
			$("#tpanel").slideToggle("slow");
			$("#tpanel2").slideUp("slow");
			});
			$("#mflip").click(function(){
			$("#mflip").css("background-color","#999999");
			$("#mpanel").css("background-color","#999999");
			$("#mflip2").css("background-color","#cccccc");	
			$("#mpanel").slideToggle("slow");
			$("#mpanel2").slideUp("slow");
			});

			$("#tflip2").click(function(){
			$("#tflip2").css("background-color","#999999");
			$("#tpanel2").css("background-color","#999999");
			$("#tflip").css("background-color","#cccccc");
			$("#tpanel2").slideToggle("slow");
			$("#tpanel").slideUp("slow");
			});
			$("#mflip2").click(function(){
			$("#mflip2").css("background-color","#999999");
			$("#mpanel2").css("background-color","#999999");
			$("#mflip").css("background-color","#cccccc");
			$("#mpanel2").slideToggle("slow");
			$("#mpanel").slideUp("slow");
			});



			  if($('input[name="get_certificate"]').val()==0){
				  $('.ckeck_downlaod_certificate').hide();
			  }
			  
			 $('input[name="get_certificate"]').on('click',function(){
				  var get_certificate=jQuery(this).val();
 				  if(get_certificate==0)
					   $('.ckeck_downlaod_certificate').hide("slow");
				  else
				 	  $('.ckeck_downlaod_certificate').show("slow");
 			  });
});

</script>

<form action="index.php?option=com_vquiz&view=configuration" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">

<div class="col101">

        <div id="tabs">
        <ul>
        <li><a href="#emailtext"><?php echo JText::_('QUIZMAIL'); ?></a></li>     
        <li><a href="#quiztext"><?php echo JText::_('RESULTTEMPLATE'); ?></a></li>   
        <li><a href="#addtional"><?php echo JText::_('ADDTIONAL'); ?></a></li>   
        </ul>



        <div  id="quiztext">    
      
        <fieldset class="adminform">   
        <legend><?php echo JText::_( 'QUIZ_RESULT_TEMPLATE'); ?></legend> 
        
        <div class="conf_left_panel">
        
                <div class="conf_left_panel_buttons">
                     <div id="tflip"><?php echo JText::_('SINGLE_SCORE_FOR_ALL_OPTIONS')?></div>
                     <div id="tflip2"><?php echo JText::_('DIFFERENT_SCORE_FOR_DIFFERENT_OPTIONS')?></div>
                </div>
        
                <div class="conf_left_panel_options">
                
                    <div  id="tpanel">
                    <h4 style="text-align:center"><?php echo JText::_('SINGLE_SCORE_FOR_ALL_OPTIONS')?></h4>
                    <?php 
                 
                    echo $editor->display("textformat",  $this->item->textformat, "400", "400", "20", "5", 1, null, null, null, array('mode' => 'simple'));
                    ?>
                    </div> 
                
                
                
                    <div  id="tpanel2">
                    <h4><?php echo JText::_('DIFFERENT_SCORE_FOR_DIFFERENT_OPTIONS')?></h4>
                    <?php 
                
                    echo $editor->display("textformat2",  $this->item->textformat2, "400", "400", "20", "5", 1, null, null, null, array('mode' => 'simple'));
                    ?>
                    </div>
                </div>
             </div>   

        
        
        
            <div class="conf_right_panel">
            
                <div class="conf-para">
                <h3><?php echo JText::_('PAIRAMETER')?></h3>
                <div>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{username}', 'params_Value');return false;" href="javascript:void(0);">{username}</a>
            <br>
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{spenttime}', 'params_Value');return false;" href="javascript:void(0);">{spenttime}</a>
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{quizname}', 'params_Value');return false;" href="javascript:void(0);">{quizname}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{startdate}', 'params_Value');return false;" href="javascript:void(0);">{starttime}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{enddate}', 'params_Value');return false;" href="javascript:void(0);">{endtime}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{maxscore}', 'params_Value');return false;" href="javascript:void(0);">{maxscore}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{userscore}', 'params_Value');return false;" href="javascript:void(0);">{userscore}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{percentscore}', 'params_Value');return false;" href="javascript:void(0);">{percentscore}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{passedscore}', 'params_Value');return false;" href="javascript:void(0);">{passedscore}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{passed}', 'params_Value');return false;" href="javascript:void(0);">{passed}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{email}', 'params_Value');return false;" href="javascript:void(0);">{email}</a>
            
            <br>
            
            <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{resultslink}', 'params_Value');return false;" href="javascript:void(0);">{flag}</a>
            
            
            
            </div>
            </div>
            </div>
        
         </fieldset>
</div>


            



           <div id="emailtext">

               <fieldset class="adminform"> 

                        <legend><?php echo JText::_( 'SEND_MAIL_TEMPLATE' ); ?></legend> 

                    <table>

                    <tr>

                    <td width="100"><label><?php echo JText::_( 'SB' ); ?></label></td>

                    <td><input type="text" name="subject" id="subject" value="<?php echo $this->item->subject;?>" /></td>

                    </tr></table>

<div class="conf_left_panel">

<div class="conf_left_panel_buttons">                        

<div id="mflip"><?php echo JText::_('SINGLE_SCORE_FOR_ALL_OPTIONS')?></div>

<div id="mflip2"><?php echo JText::_('DIFFERENT_SCORE_FOR_DIFFERENT_OPTIONS')?></div>

</div>

<div class="conf_left_panel_options">

<div  id="mpanel">

<h4><?php echo JText::_('SINGLE_SCORE_FOR_ALL_OPTIONS')?></h4>

<?php 
echo $editor->display("mailformat",  $this->item->mailformat, "400", "400", "20", "5", 1, null, null, null, array('mode' => 'simple'));

?>

</div> 



<div  id="mpanel2">

<h4><?php echo JText::_('DIFFERENT_SCORE_FOR_DIFFERENT_OPTIONS')?></h4>

<?php 

 

echo $editor->display("mailformat2",  $this->item->mailformat2, "400", "400", "20", "5", 1, null, null, null, array('mode' => 'simple'));

?>

</div>

</div>

</div>

<div class="conf_right_panel"> 

                    <div class="conf-para">

                    <h3>Parameters</h3>

                    <div>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{username}', 'params_Value');return false;" href="javascript:void(0);">{username}</a>

                   

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{spenttime}', 'params_Value');return false;" href="javascript:void(0);">{spenttime}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{quizname}', 'params_Value');return false;" href="javascript:void(0);">{quizname}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{startdate}', 'params_Value');return false;" href="javascript:void(0);">{starttime}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{enddate}', 'params_Value');return false;" href="javascript:void(0);">{endtime}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{maxscore}', 'params_Value');return false;" href="javascript:void(0);">{maxscore}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{userscore}', 'params_Value');return false;" href="javascript:void(0);">{userscore}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{percentscore}', 'params_Value');return false;" href="javascript:void(0);">{percentscore}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{passedscore}', 'params_Value');return false;" href="javascript:void(0);">{passedscore}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{passed}', 'params_Value');return false;" href="javascript:void(0);">{passed}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{email}', 'params_Value');return false;" href="javascript:void(0);">{email}</a>

                    <br>

                    <a class="hasTip" title="" onclick="if (typeof(jInsertEditorText) != 'undefined') jInsertEditorText('{resultslink}', 'params_Value');return false;" href="javascript:void(0);">{flag}</a>

          

                    </div>

                  </div>

 </div>                 

                   </fieldset>



            </div> 

            <div id="addtional">
              <table class="adminform table table-striped">
              

     <tr>
        <td><label class="hasTip" title="<?php echo JText::_('TAKE_SNAPSHOT_TOLTIP');?>"><?php echo JText::_('TAKE_SNAPSHOT');?></label></td>
        <td>
            <fieldset class="radio btn-group">
            <label for="take_snapshot1" id="take_snapshot1-lbl" class="radio"><?php echo JText::_( 'YS' ); ?></label>
            <input type="radio" name="take_snapshot" id="take_snapshot1" value="1" <?php if($this->item->take_snapshot==1) echo 'checked="checked"';?>/>
            <label for="take_snapshot0" id="take_snapshot0-lbl" class="radio"><?php echo JText::_( 'NOS' ); ?></label>
            <input type="radio" name="take_snapshot" id="take_snapshot0" value="0" <?php if($this->item->take_snapshot==0) echo 'checked="checked"';?>/>
            </fieldset>
        </td>
    </tr>
     <tr>
        <td><label class="hasTip" title="<?php echo JText::_('RESULTS_PREVIEW_TOLTIP');?>"><?php echo JText::_('RESULTS_PREVIEW');?></label></td>
        <td>
            <fieldset class="radio btn-group">
            <label for="results_preview1" id="results_preview1-lbl" class="radio"><?php echo JText::_( 'YS' ); ?></label>
            <input type="radio" name="results_preview" id="results_preview1" value="1" <?php if($this->item->results_preview==1) echo 'checked="checked"';?>/>
            <label for="results_preview0" id="results_preview0-lbl" class="radio"><?php echo JText::_( 'NOS' ); ?></label>
            <input type="radio" name="results_preview" id="results_preview0" value="0" <?php if($this->item->results_preview==0) echo 'checked="checked"';?>/>
            </fieldset>
        </td>
    </tr>
         <tr>
        <td><label class="hasTip" title="<?php echo JText::_('GET_CERTIFICATE_TOLTIP');?>"><?php echo JText::_('GET_CERTIFICATE');?></label></td>
        <td>
            <fieldset class="radio btn-group">
            <label for="get_certificate1" id="get_certificate1-lbl" class="radio"><?php echo JText::_( 'YS' ); ?></label>
            <input type="radio" name="get_certificate" id="get_certificate1" value="1" <?php if($this->item->get_certificate==1) echo 'checked="checked"';?>/>
            <label for="get_certificate0" id="get_certificate0-lbl" class="radio"><?php echo JText::_( 'NOS' ); ?></label>
            <input type="radio" name="get_certificate" id="get_certificate0" value="0" <?php if($this->item->get_certificate==0) echo 'checked="checked"';?>/>
            </fieldset>
        </td>
    </tr>
    
 
       <tr class="ckeck_downlaod_certificate">
        <td><label class="hasTip" title="<?php echo JText::_('DOWNLOAD_CERTIFICATE_TOLTIOP');?>"><?php echo JText::_('DOWNLOAD_CERTIFICATE');?></label></td>
        <td>
            <fieldset class="radio btn-group">
            <label for="download_certificate1" id="download_certificate1-lbl" class="radio"><?php echo JText::_( 'YS' ); ?></label>
            <input type="radio" name="download_certificate" id="download_certificate1" value="1" <?php if($this->item->download_certificate==1) echo 'checked="checked"';?>/>
            <label for="download_certificate0" id="download_certificate0-lbl" class="radio"><?php echo JText::_( 'NOS' ); ?></label>
            <input type="radio" name="download_certificate" id="download_certificate0" value="0" <?php if($this->item->download_certificate==0) echo 'checked="checked"';?>/>
            </fieldset>
        </td>
    </tr>
     <tr>
        <td><label class="hasTip" title="<?php echo JText::_('SHARE_BUTTON_TOLTIP');?>"><?php echo JText::_('SHARE_BUTTON');?></label></td>
        <td>
            <fieldset class="radio btn-group">
            <label for="share_button1" id="share_button1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
            <input type="radio" name="share_button" id="share_button1" value="1" <?php if($this->item->share_button==1) echo 'checked="checked"';?>/>
            <label for="share_button0" id="share_button0-lbl" class="radio"><?php echo JText::_( 'NOS' ); ?></label>
            <input type="radio" name="share_button" id="share_button0" value="0" <?php if($this->item->share_button==0) echo 'checked="checked"';?>/>
            </fieldset>
        </td>
    </tr>
<?php /*?>     <tr class="ckeck_downlaod_certificate">
        <td><label class="hasTip" title="<?php echo JText::_('SEND_CERTIFICATE_TOLTIP');?>"><?php echo JText::_('SEND_CERTIFICATE');?></label></td>
        <td>
  
            <label class="radio_certificate"><?php echo JText::_('USERS_MANUALY'); ?>
            <input type="radio" name="send_certificate" value="0" <?php if($this->item->send_certificate==0) echo 'checked="checked"';?>/>
            </label>
            <label class="radio_certificate"><?php echo JText::_('AUTOMATICALLY'); ?> 
            <input type="radio" name="send_certificate"  value="1" <?php if($this->item->send_certificate==1) echo 'checked="checked"';?>/>
            </label>
             <label class="radio_certificate"><?php echo JText::_('NONES'); ?>
            <input type="radio" name="send_certificate" value="2" <?php if($this->item->send_certificate==2) echo 'checked="checked"';?>/>
			</label>
        </td>
    </tr><?php */?>
 
              <tr>

             <td width="250" title="<?php echo JText::_('CATEGORY_IMG_DESC') ?>"><?php echo JText::_('CATEGORY_IMG') ?></td>

             <td>
             <input type="text" title="<?php echo JText::_('WIDTH_ONLY_NUMEIRC') ?>" name="categorythumbnailwidth"  value="<?php echo $this->item->categorythumbnailwidth; ?>" /> 
             <input type="text" title="<?php echo JText::_('HEIGHT_ONLY_NUMEIRC') ?>" name="categorythumbnailheight"  value="<?php echo $this->item->categorythumbnailheight; ?>" /></td>

             </tr>

              <tr>

             <td><?php echo JText::_('ENTER_DATE') ?></td>

             <td><input type="text" name="dateformate"  value="<?php echo $this->item->dateformate; ?>" /></td>

             </tr>

               <tr>

             <td></td>

             <td><a href="http://php.net/manual/en/datetime.formats.date.php" target="_blank"><?php echo JText::_('RF_LINK') ?></a></td>

             </tr>

             </table>

            </div>

 



</div>

</div>

<div class="clr"></div>



<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="com_vquiz" />
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="view" value="configuration" />
</form>







