<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access
defined('_JEXEC') or die('Restricted access');

class VquizController extends JControllerLegacy {

	function display($cachable = false, $urlparams = false) {
		$this->showToolbar();
		parent::display();
	}

	function showToolbar() {
		$view = JRequest::getVar('view', 'quizmanager');


//		JSubMenuHelper::addEntry('<span class="vquiz" title="' . JText::_('DASHBOARD') . '">' . JText::_('DASHBOARD') . '</span>', 'index.php?option=com_vquiz&view=vquiz', $view == 'vquiz');

		JSubMenuHelper::addEntry('<span class="quizcategory" title="' . JText::_('QUIZCATEGORY') . '">' . JText::_('QUIZCATEGORY') . '</span>', 'index.php?option=com_vquiz&view=quizcategory', $view == 'quizcategory');

		JSubMenuHelper::addEntry('<span class="quizcategory" title="' . JText::_('RESULTCATS') . '">' . JText::_('RESULTCATS') . '</span>', 'index.php?option=com_vquiz&view=resultcats', $view == 'resultcats');

		JSubMenuHelper::addEntry('<span class="quizmanager" title="' . JText::_('QUIZMANAGER') . '">' . JText::_('QUIZMANAGER') . '</span>', 'index.php?option=com_vquiz&view=quizmanager', $view == 'quizmanager');

//		JSubMenuHelper::addEntry('<span class="quizresult" title="' . JText::_('QUIZRESULT') . '">' . JText::_('QUIZRESULT') . '</span>', 'index.php?option=com_vquiz&view=quizresult', $view == 'quizresult');

//		JSubMenuHelper::addEntry('<span class="users" title="' . JText::_('USERS') . '">' . JText::_('USERS') . '</span>', 'index.php?option=com_vquiz&view=users', $view == 'users');
		
//		JSubMenuHelper::addEntry('<span class="configuration" title="' . JText::_('CONFIGURATION') . '">' . JText::_('CONFIGURATION') . '</span>', 'index.php?option=com_vquiz&view=configuration', $view == 'configuration');
	}

}