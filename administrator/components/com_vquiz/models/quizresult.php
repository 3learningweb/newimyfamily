<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/ 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.application.component.modellist');
 
class VquizModelQuizresult extends JModelList
 
{

		 function __construct()
			{
				parent::__construct();
		
				 
				 $mainframe = JFactory::getApplication();
				$context	= 'com_vquiz.result.list.';
				// Get pagination request variables
				$limit = $mainframe->getUserStateFromRequest($context.'limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
				$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0, 'int' );
				
				// In case limit has been changed, adjust it
				$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
				
				
				$this->setState('limit', $limit);
				$this->setState('limitstart', $limitstart);
		
				 
				 
				$array = JRequest::getVar('cid',  0, '', 'array');
				$this->setId((int)$array[0]);
			}

			function _buildQuery()
			{
			 $db =JFactory::getDBO();
			 $query="SELECT i.*, u.username as username FROM #__vquiz_quizresult as i LEFT JOIN #__users as u ON i.userid = u.id";
			 return $query;
			}

		function setId($id)	
		{
		$this->_id		= $id;
 		$this->_data	= null;
 		}

		 function &getItem()
		 {
		if (empty( $this->_data )) {
		$query='SELECT i.*, u.username as username FROM #__vquiz_quizresult as i LEFT JOIN #__users as u ON i.userid = u.id WHERE i.id = '.$this->_id;	
		$this->_db->setQuery( $query );
		$this->_data = $this->_db->loadObject();
		}



		if (!$this->_data) {
 		$this->_data = new stdClass();
  		$this->_data->id = 0;
 		$this->_data->quiztitle = null;
 		$this->_data->userid = null;
 		$this->_data->startdatetime = null;
 		$this->_data->enddatetime = null;
 		$this->_data->score = null;
 		$this->_data->passed_score = null;
 		$this->_data->maxscore = null;
 		$this->_data->quiz_spentdtime = null;
 
		}
 		return $this->_data;
 	}




		 function &getItems()
			
		{
			// Lets load the data if it doesn't already exist
			if (empty( $this->_data ))
			{
				 $query = $this->_buildQuery();
				 
				 $filter = $this->_buildContentFilter();
				 $orderby = $this->_buildItemOrderBy();
				 
				 $query .= $filter;
				 $query .= $orderby;
				 //$this->_data = $this->_getList( $query );
				 $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
			}
	
			return $this->_data;
		}
	 


	
		function getTotal()
		{
				
			if (empty($this->_total)) {
			$query = $this->_buildQuery();
			$query .= $this->_buildContentFilter();
			$this->_total = $this->_getListCount($query);    
			}
			return $this->_total;
 		}



		function _buildItemOrderBy()
			{
				$mainframe = JFactory::getApplication();
				$context	= 'com_vquiz.result.list.';
				$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order', 'filter_order', 'id', 'cmd' );
				$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir', 'filter_order_Dir', 'desc', 'word' );
				$orderby = ' group by i.id order by '.$filter_order.' '.$filter_order_Dir . ' ';
				return $orderby;
			}



			function getPagination()
			{
				// Load the content if it doesn't already exist
				if (empty($this->_pagination)) {
					jimport('joomla.html.pagination');
					$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
				}
				return $this->_pagination;
			}
 
 
 
		  function _buildContentFilter()
			 
			{
				$mainframe =JFactory::getApplication();
		 
				$context	= 'com_vquiz.result.list.';
				$search		= $mainframe->getUserStateFromRequest( $context.'search', 'search',	'',	'string' );
				
				$startdatesearch = $mainframe->getUserStateFromRequest( $context.'startdatesearch', 'startdatesearch',	'',	'string' );
				$enddatesearch = $mainframe->getUserStateFromRequest( $context.'enddatesearch', 'enddatesearch',	'',	'string' );
				
				$search		= JString::strtolower( $search );
		 
				$where = array();
				
				
				if($startdatesearch or $enddatesearch)
				{
					  $where[] = 'i.start_datetime >='.$this->_db->Quote( $this->_db->escape( $startdatesearch, true ), false );
					  if($enddatesearch)
					  $where[] = 'i.start_datetime <='.$this->_db->Quote( $this->_db->escape( $enddatesearch, true ), false );
					
				}
				
				 
				if($search)
				{	
					if (is_numeric($search)) 
					{		 
						$where[] = 'LOWER(  i.id ) ='.$this->_db->Quote( $this->_db->escape( $search, true ), false );
					}
					else
					{
					 $where[] = 'i.quiztitle LIKE '.$this->_db->Quote( '%'.$this->_db->escape( $search, true ).'%', false );
					}
				}
				 //$where[] = 'i.userid=0';
				
					 $filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';
					
		
		 
				return $filter;
			}
		
 
	function store()
 	{	
 		 $time = time();
 		$row =& $this->getTable();
 		$data = JRequest::get( 'post' );
				
 		if (!$row->bind($data)) {
 		$this->setError($this->_db->getErrorMsg());
 		return false;
 		}

  		if (!$row->check()) {
 		$this->setError($this->_db->getErrorMsg());
 		return false;
 		}
 
		if (!$row->store()) {
 		$this->setError( $row->getErrorMsg() );
 		return false; 
 		}
 		return true;



	}
 

	function delete()
	{
 		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
 		$row =& $this->getTable();
 		if (count( $cids )) {
 		foreach($cids as $cid) {
 			if (!$row->delete( $cid )) {
 				$this->setError( $row->getErrorMsg() );
 				return false;
 				}
 			}
 		}
 		return true;
 	}

			 
			 function getShowresult()
			
				{
					
					$user = JFactory::getUser();
					
				    $query = ' SELECT * FROM #__vquiz_quizresult  WHERE id = '.$this->_id.' order by id desc';

					$this->_db->setQuery( $query );
					$result = $this->_db->loadObject();

					$quiz_questions=$result->quiz_questions;
					$quiz_answers=$result->quiz_answers;
					$optiontypescore=$result->optiontypescore;
					
					$question=json_decode($quiz_questions);
					$answers=json_decode($quiz_answers);

								
					$qusetion_item = new stdClass();
					
					for($j=0;$j<count($question);$j++){
						
						//$query = 'select * from #__vquiz_question where id IN ('.implode(',',$question).')  order by id asc';
						$query = 'select qtitle from #__vquiz_question where id = '.$this->_db->quote($question[$j]);
						$this->_db->setQuery( $query );
						$qusetion_item->question[$j] = $this->_db->loadObject();
			
						//for($i=0;$i<count($qusetion_item->question);$i++){
						//$query = 'select * from #__vquiz_option where qid = '.$this->_db->quote($qusetion_item->question[$i]->id).' order by id asc';
						$query = 'select * from #__vquiz_option where qid = '.$this->_db->quote($question[$j]).' order by id asc';
						$this->_db->setQuery( $query );
						$qusetion_item->options[$j] = $this->_db->loadObjectList(); 
						//}
					}
					
					//$qusetion_item->givenanswer = $imploded;
					$qusetion_item->givenanswer =$answers;
					$qusetion_item->question_array = $question;
					$qusetion_item->optiontypescore = $optiontypescore;	
 
					return $qusetion_item;
			} 

			 
 
 
 		function getCsv()
				{
					$db = JFactory::getDbo();		
					//get the column titles for heading row
					$query = 'show columns from #__vquiz_quizresult';
					$db->setQuery( $query );
					$columnhead = $db->loadColumn();
					
					//get all the fields to export for the particular profile
					$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
			  	    $query = 'select * from #__vquiz_quizresult WHERE id IN ('.implode(',',$cids).')';
					$db->setQuery( $query );
					$fields = (array)$db->loadObjectList();
					
					$data = $db->loadRowList();
					
					//push the heading row at the top
					array_unshift($data, $columnhead);
					
					// output headers so that the file is downloaded rather than displayed
					header('Content-Type: text/csv; charset=utf-8');
					header('Content-Disposition: attachment; filename=export.csv');
					
					// create a file pointer connected to the output stream
					$output = fopen('php://output', 'w');
					
					foreach ($data as $fields) {
						$f=array();
						foreach($fields as $v)
							array_push($f, mb_convert_encoding($v, 'UTF-16LE', 'utf-8'));
						fputcsv($output, $f, ',', '"');
					}
					fclose($output);
					return true;
				}
				
}