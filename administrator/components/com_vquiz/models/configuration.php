<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined('_JEXEC') or die();

jimport('joomla.application.component.modellist');

class VquizModelConfiguration extends JModelList
{
    
	function __construct()
	{
		parent::__construct();
        $mainframe = JFactory::getApplication();
		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);

	}

 	function _buildQuery()
	{
	     $db =JFactory::getDBO();
		 
		 $query = ' SELECT * FROM #__vquiz_configuration ';
		   
		 return $query;
	}
 
 
 
	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a wproperty
	 * @return object with data
	 */
	function &getItem()
	{
		// Load the data
		if (empty( $this->_data )) {
			$query = ' SELECT * FROM #__vquiz_configuration';
					
			$this->_db->setQuery( $query );
			$this->_data = $this->_db->loadObject();
		}
		if (!$this->_data) {
			$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->subject= null;
			$this->_data->sendermail= null;
			$this->_data->name= null;
			$this->_data->textformat= null;
			$this->_data->textformat2= null;
			$this->_data->mailformat= null;
		    $this->_data->mailformat2= null;
		    $this->_data->download_certificate= null;
 			$this->_data->take_snapshot= null;
 		    $this->_data->results_preview= null;
			$this->_data->send_certificate= null;
			$this->_data->categorythumbnailwidth= null;
			$this->_data->categorythumbnailheight= null;
			$this->_data->share_button= null;

		} 
		return $this->_data;
	}

     
	 function &getItems()
	 	
	 {
		// Lets load the data if it doesn't already exist
		if (empty( $this->_data ))
		{
			 $query = $this->_buildQuery();
			 $this->_data = $this->_getList($query);
		}

		return $this->_data;
	}

 	function getTotal()
  	{
        // Load the content if it doesn't already exist
        if (empty($this->_total)) {
            $query = $this->_buildQuery();
            $this->_total = $this->_getListCount($query);    
        }
        return $this->_total;
  	}

	/**
	 * Method to store a record
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
  
	 
	function store()
	{	
	
	     $time = time();
		
	    $row =& $this->getTable();

		$data = JRequest::get( 'post' );
		 $data['mailformat'] = JRequest::getVar('mailformat', '', 'post', 'string', JREQUEST_ALLOWRAW);
		 $data['mailformat2'] = JRequest::getVar('mailformat2', '', 'post', 'string', JREQUEST_ALLOWRAW);
	     $data['textformat'] = JRequest::getVar('textformat', '', 'post', 'string', JREQUEST_ALLOWRAW);
		 $data['textformat2'] = JRequest::getVar('textformat2', '', 'post', 'string', JREQUEST_ALLOWRAW);
		  
		// Bind the form fields to the wproperty table
		if (!$row->bind($data)) {
		 
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Make sure the wproperty record is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Store the web link table to the database
		if (!$row->store()) {
			$this->setError( $row->getErrorMsg() );
			return false; 
		}
 

		return true;
	}

	/**
	 * Method to delete record(s)
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$row =& $this->getTable();

		if (count( $cids )) {
			foreach($cids as $cid) {
				if (!$row->delete( $cid )) {
					$this->setError( $row->getErrorMsg() );
					return false;
				}
			}
		}
		return true;
	}
	
 
	
	 
	
 

}