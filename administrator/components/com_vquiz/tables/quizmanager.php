<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class TableQuizmanager extends JTable
{

	var $id = null;
	var $quizzes_title = null;
	var $quiz_categoryid = null;
	var $description = null;
	var $published = null;
 
	function __construct(& $db) {
 
		parent::__construct('#__vquiz_quizzes', 'id', $db);
 
	}
	
	
		function store($updateNulls = false)
		{
	
			if(!parent::store($updateNulls))	{
				return false;
			}
	 
			$session=JFactory::getSession();
		    $this->id;
  
 
			if($session->has('message_text')){
 
				$message=$session->get('message_text');
				
				$insert = new stdClass();
				$insert->quizid =$this->id;
				$insert->text1 = $message['text1'];
				$insert->text2 = $message['text2'];
				$insert->text3 = $message['text3'];
				$insert->text4 = $message['text4'];
				$insert->text5 = $message['text5'];
				$insert->upto1	= $message['upto1'];
				$insert->upto2	= $message['upto2'];
				$insert->upto3	= $message['upto3'];
				$insert->upto4	= $message['upto4'];
				$insert->upto5	= $message['upto5'];
				try{		
 
				$this->_db->insertObject('#__vquiz_score_message', $insert, 'id');
				$session->clear('message_text');
					
								
				}
				catch (Exception $e)
				{
					$this->setError($e->getMessage());
		
					return false;
				}
	 
		}	 
	 
			return true;
		
		}
	
 



}