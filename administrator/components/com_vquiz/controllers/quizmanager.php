<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
class VquizControllerQuizmanager extends VquizController
{

	function __construct()
	{
		parent::__construct();
		
		$this->registerTask( 'add'  , 	'edit' );
		$this->registerTask( 'unpublish',	'publish' );
		$this->registerTask( 'orderup', 		'reorder' );
		$this->registerTask( 'orderdown', 		'reorder' );
		$this->registerTask( 'unfeatured','featured' );	
	
	}
	
	function edit()
	{
		JRequest::setVar( 'view', 'quizmanager' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		
		parent::display();
	}
	
	
 	function publish()
	{
		$model = $this->getModel('quizmanager');
		$msg = $model->publish();
			

		$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
	}

		function featured()
	{
 
		
		$model = $this->getModel('quizmanager');
		$msg = $model->featured();
		
		$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
		
	}
			
	function reorder()
	{ 
	
		$model = $this->getModel('quizmanager');
		$msg = $model->reorder();

		$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
	
	}
	
	function drawChart()
	{ 
		JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );
		
		$model = $this->getModel('quizmanager');
		$obj = $model->drawChart();	
		jexit(json_encode($obj));
	} 
	
 
	
 
	function saveOrder()
	{
	
		$model = $this->getModel('quizmanager');
		$msg = $model->saveOrder();

	$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
	
	}
	 
	function save()
	{
		  $model = $this->getModel('quizmanager');
		if($model->store()) {
			$msg = JText::_( 'Quizzes Save Success' );
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
		} else {
			jerror::raiseWarning('', $model->getError());
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager');
		}
	
	}
	
	
	
	function apply()
	{
		$model = $this->getModel('quizmanager');
		if($model->store()) {
			$msg = JText::_( 'Quizzes Save Success' );
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager&task=edit&cid[]='.JRequest::getInt('id', 0), $msg );
		} else {
			jerror::raiseWarning('', $model->getError());
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager&task=edit&cid[]='.JRequest::getInt('id', 0) );
		}

	}

 
	
	function remove()
	{
		$model = $this->getModel('quizmanager');
		
		if(!$model->delete()) 
		{
			$msg = JText::_( 'Error: One or More Greetings Could not be Deleted' );
		} 
		else 
		{
			$msg = JText::_( 'Quizzes(s) Deleted' );
		}
		
		$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
	}
	
	
	function cancel()
	{
			$msg = JText::_( 'Operation Cancelled' );
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
	}
	
	
	function copyquestion()
	{
		$model = $this->getModel('quizmanager');
			if(!$model->copyquestion()) 
			$msg = JText::_( 'Error: One or More Greetings Could not be Copy' );
			else 	 
			$msg = JText::_( 'Copy Succesfully' );
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
	}
			
							
  function movequestion()
	{
		$model = $this->getModel('quizmanager');
			if(!$model->movequestion()) 
			$msg = JText::_( 'Error: One or More Greetings Could not be Copy' );
			else 	 
			$msg = JText::_( 'Move Succesfully' );
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager', $msg );
	}
	
 
 function savemessage()
	{ 
		$model = $this->getModel('quizmanager');
		if($model->messagestore()) {
			$quizid=JRequest::getInt('id');
			$msg = JText::_( 'Message Save Success' );
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager&layout=messages&id='.$quizid.'&tmpl=component', $msg );
		} else {
			jerror::raiseWarning('', $model->getError());
			$this->setRedirect( 'index.php?option=com_vquiz&view=quizmanager&layout=messages&tmpl=component');
		}
	
	}

 
}