<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined('_JEXEC') or die();
jimport('joomla.application.component.modellist');
class VquizControllerVquiz extends VquizController
{

	function __construct()
	{
		parent::__construct();
		$this->model = $this->getModel('vquiz');
		JRequest::setVar( 'view', 'vquiz' );
 
		$this->registerTask( 'add'  , 	'edit' );
		$this->registerTask( 'unpublish',	'publish' );

	}
	
	  function drawLineChart()
	{ 
 
 		$model = $this->getModel('vquiz');
 		$obj = $model->getLinechart();	
 		jexit(json_encode($obj));
 
	} 
	
	function drawpieChart()
	{ 
  		
		$model = $this->getModel('vquiz');

		$obj = $model->getpiechart();	
 	
		jexit(json_encode($obj));


	} 
	
		function drawflagChart()
	{ 
 
 		$model = $this->getModel('vquiz');

		$obj = $model->getflagpiechart();	
 	
		jexit(json_encode($obj));


	} 
	
	 function drawgeoChart()
	{ 
	 
		
		$model = $this->getModel('vquiz');

		$obj = $model->getgeochart();	
 	
		jexit(json_encode($obj));


	} 
 
}

 
?>