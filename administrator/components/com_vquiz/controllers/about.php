<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class VquizControllerAbout extends VquizController



{

	function __construct()

		{



			parent::__construct();

			$this->registerTask( 'add'  , 	'edit' );

		} 



		function cancel()

		{



			$msg = JText::_( 'Operation Cancelled' );

			$this->setRedirect( 'index.php?option=com_vquiz&view=vquiz', $msg );



		}



 



}